# To Do List:
- Verificar que el contenido sea generico.
- Crear una plantilla generica que ilustre el uso de imagenes, referencias externas e internas.
- Validar que el formato de la tesis cumple con el formato reglamentario de la UADY.
- Comentar debidamente el archivo main.tex
- Agregar las references correspondientes en el archivo main.tex
- Crear un archivo instructivo para modificar y compilar enfocado a personal no acostumbrado a latex
- Cuales son las dependencias necesarias?
- Cual es la estructura de carpetas y ficheros?
