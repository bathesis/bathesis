\renewcommand{\thepage}{\arabic{page}}
%\renewcommand{\thesection}{\arabic{section}}
%\renewcommand*\thesection{\arabic{section}}
%\renewcommand*\thesubsection{\arabic{subsection}}
\chapter{Introducción}
%: ----------------------- HELP: latex document organisation
% the commands below help you to subdivide and organise your thesis
%    \chapter{}       = level 1, top level
%    \section{}       = level 2
%    \subsection{}    = level 3
%    \subsubsection{} = level 4

La robótica se ha convertido en una herramienta ubicua en varios campos
de la industria y la sociedad, como una tecnología que brinda enormes beneficios
económicos y sociales a muchas naciones.
Los robots, son generalmente una herramienta de apoyo o reemplazo en la realización
de tareas arriesgadas o imposibles para un ser humano. Aunque recientemente 
han sido utilizados en entornos ajenos a lo industrial,
como podría ser la casa o la oficina;
se utilizan para realizar actividades como:
monitoreo, exploración, servicios, asistencia personal, asistencia médica y
entretenimiento.

En la robótica móvil de interiores, los robots que sobresalen son los robots basados
en ruedas porque son los más óptimos y simples que existen, comparados con los robots
articulados de piernas.

Todos los robots con más de dos ruedas son estáticamente
estables; sin embargo, un robot con dos ruedas diferenciales minimiza su
complejidad mecánica, haciendo de éste mucho más compacto y manejable,
ideal para interiores.

El robot péndulo invertido, robot balancín o
\textit{two wheeled inverted pendulum robot}, en inglés,
es un robot con dos ruedas que pueden moverse con libertad de forma independiente,
sus ejes de rotación son el mísmo, así que tienen una alineación de sentido paralelo,
pero se encuentran separadas la una de la otra por el cuerpo del robot,
dicho cuerpo tiene el centro de masa por encima del eje de rotación de las ruedas,
pero al centro de su separación.

Si se pretende dar movilidad y maniobrabilidad sobre un área de operación,
como lo haría cualquier robot móvil, no podría hacerse con un modelo simple
usando cinemática, el sistema es un poco más complejo, porque para garantizar
el equilibrio del robot, es necesaria una fuerza que evite su precipitación.
Una forma de conseguir ésto usando únicamente sus dos ruedas,
es aprovechando el momento del robot, haciendo un movimiento oscilatorio preciso,
para que la fuerza inercial (además del torque aplicado por los motores)
cancele la acción de la gravedad sobre el cuerpo del robot.

El modelo del péndulo invertido permite analizar el problema de equilibrio que presenta
el robot descrito, porque al simplificar el sistema, el cuerpo del robot puede
abstraerse como el péndulo, y las ruedas del robot como el eje de rotación del péndulo,
de ahí el nombre del robot \cite{article_1}.

Métodos clásicos de control bastan para estabilizar el ángulo de inclinación del robot,
pero no para estabilizarlo mientras se mueve o rota sobre sí mismo, porque
su traslación y el balance están acoplados entre si y controlados únicamente por dos
ruedas, por esto se afirma que es un sistema infra-controlado.
Para conseguir un control preciso tanto de traslación como de balance, es necesario
un controlador de múltiples entradas y múltiples salidas
(aunque éste sí puede ser una combinación de controles clásicos
\cite{article_2,article_3,article_4}).

En este proyecto de investigación, se propone estudiar el problema del péndulo invertido
y extenderlo para describir y controlar la dinámica del robot RAP (figura
\ref{fig:standing0}), siglas de Robot Asistente Personal,
un robot péndulo invertido con dos ruedas diferenciales,
que puede modelarse como un sistema holonómico de dos grados de libertad
(traslación lineal del robot y ángulo de inclinación del cuerpo).

El reto principal y enfoque de este trabajo es la estabilidad mecánica del robot,
ya que el sistema presenta un comportamiento no lineal y altamente inestable,
el cual, no puede ser controlado utilizando modelos cinemáticos.
Es necesario diseñar un modelo más completo que describa el comportamiento
dinámico, el cual servirá como base para el desarrollo de un controlador automático de
múltiples entradas y múltiples salidas, mísmo que debe implementarse en tiempo real
sobre una plataforma de microcontrolador utilizando un sistema operativo en tiempo real.

Al finalizar el proyecto, se contará con un prototipo robótico péndulo invertido
de dos ruedas diferenciales con un sistema de control embebido, que servirá como base
de experimentación para futuras investigaciones sobre robótica móvil o control
automático.

\section{Antecedentes}

El robot péndulo invertido es una máquina muy divertida, sobre todo para
desarrollar como pasatiempo, y lo han demostrado proyectos basados en Arduino$^{TM}$
que pueden encontrarse en internet. La gran mayoría de estos utilizan
un control PID gracias a su simplicidad y a que no requieren un modelo matemático
que estabilizar, sin embargo, el método para encontrar las ganancias adecuadas
del controlador a través de prueba y error, muchas veces toma tiempo y 
significan un desgaste mecánico del robot cuando éste se desestabiliza,
así que encontrar un modelo y estabilizarlo de forma teórica es una solución
más adecuada.

El péndulo invertido es el problema predilecto de control automático y/o sistemas
dinámicos, por lo que no es raro encontrar un modelo en libros de texto para ingeniería.
El modelo clásico que se plantea en estos es el de un péndulo cuyo eje reposa
sobre un carrito libre de moverse hacia delante o hacia atrás
(figura \ref{fig:invertedPendulumClassic}), despreciando completamente la dinámica
de las ruedas o la fricción.
El único interés de este modelo es describír como se comporta el péndulo
según las fuerzas aplicadas por el carrito al moverse \cite{mathWorksInvertedPendulum}.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.2]{./00-Resources/02-Figures/invertedPendulumClassic}
	\caption{Péndulo invertido clásico}
	\label{fig:invertedPendulumClassic}
\end{figure}

A pesar de las diferencias entre el modelo y el sistema físico, esta puede ser una buena
simplificación del problema para estabilizar el balance del robot \cite{TWSBR}.
Por otro lado, es posible tomar en cuenta el efecto del giro de las ruedas sobre la
dinámica del péndulo (figura \ref{fig:wheeledInvertedPendulum}),
es un mejor acercamiento porque en este caso el giro de las ruedas afecta directamente
la inclinación del péndulo. De esta manera se consigue un mejor sistema de control
\cite{balancingTWSegwayRobot,article_4}, sin embargo, aún no contempla el efecto
de tener dos ruedas diferenciales.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.15]{./00-Resources/02-Figures/invertedPendulumModel}
	\caption{Péndulo invertido sobre una rueda.}
	\label{fig:wheeledInvertedPendulum}
\end{figure}

Los controladores más populares para el robot péndulo invertido son: el controlador PID,
el controlador LQR y el controlador Difuso. Estos son los que han sido demostrados
y pueden encontrarse en cualquier investigación sobre el tema \cite{review_1}.
Sin embargo, existen propuestas diferentes muy interesantes, como
la combinación de compensadores integrales y derivativos en
un esquema de múltiples entradas y múltiples salidas, como podría ser
un sistema de control en cascada usando múltiples controles PID,
que ha mostrado ser una excelente propuesta de control
capaz de estabilizar el balance, y regular la posición del robot \cite{article_4}.
Pero es necesario encontrar los compensadores adecuados considerando cada controlador
como un conjunto de sistemas de control acoplado, siendo una actividad compleja.

Por otro lado, gracias al modelo del péndulo invertido se han podido construír
productos comerciales como es el caso de \textit{Segway}, un dispositivo de transporte
personal basado en dos ruedas diferenciales y un sistema de balance activo.
Su modelo más reciente \textit{Segway S-PLUS} (figura \ref{fig:segway}),
además de lo anterior, puede navegar de forma autónoma o ser controlado remotamente
por el usuario a través de una aplicación móvil. Sin embargo, no se conoce nada sobre
su sistema de control o su modelo matemático, más allá de que es funcional y estable.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.2]{./00-Resources/02-Figures/segway-s-plus}
	\caption{Segway S-PLUS}
	\label{fig:segway}
\end{figure}

El robot RAP (figura \ref{fig:standing0}), por su parte, es un proyecto no
comercial cuyo hardware electrónico fue desarrollado utilizando
componentes profesionales de bajo costo.
El proyecto que se propone en éste documento se diferencía de la literatura revisada en el diseño
del robot y su implementación.

El robot RAP es un prototípo de robot móvil cuyo sistema se encuentra distribuído
en varios módulos, los cuales se conocen cómo módulo de control (MC) y módulo de
inteligencia básica (MIB).
El módulo de control, como su nombre lo indíca, es el sistema embebido
basado en un microcontrolador de arquitectura ARM$^{TM}$ que interactua
directamente con los sensores y actuadores esenciales del
robot (acelerómetro, giroscopio, magnetómetro y encoders).
Dicho sistema tiene la obligación de estabilizar
la dinámica del robot y convertir comandos en movimiento.
El módulo de inteligencia básica, es el modulo que gobierna al módulo de
control considerando que el sistema es estable y controlable.
Está basado en un microprocesador de arquitectura ARM$^{TM}$
con sistema operativo Linux. Envía comandos de acción para que el robot RAP
pueda trasladarse. Además, el módulo de inteligencia básica se puede comunicar con otros
sistemas computacionales para escalar las habilidades del robot.
Éste diseño depende en su totalidad de la funcionalidad del módulo de control,
sin él, no es posible convertir el sistema físico en un robot móvil.

El presente trabajo se enfoca en la primera parte del proyecto,
el desarrollo del controlador para el MC del robot RAP.
El sistema físico en realidad tiene un espacio de configuraciones que debe contemplar:
movimiento lineal sobre el plano X-Y que cuenta como 2 grados de libertad,
rotación del eje del péndulo y rotación del robot sobre el centro entre sus ruedas,
lo que suma un total de 4 grados de libertad, un modelo completo del robot RAP
debería contemplar todas estas variables. Resulta en un modelo completo
del sistema, pero altamente acoplado y no lineal.
Simplificando el modelo para describir únicamente el ángulo de inclinación,
posición y velocidad, linelizandolo para un rango seguro de operación del robot
y estabilizandolo con un controlador LQR, se consigue con la primera parte del proyecto
y un prototipo robótico péndulo invertido de dos ruedas diferenciales con un
sistema de control embebido, que servirá como base de experimentación para
futuras investigaciones sobre robótica móvil o control automático.

\section{Objetivo}

\subsection{Pregunta de investigación}

¿Puede un controlador LQR digital estabilizar las variables de posición, velocidad y balance
del robot péndulo invertido RAP de dos ruedas diferenciales?

\subsection{Objetivo general}

Implementar en un sistema embebido, un controlador que estabilice las
variables de posición, velocidad e inclinación del robot péndulo
invertido RAP de dos ruedas diferenciales.

\subsection{Objetivos específicos}

\begin{itemize}
\item{Diseñar un modelo no lineal que describa el comportamiento dinámico del robot RAP
		para las variables de inclinación, posición y velocidad,
		utilizando la ecuación D'Alembert-Lagrange.}
\item{Elegir una restricción de operación y linealizar el modelo dinámico del robot RAP.}
\item{Obtener la representación de estado del sistema a partir del modelo dinámico linealizado.}
\item{Calcular la matriz de ganancias para el controlador por estado usando el algoritmo LQR.}
\item{Discretizar el control automático utilizando el método de Tustin ó el método retenedor de orden cero.}
\item{Implementar el controlador por estado en un microcontrolador TM4C123G de
	Texas Instruments$^{TM}$ usando el sistema operativo en tiempo real TI-RTOS.}
\item{Probar la estabilidad del controlador físicamente, muestreando el estado del robot en tiempo real.}
\end{itemize}


\section{Motivación}

Como estudiante de la carrera de \textit{ingeniería en computación}
de la \textit{Facultad de Matemáticas}, desarrollé un enorme gusto por la programación
de dispositivos embebidos y el diseño de circuitos digitales.
Éste gusto me llevó a aprender nuevas arquitecturas de hardware, y conocer
arquitecturas de software para mejorar mis habilidades como programador.
De ésta manera, me aventuré a conocer software embebido en tiempo real,
aprender un poco sobre instrumentación, e inevitablemente, terminé estudiando
fundamentos de teoría de control automático. El conjunto de disciplinas
en ingeniería que había estado estudiando terminó forjando en mí una pasión.

El robot RAP, viene de las siglas en español de \textit{Robot Asistente Personal},
y es el prototipo de un proyecto que consiste en construir un robot móvil que pudiese asistir a un humano
con tareas como: guía de museo, hostes de un restaurante, ó un monitor remoto para el hogar.
Comprendiendo el funcionamiento de cada componente, tanto de software como de hardware.
Mi motivación para desarrollar el proyecto, es profundizar en aquellos temas que no entran
en el plan de estudios de la carrera, pero permiten complementar mi desarrollo profesional.

\section{Planteamiento del problema}

Actualmente se tiene un robot físico (figura \ref{fig:standing0}),
con un control estable de balance utilizando el método clásico PID
(Proporcional, Integral y Derivativo),
dicho control no contempla ninguna otra variable más que el ángulo de inclinación.
Es por esto que no cumple con las características de un robot móvil,
porque aunque su balance es estable, no es capaz de desplazarse o
permanecer en un lugar fijo.
Por lo tanto, con el sistema actual no es posible implementar rutinas de navegación.
Sin embargo, se cuenta con una base de hardware previamente
construído, sobre el cual poder desarrollar el presente trabajo.

\begin{figure}[H]
	\centering
	%\includegraphics[scale=0.05]{./00-Resources/02-Figures/standing1}
	\includegraphics[scale=0.1]{./00-Resources/02-Figures/standing0}
	\caption{Robot RAP péndulo invertido con control de balance PID.}
	\label{fig:standing0}
\end{figure}


El par de fuerzas que aplican las ruedas diferenciales del RAP, afectan
directamente su posición, velocidad y equilibrio.
El problema del robot péndulo invertido sobre dos ruedas diferenciales,
consiste en diseñar un modelo matemático que describa a todas
las variables a controlar: posición, velocidad, y ángulo de inclinación.
Y estabilizar la dinámica del sistema infra-controlador a partir del modelo,
para que el RAP mantenga su posición o se traslade de forma segura.


\section{Metodología}

Este trabajo consiste en construir un modelo dinámico del robot péndulo invertido RAP,
a partir de sus propiedades físicas, utilizando el principio de D'Alembert como base
para el modelo no lineal, que describe el ángulo de inclinación del cuerpo del robot,
así como la posición y velocidad del centro entre la separación del eje de rotación de
las ruedas (figura \ref{fig:twoWheeledInvertedPendulum});
todas estas variables conviven en un único sistema dinámico por representación de estado
y muestran la inestabilidad y no linealidad del sistema físico.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.3]{./00-Resources/02-Figures/twoWheeledInvertedPendulumModel}
	\caption{Péndulo invertido sobre dos ruedas.}
	\label{fig:twoWheeledInvertedPendulum}
\end{figure}

Con el modelo lineal del sistema en representación de estado
(ecuación \ref{eq:stateSpacex} y \ref{eq:stateSpacey}),
obtenido para una región de operación conocida, se calcula el rango de la matriz de
controlabilidad, para saber si el sistema es controlable.
La región de operación del robot RAP, consiste en definir el rango admisible del ángulo
de inclinación del robot, el cual es cercano a cero y al rededor del cero con variaciones
muy pequeñas, de tal forma que el seno del ángulo sea igual al ángulo, y el coseno del ángulo
sea igual a 1.

Utilizando el algoritmo \textit{regulador lineal cuadrático} (LQR,
por sus siglas en inglés), y definiendo una matriz de costo Q y R para la ecuación 
de costo (ecuación \ref{eq:lqrCostFunctionInfiniteHorizon}), de forma que la velocidad de reacción
aumente mientras que la señal de control sea la menos agresiva posible,
se encuentra el valor de la matriz de control K %(ecuación \ref{eq:control})
que garantiza un control del sistema linealizado.

Posterior a ésto, utilizando el software Octave, se codifica una simulación
para el sistema lineal del robot RAP. Esto es, ingresar
las ecuaciones del sistema en representación de estado a Octave (tanto el sistema
sin control como el sistema con control), asignar un vector de estado inicial
donde exista un ángulo de inclinación diferente de cero, graficar la respuesta del sistema
sin controlador y con controlador, y comparar la respuestas.
Se debe observar que el sistema controlado converge y el sistema sin control diverge,
lo que significa que de los dos sistemas, el sistema controlado es estable.

Antes de implementar el sistema en el RAP, es necesario discretizarlo,
para ello, se hace uso de la relación entre la variable $s$ y la variable
$z$, propuesta por el \textit{método de Tustin}.
Se asigna un periodo de muestreo adecuado, que depende de la frecuencia
del modelo linealizado del sistema de control, y se programa al robot RAP
para emitir por el puerto serial, los valores de las variables de estado en tiempo
real, ésta información produce las gráficas del comportamiento dinámico
del sistema real previo al estabilizador.

Con todo esto, solo resta implementar el controlador en el microcontrolador
del robot. El microcontrolador TM4C123G es un microcontrolador de arquitectura
ARM$^{TM}$ Cortex-4MF, de 32 bits con unidad de coma flotante, acompañado
del sistema operativo en tiempo real TI-RTOS, propiedad de Texas Instruments $^{TM}$
y de libre uso para los microcontroladores de su marca.

Utilizando ésta plataforma embebida, y un sensor de inercia MPU9250 completamente
digital (que cuenta con un acelerómetro, un giroscopio y un magnetómetro),
se implementa una base de software embebido para realizar el proceso
de medición, filtrado y control, con garantía de ejecución en tiempo real,
y un proceso asíncrono para enviar y recibir datos por el puerto serial.
De esta manera, el sistema embebido se encarga de los procesos
que son sensibles al tiempo, delegando a un ordenador externo el proceso
de decidir las direcciones y magnitudes del movimiento del robot (navegación).

Comprobar la efectividad del estabilizador, consiste en perturbar al robot RAP
con ligeras desviaciones del ángulo de inclinación de su cuerpo y,
empujando a éste para obligarlo a moverse de lugar. Por un lado, 
las gráficas resultantes de esta actividad deben mostrar que las variables
de estado convergen, por otro, el robot RAP debe presentar resistencia
perceptible al movimiento y evitar desplomarse, para lo cual,
se puede afirmar que el sistema físico es estable y el controlador funciona.

\section{Contribuciones}
\label{sec:contribuciones}

La singularidad del presente trabajo radica en el modelo matemático del robot RAP, y la implementación
de un sistema de control para el robot RAP, siendo los siguientes, los aspectos principales:

\begin{itemize}
	\item{Diseño de un modelo dinámico no lineal y linealización para los criterios de operación
		del robot RAP, utilizando la ecuación de D'Alembert - Lagrange.}
	\item{Estabilización del modelo linealizado del robot RAP con un controlador LQR,
		e implementación física del control LQR en el robot RAP.}
\end{itemize}

\section{Estructura del documento}

El documento esta organizado por capítulo, el primero de ellos sirve como introducción al tema
principal del proyecto, el segundo, expone los fundamentos teóricos del proyecto.
Los capítulos que comprenden del tercero al quinto, muestran la metodología dividida por fases.
El documento concluye con el capítulo sexto, donde se exponen los resultados obtenidos,
las observaciones del autor y los trabajos futuros.

A continuación se menciona una breve descripción por capítulos de este trabajo.

\textbf{Capítulo 1.- Introducción.}

Este capítulo sirve como justificación del documento, expone la motivación
del autor, hace una introducción al tema, describe los antecedentes,
objetivos y la metodología seguida durante el desarrollo.

\textbf{Capítulo 2.- Marco teórico.}

Éste capítulo expone los fundamentos teóricos detrás de los métodos
y decisiones tomadas en los siguientes capítulos.
Los principales conceptos son:
\textit{mecánica clásica}, \textit{sistemas LTI continuos}, \textit{sistemas LTI discretos}
y \textit{control automático de sistemas LTI}.

\textbf{Capítulo 3.- Diseño del robot.}

Este es el primer capítulo de la metodología, presenta el sistema físico conocido como RAP.
Se muestra el diseño del robot, las dimensiones, grados de libertad, restricciones,
sensores y actuadores disponibles. Además, expone los rangos operativos del robot y propone
un modelo matemático no lineal, junto con su respectiva linealización y simplificación.

\textbf{Capítulo 4.- Diseño del controlador.}

El segundo capítulo del método analiza el modelo matemático en su forma LTI para
definir los requerimientos del controlador y encontrar la ley de control.

\textbf{Capítulo 5.- Implementación física.}

El último capítulo del método propone una arquitectura de software embebido
para el sistema en tiempo real, de forma que el robot sea capaz de muestrear
el estado del sistema dinámico, reaccionar a tiempo a las perturbaciones externas,
recibir comandos de control y comunicar las variables de estado de la planta.

\textbf{Capítulo 6.- Discución de resultados.}

El último capítulo del documento exhibe los resultados obtenidos,
muestra las concluciones del autor, y por otro lado, propone mejoras
a futuro que el proyecto podría tomar en cuenta.
