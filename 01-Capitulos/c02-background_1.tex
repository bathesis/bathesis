\renewcommand{\thepage}{\arabic{page}}
\section{Sistemas LTI continuos.}

Un sistema (figura \ref{fig:systemBlockDiagram})
es una combinación de componentes que actúan juntos
y realizan un objetivo determinado \cite{ogata2003ingenieria}.
Es una entidad capaz de producir una señal
en respuesta a otra señal de entrada y su propio estado,
mísmo que puede evolucionar con el tiempo. Cuando ésto
sucede, se dice que el sistema es dinámico.

Un sistema puede ser representado por una función que depende
de una señal de entrada.
Por su parte, una señal es una representación numérica de
una entidad medible, ésta puede ser alguna propiedad
del sistema, alguna magnitud externa al sistema, etc.
También puede ser representada como una función, cuya variable
independiente es el tiempo.

Una señal de tiempo continuo (o señal continua),
es aquella que se define sobre un intervalo continuo de tiempo.
La amplitud puede tener intervalo continuo de valores o 
solamente un número finito de valores distintos \cite{ogata1996sistemas}.
Un sistema de tiempo continuo, por lo tanto, es un sistema cuya
señal de respuesta a cualquier entrada es una señal continua.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{./00-Resources/02-Figures/system}
	\caption{Representación de un sistema en un diagrama a bloques.}
	\label{fig:systemBlockDiagram}
\end{figure}

Cuando un sistema tiene propiedades sujetas a degradación
o amplificación según el tiempo, se dice que
el sistema es \textit{variante en el tiempo}.
Además, si el sistema no cumple con el principio de \textit{superposición},
se dice que el sistema es \textit{no lineal}.

Sea $F$ un sistema, $a$, $b$ escalares y $x$, $y$ señales,
se dice que $F$ es lineal sí y sólo sí cumple con el principio
de superposición (ecuación \ref{eq:superposition}).

\begin{equation}
	F(ax+by) = aF(x) + bF(y)
\label{eq:superposition}
\end{equation}

Un sistema LTI (lineal e invariante en el tiempo, por sus siglas en inglés) es
un sistema que cumple con el principio de superposición y cuyas propiedades no
cambian con el tiempo. Éstos sistemas, al cumplir con dicho principio,
pueden ser caracterizados a través de su respuesta al impulso $h(t)$,
la cual es una señal que representa la salida del sistema a una señal
teórica de entrada llamada delta de Dirac
\footnote{Delta de Dirac es también denominada impulso unitario.}
$\delta (t)$,
y por consecuencia, se puede determinar la respuesta $y(t)$ a cualquier señal de entrada $x(t)$
computando la convolución de la señal de entrada $x(t)$ y la respuesta al impulso $h(t)$
\cite{haykin2007signals},
de forma que:

\begin{equation}
	y(t) = h(t)*x(t) %= (h*x)(t)
	\triangleq 
	\int_{\infty}^{\infty} h(\tau) x( t - \tau ) d \tau
\label{eq:convolution}
\end{equation}

Si el sistema en cuestion, caracterizado por el impulso unitario $h(t)$,
es un sistema sin memoria, entonces $h(t) = c\delta (t)$, siendo $c$
una constante arbitraria. Si por su parte, el sistema
es un sistema causal, entonces $h(t) = 0\ \forall\ t<0$.

La linealidad de un sistema LTI se puede apreciar mejor cuando el sistema
es representado en el dominio complejo $s$, lo cual se consigue a través
de realizar la transformación de Laplace al sistema (ecuación \ref{eq:laplaceTransform}).
De ésta forma, un sistema LTI se puede caracterizar en el dominio $s$
a través de la función de transferencia $H(s)$, como sigue:

\begin{equation}
	H(s) = \frac{Y(s)}{X(s)}
\label{eq:transferFunction}
\end{equation}

Dónde $X(s)$ y $Y(s)$ son el resultado de aplicar la transformación de Laplace
a la señal de entrada y a la señal de salida, respectivamente.

La función de transferencia \ref{eq:transferFunction}
se reduce al cociente de dos polinomios como muestra
la ecuación \ref{eq:transferFunctionPolynomRatio}.

\begin{equation}
	H(s) = \frac{b_Ms^M + b_{M-1}s^{M-1} + \dots + b_0 }{s^N + a_{N-1}s^{N-1} + \dots + a_1s + a_0 }
	= \frac{B(s)}{A(s)}
	\label{eq:transferFunctionPolynomRatio}
\end{equation}

Ésta representación es útil para analizar el sistema, ya que las raíces
\footnote{Se denomina raíz de un polinomio, a aquellos valores constantes
(en este caso $s_i\ \in \mathbb{C}$) que evaluados en el polinomio lo reducen a cero.}
del polinomio $A(s)$
(denominados polos) indican si el sistema es inestable a través de encontrar por lo menos
una raíz $s_i$ que cumpla que $Re(s_i) > 0$ (la parte real de $s_i$ sea mayor a cero),
por otro lado, las raíces del polinomio $B(s)$ se denominan ceros, y sirven para
analizar las respuestas frecuenciales del sistema
\cite{ogata2003ingenieria}.

\subsection{Transformada integral de Laplace}

La transformada integral de Laplace, es una transformación integral
que permite representar una función en el dominio real $\mathbb{R}$, con una función
análoga en el dominio complejo $\mathbb{C}$.

Sea $f$ una función $f:\mathbb{R} \to \mathbb{R}$, con variable
independiente $t$, y sea $F$ su análoga $F\ :\ \mathbb{C} \to \mathbb{C}$
con variable independiente $s\ |\ s = \sigma + i \omega$, $F(s)$ se computa como sigue:

\begin{equation}
	F(s) = \mathcal{L}\{f\}
	= \int_{-\infty}^{\infty} f(t)e^{-st} dt
\label{eq:laplaceTransform}
\end{equation}

En el caso de $f(t)$ ser una función causal, entonces $f(t) = 0 \ \forall t<0$ lo que resulta en
la transformada de Laplace unilateral (ecuación \ref{eq:unilateralLaplaceTransform}).

\begin{equation}
	F(s) = \mathcal{L}_u\{f\}
	= \int_{0}^{\infty} f(t)e^{-st} dt
\label{eq:unilateralLaplaceTransform}
\end{equation}

La transformada de Laplace existe cuando la integral converge a un valor.
Una condición necesaria es la integrabilidad absoluta de $f(t)e^{-st}$.
Siendo que $|f(t)e^{-st}| = |f(t)e^{- \sigma t}|$, debemos tener que:

\begin{equation}
	\int_{0}^{\infty} | f(t)e^{-\sigma t} | dt 
	\leq c
\label{eq:laplaceROC}
\end{equation}

Para algún valor finito c. Entonces, el rango de valores de $\sigma$ para los
cuales la ecuación \ref{eq:laplaceROC} converge, se conoce como región de convergencia.

Una propiedad muy útil e importante sobre la transformada de Laplace, es el teorema
de corrimiento temporal, que expresa lo siguiente:

\begin{equation}
	\mathcal{L}\{ x(t-\tau) \} = e^{-s\tau}X(s)
\label{eq:laplaceTimeShift}
\end{equation}

\subsection{Serie de Taylor.}

En la naturaleza, los sistemas son generalemente dinámicos,
variantes en el tiempo y no lineales. Sin embargo, al momento
de análizar una planta
\footnote{En el campo de la ingeniería de control, se denomina planta al sistema que se desea controlar.}
se puede simplificar el sistema despreciando la degradación física
que lo convierte en un sistema variante en el tiempo, y linealizando
las propiedades no lineales del mísmo.

Para linealizar una función no lineal, se hace uso de la serie de Taylor,
que consiste en aproximar una función a una serie de potencias cuyos coeficientes
son las derivadas de la misma función, y es válido cuando la linealización
se hace en un rango concreto, sobre el que se sabe de antemano que será
el rango de operación del sistema.

Si $f$ es una función no lineal que depende de $x$, y $a$ es el punto central
del rango de operación de $f$, entonces, la linealización de $f$ usando la serie de Taylor es la siguiente:

\begin{equation}
	f(x) \approx \sum_{n=0}^{\infty} \frac{f^{(n)}(a)}{n!}(x-a)^n
\label{eq:taylor}
\end{equation}

Donde $f^{(n)}(a)$ denota la derivada n-ésima de la función $f$ evaluada en $a$.

La serie converge a $f(x)$, siempre y cuando $f(x)$ sea diferenciable en el intervalo $(n,m)$
de forma que $n<a<m$.

\subsection{Representación de sistemas LTI en el espacio de estado.}

El vector de estado de un sistema dinámico ($\vec{x}$),
es el arreglo más pequeño de variables que representan al sistema.
Las variables dentro de éste arreglo se denominan variables de estado.
De forma que el conocimiento de éstas variables en $t=t_0$, junto con
el conocimiento de la señal de entrada (representada como $\vec{u}$) para $t \geq t_0$,
determinan completamente el comportamiento del sistema en cualquier $t \geq t_0$ \cite{ogata2003ingenieria}.

Un sistema LTI representado en el espacio de estado puede expresarse con las ecuaciones
\ref{eq:stateSpacex} y \ref{eq:stateSpacey}, y su representación gráfica
usando el diagrama a bloques se ilustra en la figura \ref{fig:stateSpace}.

\begin{equation}
	\dot{\vec{ x }} = A \vec{x}(t) + B \vec{u}(t)
	\label{eq:stateSpacex}
\end{equation}

\begin{equation}
	\vec{y} = C \vec{x}(t) + D \vec{u}(t)
	\label{eq:stateSpacey}
\end{equation}

Dónde $\vec{y}$ es el vector de salidas del sistema, $\vec{u}$ es el vector de entradas,
$A$ es la matriz de estado, $B$ es la matriz de entrada, $C$ es la matriz de salida
y $D$ es la matriz de transmisión directa.

Cabe mencionar que las variables de estado no son necesariamente variables físicas, y que,
al conjunto de valores que puede tomar cada variable de estado, desde el punto de vista
del vector de estado $\vec{x}$, se le denomina espacio de estado.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{./00-Resources/02-Figures/stateSpace}
	\caption{Diagrama a bloques de un sistema dinámico en el espacio de estado.}
	\label{fig:stateSpace}
\end{figure}
