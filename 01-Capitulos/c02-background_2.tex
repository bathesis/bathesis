\renewcommand{\thepage}{\arabic{page}}

\section{Sistemas LTI discretos.}

Una señal de tiempo discreto, es una señal definida sólo en
valores discretos de tiempo (esto es, aquellos en los que
la variable independiente $t$ está cuantificada).
No es lo mísmo una señal de tiempo discreto que una señal digital,
una señal digital, es una señal de tiempo discreto y de amplitud también
discreta \cite{ogata1996sistemas}. En el presente trabajo
se respetará la diferencia entre éstos términos.

Un sistema de tiempo discreto es, por lo tanto, un sistema cuya
respuesta a cualquier entrada, es una señal de tiempo discreto.
Las señales en tiempo discreto surgen si el sistema involucra
la operación de muestreo de señales en tiempo continuo (sección \ref{sec:sampler}).

Al igual que los sistemas continuos, los sistemas discretos LTI cumplen
con el principio de superposición (ecuación \ref{eq:superposition}),
y la invarianza temporal.

De ésta manera, un sistema LTI discreto puede ser caracterizado a través
de su respuesta al impulso $h[k]$, la cual es una señal que representa la
salida del sistema a una señal teórica de entrada llamada delta de Kronecker
\footnote{Esta función es similar a la función delta de Dirac para el tiempo continuo, solo que modulada
como un único pulso en $t=0$.}
 $\delta[k]$,
y por consecuencia, se puede determinar la respuesta $y[k]$ a cualquier señal de
entrada $x[k]$ computando la convolución discreta de la señal de entrada $x[k]$
y la respuesta al impulso $h[k]$ \cite{haykin2007signals}, de forma que:

\begin{equation}
	y[k] = h[k]*x[k] %= (h * x)[k] 
	\triangleq 
	\sum_{n=-\infty}^{\infty} h[ n ] x[ k - n ]
\label{eq:discreteConvolution}
\end{equation}

Al igual que un sistema LTI continuo, un sistema LTI discreto
tiene una representación análoga en el dominio complejo $z$,
a través de calcular la transformada Z del sistema
(ecuación \ref{eq:zTransform}).
La función de transferencia $G(z)$ de un sistema se define entonces como:

\begin{equation}
	G(z) = \frac{Y(z)}{X(z)}
	= \frac{B(z)}{A(z)}
	= \frac{b_0 + b_1z^{-1}+b_2z^{-2}+\dots+b_mz^{-m}}
	{1+a_1z^{-1}+a_2z^{-2}+\dots+a_nz^{-n}}
\label{eq:zTransform}
\end{equation}

Dónde, $X(z)$ y $Y(z)$ son la transformación Z de la señal
de entrada y la señal de salida respectivamente.
Las raíces del polinomio $B(z)$ se conocen como los ceros del sistema,
y las raíces del polinomio $A(z)$ se conocen como los polos del sistema.

\subsection{Transformada Z.}

La transformada Z, es una transformación que permite representar
una función en el dominio entero $\mathbb{Z}$, con una función análoga en el
dominio complejo $\mathbb{C}$.

Sea $f$ una función discreta $f\ :\ \mathbb{Z} \to \mathbb{R}$ con
variable independiente $k$, y sea $F$ su análoga $F\ :\ \mathbb{C} \to \mathbb{C}$,
con variable independiente $z\ |\ z=r\e^{j\omega}$, $F(z)$ se computa como sigue:

\begin{equation}
	F(z) = \mathcal{Z}\{f\}
	=
	\sum_{n=-\infty}^{\infty} f[ n ] z^{-n}
\label{eq:zTransform}
\end{equation}

En el caso de $f[k]$ ser una función causal, entonces $f[k] =0\ \forall\ t<0$,
lo que resulta en la transformada Z unilateral (ecuación \ref{eq:unilateralZTransform}).
\begin{equation}
	F(z) = \mathcal{Z}_u\{f\}
	=
	\sum_{n=0}^{\infty} f[ n ] z^{-n}
\label{eq:unilateralZTransform}
\end{equation}

La transformada Z existe cuando la suma infinita converge a un valor.
Una condición necesaria es la sumabilidad absoluta de $f[n]z^{-n}$.
Siendo que $|f[n]z^{-n}| = |f[n]r^{-n}|$, se debe tener que:

\begin{equation}
	\sum_{n=0}^{\infty} |f[ n ] r^{-n}|
	\leq c
\label{eq:zROC}
\end{equation}

Para algún valor finito $c$. Entonces, el rango de valores de $r$ para los
cuales la ecuación \ref{eq:zROC} converge, se conoce como
región de convergencia.

Una propiedad muy útil e importante sobre la transformada Z es el teorema de
corrimiento temporal, que expresa lo siguiente:

\begin{equation}
	\mathcal{Z}\{ x[k-n] \} = z^{-n}X(z)
\label{eq:zTimeShift}
\end{equation}

\subsection{Representación de sistemas discretos LTI en el espacio de estado.}

Al igual que los sistemas LTI continuos, es posible representar un sistema 
discreto en el espacio de estado cuyo sistema de ecuaciones se expresa como

\begin{equation}
	\vec{ x }[ k+1 ] = G \vec{x}[k] + H \vec{u}[k]
	\label{eq:dStateSpacex}
\end{equation}

\begin{equation}
	\vec{y}[k] = C \vec{x}[k] + D \vec{u}[k]
	\label{eq:dStateSpacey}
\end{equation}

Donde $\vec{x}$ es el vector de estado,
$\vec{u}$ es el vector de entradas, $\vec{y}$ es el vector de salidas,
$G$ es la matriz de estado, $H$ es la matriz de entrada,
$C$ es la matriz de salida y $D$ es la matriz de transmisión directa.

Los detalles más profundos sobre la ésta representación pueden encontrarse
en la bibliografía \cite{ogata1996sistemas}.

\subsection{Muestreador.}
\label{sec:sampler}

Un muestreador es un sistema que recibe una señal de tiempo continuo
y produce una señal de tiempo discreto a través de un proceso
denominado proceso de muestreo o simplemente muestreo.

El proceso de muestreo, es el proceso de reemplazar una señal continua
por una secuencia de valores en puntos discretos de tiempo.
Esto es, siendo $f(t)$ una señal de tiempo continuo con variable independiente $t$,
la señal de tiempo discreto $f_{d}[k]$ se define como:

\begin{equation}
	f_{d}[k] = f(kT)
\label{eq:discreteSampling}
\end{equation}

Donde $T$ es una constante en $\mathbb{R}$ denominada periodo de muestreo,
y $k$ es la variable independiente de la señal que pertenece a $\mathbb{Z}$.
Entonces $f_{d}\ :\ \mathbb{Z} \to \mathbb{R}$. Este muestreo por impulsos
de amplitud igual a la señal continua en el instante $k$, puede aproximar
la señal original $f$ con un sistema de retención de datos capaz de generar
la señal faltante en el intervalo de tiempo $kT \leq t < (k+1)T$ mediante
una extrapolación polinomial en $\tau$ como sigue:

\begin{equation}
	f_{d}[k+\tau] = a_{n}\tau^{n} + a_{n-1}\tau^{n-1} + \dots + a_{0}
%\label{eq:discreteSampling}
\end{equation}

Donde $0 \leq \tau < T$. Como la señal $f_d[k] = f(kT)$ entonces se debe tener:

\begin{equation}
	f_{d}[k+\tau] = a_{n}\tau^{n} + a_{n-1}\tau^{n-1} + \dots + f(kT)
%\label{eq:discreteSampling}
\end{equation}

Si el sistema de retención de datos es un extrapolador polinomial de n-ésimo orden,
entonces se conoce como retenedor de n-ésimo orden. De éste modo, si $n=1$, se denomina
retenedor de primer orden, y cuando $n=0$ se tiene un retenedor de orden cero.

Un sistema de muestreo y retención de orden cero, implica que la señal
muestreada en el instante $kT$ mantendrá su amplitud constante hasta el siguiente periodo
de muestreo $(k+1)T$,
los detalles se encuentran en la bibliografía \cite{ogata1996sistemas}.

\subsection{Correspondencia entre la variable S y la variable Z.}

Se considera un muestreador ficticio llamado muestreador por impulsos
(ecuación \ref{eq:discreteSampling}). La salida $x^{*}(t)$ de éste muestreador se
considera como un tren de impulsos que comienza en $t=0$,
con el periodo de muestreo $T$ y la magnitud de cada impulso igual al valor
muestreado de la señal en el tiempo contínuo en el instante de muestreo
correspondiente, es decir:

\begin{gather*}
	x^{*}(t) = \sum_{k=0}^{\infty} x(kT) \delta(t-kT) \\
	\delta (t-kT) = 0\ \forall\ t \neq kT \\
	k = 0,\ 1,\ 2,\ 3,\ \dots\\
\end{gather*}

Aplicando la transformada de Laplace de $x^{*}$ para obtener $X^{*}(s)$:

\begin{equation}
\begin{align*}
	X^{*}(s) = x(0) \mathcal{L}\{\delta(t)\} + x(T) \mathcal{L}\{\delta(t-T)\}
		+ x(2T) \mathcal{L}\{\delta(t-2T)\} + \dots \\
	= x(0) + x(T)e^{-Ts} + x(2T)e^{-2Ts} + \dots \\
	= \sum_{k=0}^{\infty} x(kT)e^{-kTs}
\end{align*}
	\label{eq:laplaceztransform}
\end{equation}

Es posible observar, que si definimos a $z = e^{sT}$, o $s = \frac{1}{T}ln(z)$,
la ecuación \ref{eq:laplaceztransform} se convierte en:

\begin{equation}
\label{eq:laplacezrelation}
	X^{*}(s) \biggr|_{s=\frac{1}{T}ln z} = X(z) = \sum_{k=0}^{\infty} x(kT)z^{-k}
\end{equation}

Como menciona la bibliografía \cite{ogata1996sistemas},
la transformada de Laplace de la señal muestreada mediante impulsos $x^{*}(t)$
ha mostrado ser la misma que la transformada Z de la señal $x(t)$ si
$e^{Ts}$ se define como $z$ ó $z \triangleq e^{Ts}$.

Si se utiliza la relación entre la variable $s$ y la variable $z$ dónde
$s = \sigma + j \omega$, entonces se tiene que:
$$
z = e^{T(\sigma + j \omega)} = e^{T\sigma}e^{jT\omega}
$$
Cuando la parte real de $s$ es menor a cero ($Re\{s\} = \sigma < 0$), y si se aplica
la formula de Euler tal que, $re^{jb} = r( cos(b) + j sen(b) )$, el resultado debe ser
\begin{equation}
\begin{align*}
	z = e^{T\sigma} e^{jT\omega} = e^{T\sigma}( cos(\omega) + j sen(\omega) )\\
	|z| = e^{T\sigma} \sqrt{ cos^{2}(\omega) + sen^{2}(\omega) } \\
	|z| = e^{T\sigma} < 1
\end{align*}
	\label{eq:laplacezUnitCircle}
\end{equation}

La ecuación \ref{eq:laplacezUnitCircle} indica que todos los valores complejos
$s$ cuya componente real $Re\{s\}$ sea menor a cero, corresponden a valores
en variable compleja $z$ dentro del círculo unitario $|z| < 1 $.

Por su parte, se tiene $s = j\omega$ cuando $\sigma = 0$, lo que corresponde en variable $z$ a
lo siguiente:

\begin{equation}
\begin{align*}
	z = e^{T\sigma} e^{jT\omega} = e^{T\sigma}( cos(\omega) + j sen(\omega) )
	\biggr|_{s=j\omega}\\
	|z| = \sqrt{ cos^{2}(\omega) + sen^{2}(\omega) } \\
	|z| = 1
\end{align*}
\end{equation}

Lo que implica que cuando la variable compleja $s$ tiene componente real $Re\{s\}$ 
igual a cero, entonces, existe una correspondencia de dicha variable en el
círculo unitario, tal que $|z| = 1$, para la variable compleja $z$.

Además, cuando $\sigma$ tiende a infinito ($\sigma\to\infty$) ó $\sigma$ tiende a menos infinito
($\sigma\to -\infty$) se tienen los siguientes límites:

\begin{equation}
\label{eq:laplacezlimit1}
\lim _{\sigma \to \infty} z = 
\lim _{\sigma \to \infty} e^{sT} = 
\lim _{\sigma \to \infty} e^{T\sigma}e^{jT\omega} = \infty
\end{equation}

\begin{equation}
\label{eq:laplacezlimit2}
\lim _{\sigma \to -\infty} z = 
\lim _{\sigma \to -\infty} e^{sT} = 
\lim _{\sigma \to -\infty} e^{T\sigma}e^{jT\omega} =
\lim _{\sigma \to \infty} \frac{ e^{jT\omega} }{e^{T\sigma} } = 0
\end{equation}

El límite \ref{eq:laplacezlimit1}, indica que cuando la parte real de $s$
tiende a infinito $Re\{s\} \to \infty$, la variable compleja $z$ también tiende
a infinito $z \to \infty$. Por su parte, el límite \ref{eq:laplacezlimit2}, indica que cuando
la parte real de $s$ tiende a infinito negativo $Re\{s\} \to -\infty$, la
variable compleja $z$ tiende a cero $z\to0$.

Por último, si se considera $s = \sigma + j\omega$ en la relación que tiene forma polar $z = e^{sT}$,
y se analísa el ángulo de $e^{sT}$ se debe tener que:

$$
	\angle z = \angle e^{T\sigma}e^{jT\omega} = \angle e^{T\sigma}( cos(T\omega) + jsen(T\omega) )
	= \tan\biggr( \frac{e^{T\sigma}sen(T\omega)}{e^{T\sigma}cos(T\omega)}\biggr) = T\omega
$$

Dónde se puede deducir que $T\omega = T\omega + 2\pi k\ \forall\ k \in \mathbb{Z}$,
lo que implica que para una variable $z$ existen infinitas variables $s$ que satisfacen
la relación $z = e^{sT}$, y que sólo éxiste correspondencia directa para valores de $\omega$
dentro del rango $(-\pi,\pi)$. Ésta información se encuentra detallada en la bibliografía \cite{ogata1996sistemas}.

\subsection{Teorema de muestreo.}

El teorema de muestreo, o también llamado teorema de muestreo de Nyquist-Shannon
o teorema de Nyquist, es un teorema muy importante para la ingeniería de control,
el procesamiento de señales y de especial interés en las telecomunicaciones.
El teorema dice lo siguiente:

\textit{
Sea $x(t)$ una señal oscilatoria de frecuencia máxima $\omega_{max}$,
Si $\omega_{s} > 2\omega_{max}$, dónde $\omega_{s} = \frac{2\pi}{T}$
denominado frecuencia de muestreo y $T$ denominado periodo de muestreo,
entonces la señal $x(t)$ puede ser determinada inequívocamente por sus
muestras $x(nT)$ con $n\in\mathbb{Z}$.
}

De ésta manera, la frecuencia mínima de muestreo $2\omega_{max}$ se denomina
frecuencia de muestreo de Nyquist, tal y como se expone en el libro \cite{haykin2007signals}.

Éste teorema nos permite construir señales discretas a partir de señales continuas
con la garantía de poder recuperar la señal original a través de un proceso
de conversión de señal discreta a señal continua y un filtro pasa bajas.

%\subsection{Ancho de banda.}

%\subsection{Diagrama de Bode.}

\subsection{Transformación bilineal.}

También llamado método de Tustin, es una transformación utilizada comúnmente en el campo
de procesamiento de señales y teoría de control de señales discretas.
Ésta transformación permite convertir señales de tiempo continuo en señales de tiempo
discreto a partir de la relación $z = e^{sT}$.

Si partimos de $f(x) = e^{x}$, siendo una función diferenciable al rededor del origen,
es decir $x = 0$, entonces, podemos aproximar un polinomio utilizando la
serie de Taylor (ecuación \ref{eq:taylor}) como sigue:

$$
	f(x) = \sum_{k=0}^{\infty} \frac{ f^{(k)}(0) (x-0)^{k} }{ k! }
	= \sum_{k=0}^{\infty} \frac{ e^0 x^k }{ k! }
	= \sum_{k=0}^{\infty} \frac{ x^k }{ k! }
$$

Si usamos la serie de Taylor de primer orden se debe tener que:

$$
	f(x) = e^x = \sum_{k=0}^{k=1} \frac{ x^k }{ k! }
	\approx 1 + x
$$

Ésta aproximación converge a $e^x$ para valores pequeños de x al rededor del origen,
y permiten desarrollar la siguiente expresión:

\begin{equation}
\begin{align*}
	z = e^{sT} = e^{s\frac{T}{2}}e^{s\frac{T}{2}} = \frac{e^{s\frac{T}{2}}}{e^{-s\frac{T}{2}}}
	\approx \frac{ 1+\frac{sT}{2} }{ 1-\frac{sT}{2} }\\
	z \approx \frac{ 1+\frac{sT}{2} }{ 1-\frac{sT}{2} }
\end{align*}
	\label{eq:zbilinear}
\end{equation}

Ó, si despejamos $s$, se debe tener lo siguiente:

\begin{equation}
\begin{align*}
	%z = \frac{ 1+\frac{sT}{2} }{ 1-\frac{sT}{2} } \\
	%z = \frac{ 2+sT }{ 2-sT } \\
	%2z - zsT = 2 + sT \\
	%2z - 2 = sT + zsT \\
	%2(z-1) = sT(1 + z) \\
	%\frac{2(z-1)}{T(1+z)} = s \\
	s = \frac{2}{T}\frac{(z-1)}{(z+1)}
\end{align*}
\label{eq:sbilinear}
\end{equation}

Para los sistemas cuyos polos $p_i$ son estables ($Re\{p_i\} < 0$),
la transformación bilineal permite mapear dichos polos dentro del
círculo unitario $|z| < 1$
conservando la estabilidad del sistema, tal como se expone en la bibliografía \cite{aastrom2013computer}.
