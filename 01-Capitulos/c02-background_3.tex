\renewcommand{\thepage}{\arabic{page}}

\section{Control automático de sistemas LTI.}

Un sistema de control, es un tipo de sistema cuyo propósito
es manipular la respuesta de otro sistema al que se denomina planta,
a partir de una señal de entrada y un requisito de comportamiento.

En general, un sistema de control recibe una entrada a la que se denomina
\textit{referencia}, y se expresa como la función temporal $r(t)$.
La diferencia entre la salida del sistema a controlar (la planta),
expresada como la función temporal $y(t)$,
y la entrada del sistema, $r(t)$, se expresa como la señal $e(t)$ denominada error,
es decir, $e(t) \triangleq r(t) - y(t)$.

Cuando el sistema de control recibe como entrada la señal de referencia, $r(t)$,
y otra señal que depende de el mismo, como podría ser el error, $e(t)$,
%Cuando el sistema de control recibe como entrada el error, $e(t)$, del sistema,
se denomina \textit{control de lazo cerrado} o \textit{realimentado} (figura \ref{fig:openloop}).
Si, por otro lado, el sistema recibe como entrada únicamente la señal de referencia $r(t)$,
éste se denomina control de lazo abierto (figura \ref{fig:closedloop}).

Para cualquier caso, el sistema de control produce una señal de salida expresada
como la función temporal, $u(t)$, denominada señal de control. La señal de control
es la señal de entrada que recibe la planta para producir la señal de salida, $y(t)$.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.3]{./00-Resources/02-Figures/controllerOpenLoop}
	\caption{Diagrama a bloques de un sistema de control de lazo abierto ideal.}
	\label{fig:openloop}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.3]{./00-Resources/02-Figures/controllerClosedLoop}
	\caption{Diagrama a bloques de un sistema de control de lazo cerrado ideal.}
	\label{fig:closedloop}
\end{figure}

En general, teoría de control estudia la estabilidad y controlabilidad
de sistemas dinámicos.

\subsection{Control de lazo abierto.}

Los controladores de lazo abierto se utilizan para manipular sistemas dinámicos
expuestos a perturbaciones
\footnote{Interferencia o alteración del sistema debido a agentes externos al sistema y el controlador.}
ligeras y despreciables, o cuando la precisión de la variable de control
\footnote{Al valor físico de la señal $y(t)$ se le denomina variable de control.}
no es un requisito de diseño del controlador.

Para construir un control de lazo abierto, es un requisito indispensable que el sistema
dinámico de la planta sea estable en lazo abierto
\footnote{La estabilidad de un sistema radica en la ubicación de sus polos sin sistema de control.},
ya que el control de lazo abierto no
puede estabilizar el sistema.

\subsection{Control de lazo cerrado.}

Los controladores de lazo cerrado, en contraste, ofrecen más opciones,
permiten manipulan sistemas dinámicos expuestos a perturbaciones, 
aumentan la precisión reduciendo el error de control, pueden ser más eficientes,
más rápidos y más confiables. Éstos no requieren que la planta sea estable,
ya que pueden estabilizar la dinámica de la planta.

En esencia, un control de lazo cerrado usa la información proporcionada
por la realimentación, para responder a una serie de parámetros mientras
reduce el error de control, $e(t)$.

Los controladores de lazo cerrado pueden organizarse en dos categorías,
el control estabilizante o regulador, y el controlador seguidor o servocontrolador.

El control estabilizante consiste en minimizar el error, $e(t)$,
para una señal de referencia fija en el origen, $r(t)=0\ \forall t$, provocando que la señal
de salida, $y(t)$, sea cero.

El segundo tipo es el controlador seguidor o servocontrolador, éste sistema de control
pretende que la señal de error, $e(t)$, sea mínima mientras la señal de entrada, $r(t)$,
varía con el tiempo, provocando que la señal de salida, o conjunto de señales de salida,
$y(t)$, sigan a los valores de la señal de entrada, $r(t)$.

\subsection{Estabilidad de un sistema LTI.}

Como se explica en \cite{haykin2007signals}, un sistema se dice estable,
si la entrada, $x(t)$, acotada en amplitud, produce una señal de salida, $y(t)$,
también acotada en amplitud, es decir:

\begin{equation}
\begin{split}
	|x(t)| \leq C \ \biggr |\ C < \infty,\ \ \forall t \\
	|y(t)| \leq D \ \biggr |\ D < \infty,\ \ \forall t \\
\end{split}
\end{equation}

Si el sistema es un sistema LTI con respuesta al impulso, $h(t)$,
entonces se puede obtener a $y(t)$ como sigue:

\begin{equation}
\begin{split}
	y(t) = (h*x)(t) = \int_{-\infty}^{\infty} h(\tau)x(t-\tau) d\tau \\
	| y(t) | = | (h*x)(t) | = \int_{-\infty}^{\infty} | h(\tau)x(t-\tau) |d\tau \\
	| y(t) | \leq \int_{-\infty}^{\infty} |h(\tau)|C d\tau \\
\end{split}
\end{equation}

Dónde se deduce que, para confirmar que un sistema LTI es estable, es condición suficiente
la integrabilidad absoluta de la respuesta al impulso del sistema.

$$
	\int_{-\infty}^{\infty} |h(\tau)| d\tau \leq E \ \biggr |\ E < \infty
$$

Ésta condición está estrechamente relacionada con los polos del sistema
en su representación como función de transferencia \ref{eq:transferFunctionPolynomRatio}.

Como dice \cite{kamen2008fundamentos}, un sistema con una función de transferencia
$H(s)$, es estable si y sólo si todos los polos se localizan en el semiplano
izquierdo abierto, ($Re\{p_i\} < 0$, la parte real del polo $i$ es menor a cero).
Y es marginalmente estable si y sólo si todos los polos se localizan en el semiplano izquierdo cerrado
($Re\{p_i\} \leq 0$, la parte real del polo $i$ es menor o igual a cero), siempre y cuando
los polos sean únicos.

Un sistema dinámico LTI representado como un sistema lineal de ecuaciones diferenciales
(ecuaciones \ref{eq:stateSpacex} y \ref{eq:stateSpacey}), puede expresar una
función de transferencia como sigue:

\begin{equation}
\begin{split}
		X = X(s) = \mathcal{L}\{x(t)\}\\
		Y = Y(s) = \mathcal{L}\{y(t)\}\\
		U = U(s) = \mathcal{L}\{u(t)\}\\
		sX = AX + BU \\
		%\Rightarrow
		(sI - A)X = BU \\
		%\Rightarrow
		X = (sI - A)^{-1}BU \\
		%\Rightarrow
		Y = C(sI - A)^{-1}BU + DU \\
		%\Rightarrow
		Y = (C(sI - A)^{-1}B + D)U \\
		%\Rightarrow
		G(s) = C(sI - A)^{-1}B + D \\
		%\Rightarrow
		G(s) = C\biggr( \frac{1}{det(sI - A)}adj((sI-A)^T) \biggr)B + D \\
		%\Rightarrow
		G(s) = \frac{Q(s)}{ det(sI - A) }
\end{split}
\end{equation}

Dónde $Q(s)$ es algún polinomio en $s$, $adj((sI-A)^T)$ es la matriz adjunta de la
transpuesta de la matriz $(sI-A)$, $det(sI-A)$ es el determinante de la matriz $(sI-A)$.

Del desarrollo anterior, se puede decir con seguridad que los polos del sistema $G(s)$
son los valores propios de la matriz $A$.

\subsection{Control por realimentación de estado.}

Un sistema de control moderno puede poseer muchas entradas y muchas salidas,
que se relacionan entre sí. El enfoque en el espacio de estado para el análisis
de sistemas es el más conveniente desde éste punto de vista. Mientras
la teoría de control convencional se basa en la relación entrada-salida,
o función de transferencia, la teoría de control moderna se basa en la
descripción de las ecuaciones de un sistema en términos de $n$ ecuaciones
diferenciales de primer orden, que se combinan en una ecuación diferencial
vectorial de primer orden \cite{ogata2003ingenieria}.

Un sistema de control por realimentación de estado (figura \ref{fig:stateFeedback}), es un sistema
de lazo cerrado cuya señal de realimentación no es la señal de salida,
sino el estado mísmo del sistema.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{./00-Resources/02-Figures/stateFeedbackController}
	\caption{Diagrama a bloques de un sistema de control por realimentación de estado.}
	\label{fig:stateFeedback}
\end{figure}

Éste sistema puede representarse como el siguiente sistema lineal de ecuaciones diferenciales:

\begin{equation}
\begin{split}
	u(t) = -Kx(t)\\
	\dot{\vec{x}}(t) = ( A - BK ) \vec{x}(t) \\
	\vec{y}(t) = ( C - DK ) \vec{x}(t)
\end{split}
\end{equation}

Dónde $K$, es la matriz de ganancias.

La expresión $\dot{\vec{x}}(t) = ( A - BK ) \vec{x}(t)$, describe como evoluciona el estado
del sistema en función de la realimentación.
La expresión $\vec{y}(t) = ( C - DK ) \vec{x}(t)$, describe como evoluciona la salida
del sistema en función del estado y la realimentación.

Si se definen las matrices $E$ y $F$, tal que $E = (A-BK)$, y $F = (C-DK)$, entonces se debe tener:

\begin{equation}
\begin{split}
	\dot{\vec{x}}(t) = E\vec{x}(t) \\
	\vec{y}(t) = F\vec{x}(t) \\
	sX(s) = EX(s) \\
	X(s) = (sI-E)^{-1} \\
	X(s) = \frac{1}{det(sI-E)} adj((sI-E)^T) \\
	Y(s) = \frac{1}{det(sI-E)} F( adj((sI-E)^T) ) \\
\end{split}
\end{equation}

De esta forma, los polos de lazo cerrado son los valores propios de la matriz $E$,
es decir, los valores propios de la matriz $A-BK$.

Como se menciona en \cite{ogata2003ingenieria}, la estabilidad y las características
de respuesta transitoria se determinan mediante los valores caracterísiticos de la
matriz $A-BK$. Si se elige la matriz $K$ de forma adecuada, la matriz $A-BK$ se
estabiliza y para todas las variables de estado, $x_i(0) \neq 0$, es posible 
hacer que $x_i(t) \to 0$ cuando $t \to \infty$.
Los valores propios de la matriz $A-BK$ se denominan polos del regulador. El problema
de situar los polos en lazo cerrado en las posiciones deseadas se denomina problema
de asignación de polos, y es posible si y sólo si el sistema en lazo cerrado
es completamente controlable.

\subsection{Controlabilidad.}

Se dice que el sistema es de estado controlable en $t=t_0$,
si es posible construir una señal de control $u(t)$ sin restricciones,
que transfiera un estado inicial, $\vec{x_0} = \vec{x}(t_0)$, a cualquier estado final,
$\vec{x_1} = \vec{x}(t_1)$, en un intervalo finito de tiempo $t_0 \leq t \leq t_1$,
si todos los estados son controlables, entonces el sistema es completamente
controlable \cite{ogata2003ingenieria}.

Para un sistema representado en el espacio de estado con la ecuación \ref{eq:stateSpacex},
con matriz de estado $A \in \mathbb{R}^{n \times n}$, matriz de entrada
$B \in \mathbb{R}^{n \times r}$ y señal de entrada $\vec{u}(t) \in \mathbb{R}^{r}$,
se dice que el sistema es de estado completamente controlable si y sólo si, la \textit{matriz de controlabilidad},
ecuación \ref{eq:controllabilityMatrix}, es de rango $n$, lo que equivale a decir que los $n$ vectores
columna son linealmente independientes \cite{ogata2003ingenieria}.

\begin{equation}
	[B | AB | \dots | A^{n-1}B ] \in \mathbb{R}^{n\times nr}
\label{eq:controllabilityMatrix}
\end{equation}

Sin embargo, la controlabilidad completa del estado no implica necesariamente
la controlabilidad de la salida del sistema.

Si se considera la salida del sistema como la ecuación \ref{eq:stateSpacey},
con matriz de salida $C\in\mathbb{R}^{m\times n}$ y matriz de transmisión directa
$D\in\mathbb{R}^{m\times r}$, entonces, el sistema es de salida completamente
controlable si y sólo si, la matriz \ref{eq:controllabilityMatrixOfOutput} es de rango $m$.

\begin{equation}
	[CB | CAB | CA^{2}B | \dots | CA^{n-1}B | D ] \in \mathbb{R}^{m\times (n+1)r}
\label{eq:controllabilityMatrixOfOutput}
\end{equation}

\subsection{Observabilidad.}

La observabilidad de un sistema LTI sin exitación, (sin señal de entrada),
consiste en determinar el estado del sistema, $\vec{x}(t)$, a partir de la observación de la
salida, $\vec{y}(t)$, durante un intervalo finito de tiempo $t_0 \leq t \leq t_1$. Ésto
significa que la salida del sistema debe ser afectada por todas y cada una de las
variables de control \cite{ogata2003ingenieria}.

La observabilidad de un sistema permite deducir variables que no pueden ser medidas
físicamente.

Un sistema LTI, descrito por las ecuaciones \ref{eq:stateSpacex} y \ref{eq:stateSpacey},
con entrada $\vec{u}(t) = 0\ \forall\ t$, con matriz de estado,
$A\in\mathbb{R}^{n\times n}$, y matriz de salida, $C\in\mathbb{R}^{m\times n}$; es completamente
observable si y sólo si la matriz de observabilidad (ecuación \ref{eq:observabilityMatrix}), tiene rango $n$.

\begin{equation}
\begin{bmatrix}
	C\\
	CA\\
	\cdots\\
	CA^{n-1}
\end{bmatrix}
\in \mathbb{R}^{nm\times n}
\label{eq:observabilityMatrix}
\end{equation}

\subsection{Control óptimo.}

Teoría de control óptimo, es una teoría de control moderno cuyo objetivo
es determinar la señal de control que provoque que un sistema satisfaga
restricciones físicas y al mísmo tiempo, minimice (o maximice) un
criterio de rendimiento\cite{kirk2012optimal}.

Una señal de control, $\vec{u}(t)$, que satisface las restricciones durante
un intervalo $[t_0,t_f]$, se denomina \textit{control aceptable}.

Una trayectoria del vector de estado, $\vec{x}(t)$, que satisface
las restricciones de estado en el intervalo $[t_0,t_f]$, se denomina
\textit{trayectoria aceptable}.

Por su parte, si a partir del estado inicial $\vec{x}(t_0)=\vec{x_0}$,
se aplica una señal de control $\vec{u}(t)$ en el intervalo $[t_0,t_f]$,
ésta producirá una trayectoria de estado. La función que asigna
un valor real único a cada trayectoria de estado del sistema se denomina
\textit{medida de rendimiento}.

El problema de control óptimo consiste en encontrar un control
aceptable $\vec{u}^{*}(t)$ que provoque que el sistema
$\vec{x}(t) = \vec{a}( \vec{x}(t), \vec{u}(t), t )$ (no necesariamente LTI)
siga una trayectoria admisible $\vec{x}^{*}(t)$ que minimice la función de rendimiento
(ecuación \ref{eq:nonLinealPerformanceMeasure}).

\begin{equation}
	J = h(\vec{x}(t_f),t_f) + \int_{t_0}^{t_f} g(\vec{x}(t),\vec{u}(t),t)dt
\label{eq:nonLinealPerformanceMeasure}
\end{equation}

Dónde $h$ y $g$ son funciones escalares asociadas a la dinámica del sistema
y las variables de rendimiento.

$\vec{u}^{*}$ se denomina control óptimo y $\vec{x}^{*}(t)$
se denomina trayectoria óptima, y cumplen que:

% \mathbf{x} = \vec{x}
$$
	J^{*} = h( x^{*}(t_f), t_f ) + \int_{t_0}^{t_f} g(\vec{x}^{*}(t),\vec{u}^{*}(t),t)dt
	\leq
	J = h(x(t_f),t_f) + \int_{t_0}^{t_f} g(x(t),u(t),t)dt
$$

Si se puede encontrar una relación funcional de la forma $u^{*}(t) = f(x(t),t)$,
para el control óptimo $u^{*}(t)$ en el tiempo $t$, entonces la función $f$,
se denomina \textit{ley de control óptimo}, o \textit{política de control}.
Pues ésta produce una señal de control óptimo para cualquier trayectoria de estado
$x(t)$ admisible.
Si la ley de control óptimo es una realimentación LTI del estado,
esta será de la forma $u^{*}(t) = Fx(t)$ \cite{kirk2012optimal}.

\subsection{Regulador lineal cuadrático de tiempo continuo.}

El regulador lineal cuadrático o LQR por sus siglas en inglés, es un criterio
de control óptimo donde la función de desempeño de un sistema LTI en el espacio de estado
(ecuaciones \ref{eq:stateSpacex} y \ref{eq:stateSpacey}),
es una función diferencial cuadrática de la forma:

\begin{equation}
	J = \int_{t_0}^{\infty} \biggr[\vec{x}^{T}(t)\mathbf{Q}\vec{x}(t) +
		\vec{u}^{T}(t)\mathbf{R}\vec{x}(t) +
		2\vec{x}^{T}(t)\mathbf{S}\vec{u}(t)\biggr] dt
\label{eq:lqrCostFunctionInfiniteHorizon}
\end{equation}

Dónde las matrices $\mathbf{Q}\in\mathbb{R}^{n\times n}$, $\mathbf{R}\in\mathbb{R}^{m\times m}$ y
$\mathbf{S}\in\mathbb{R}^{n\times m}$ son matrices arbitrarias que determinan la importancia
relativa del error (en el caso de la matriz $Q$) y la energía de la señal de control
(en el caso de la matriz $R$), mientras que la matriz $S$ determina el costo
del valor final.

La ley de control que minimiza el valor de costo es $\vec{u}(t) = -K\vec{x}(t)$,
dónde la matriz $K$ está dada por $K = R^{-1}(B^{T}P+N^{T})$, y $P$ se encuentra resolviendo
la ecuación algebraica de Riccati en tiempo continuo \cite{kirk2012optimal}
(ecuación \ref{eq:riccatiInfiniteHorizon}).

\begin{equation}
	A^{T}P + PA - (PB + N)R^{-1}(B^{T}P + N^{T}) + Q = 0
\label{eq:riccatiInfiniteHorizon}
\end{equation}

Por suerte, existen métodos numéricos implementados en paquetes de software como Matlab\textregistered\
u Octave que pueden computar la matriz de ganancia $K$ a partir de las
matrices $Q$, $R$ y $S$.

\subsection{Regulador lineal cuadrático de tiempo discreto.}

El criterio del regulador lineal cuadrático puede aplicarse a sistemas LTI
en el espacio de estado (ecuaciones \ref{eq:dStateSpacex} y \ref{eq:dStateSpacey})
de tiempo discreto con la función de desempeño \ref{eq:lqrCostFunctionDiscrete}.

\begin{equation}
	J = \sum_{k=0}^{\infty} \biggr[\vec{x}^{T}[k]\mathbf{Q}\vec{x}[k] +
		\vec{u}^{T}[k]\mathbf{R}\vec{x}[k] +
		2\vec{x}^{T}[k]\mathbf{S}\vec{u}[k]\biggr]
\label{eq:lqrCostFunctionDiscrete}
\end{equation}

%$F = (R+B^{T}PB)^{-1}(B^{T}PA+N^{T})$, y $P$
La ley de control que minimiza el valor de costo es $\vec{u}[k] = -F\vec{x}[k]$,
dónde la matriz $F$, está dada por $F = (R+H^{T}PH)^{-1}(H^{T}PG+N^{T})$, y $P$
es la única matriz solución definida positiva para la ecuación algebraica de Riccati
de tiempo discreto \cite{kirk2012optimal} (ecuación \ref{eq:riccatiDiscrete}).

\begin{equation}
	P = G^{T}PG - (G^{T}PH + N)(R + H^{T}PH)^{-1}(H^{T}PG+N^{T})+Q
	%P = A^{T}PA - (A^{T}PB + N)(R + B^{T}PB)^{-1}(B^{T}PA+N^{T})+Q
\label{eq:riccatiDiscrete}
\end{equation}
