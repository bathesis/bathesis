\renewcommand{\thepage}{\arabic{page}}
\chapter{Marco Teórico}

\section{Mecánica clásica.}

La mecánica clásica, es la rama de la física  que estudia las leyes del
comportamiento de cuerpos físicos macroscópicos en equilibrio y a velocidades
pequeñas comparadas a la velocidad de la luz.
Se podría decir que la mecánica clásica se divide en tres campos de estudio,
la \textit{estática}, la \textit{cinemática} y la \textit{dinámica}.

Por un lado, la estática estudia los cuerpos en equilibrio
y por otro, la cinemática y la dinámica estudian los cuerpos en movimiento,
aunque bajo diferentes enfoques.
La cinemática estudia el movimiento de los cuerpos sin consideración de las
fuerzas que lo provocan. La dinámica estudia las fuerzas que producen el
movimiento de un cuerpo.

Los fundamentos de la dinámica son las tres leyes de Newton:
\begin{itemize}
	\item{La ley de inercia.}
	\item{La ley de la interacción y la fuerza}, y por último
	\item{la ley de acción-reacción.}
\end{itemize}

La dinámica estudia a los cuerpos como una partícula centro de masa,
o como un conjunto de partículas que interactuan entre sí.
De ésta manera, un cuerpo sólido es un cuerpo cuyo conjunto de particulas
que lo conforman tienen separación y forma constante. Aunque en la naturaleza,
todos los cuerpos se deforman por cambios de temperatura, presión y/o fuerzas
externas, en casos prácticos es posible simplificar el análisis suponiendo
lo contrario.

La primera ley de Newton dicta:

\textit{Toda partícula conserva su estado de reposo o velocidad
constante (a partir de un marco de referencia inercial) a menos
que una fuerza sea aplicada.}

Para que un punto en el espacio pueda ser considerado un marco
de referencia inercial, éste debe tener aceleración nula.
Lo que permite que la variación del momento lineal de una partícula sea
igual a las fuerzas reales sobre la mísma. Lo que nos lleva a la segunda
ley de Newton:

\textit{El cambio del estado de movimiento de una partícula está dada por
el cambio de su momento, siendo directamente proporcional 
a las fuerzas motrices impresas.}

Sea $\vec{p}$ el momento lineal de una partícula de masa $m$ y velocidad
$\vec{v} \in \mathbb{R}^3$,
tal que $\vec{p} = m\vec{v}$, entonces, la segunda ley de Newton indica que la
fuerza puede ser descrita como muestra la ecuación \ref{eq:newton2ndLaw}.

\begin{equation}
	\dot{\vec{p}} = m\dot{\vec{v}} = m\vec{a} = \vec{f}
	\label{eq:newton2ndLaw}
\end{equation}

Por último, la tercer ley de Newton dice:

\textit{A toda acción le corresponde una reacción igual pero en sentido
contrario: lo que quiere decir que las acciones mutuas de dos cuerpos siempre
son iguales y dirigidas en sentido opuesto.}


\subsection{El principio de mínima acción.}

Un cuerpo tiene energía cuando es capaz de realizar un trabajo.
En los sistemas mecánicos se identifican dos tipos de energía:
la \textit{energía cinética} y la \textit{energía potencíal}.

El trabajo $W_{AB}$ realizado por una partícula para desplazarse desde un
punto $A$ hasta un punto $B$ (figura \ref{fig:displacement}) se calcula como muestra la ecuación
\ref{eq:workprinciple1}.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{./00-Resources/02-Figures/work}
	\caption{Desplazamiento de una partícula}
	\label{fig:displacement}
\end{figure}

Dado que el trabajo de una fuerza de acción sobre una partícula depende del
trayecto de desplazamiento entre un punto A y un punto B,
éste será diferente para cada trayecto. Sin embargo, además de todos
los posibles desplazamientos que una partícula puede seguir,
sólo exíste uno que minimizará la acción (el trabajo), y ésta será la trayectoria
que la partícula seguirá de forma fortuita. A esta afirmación se le conoce
como \textit{principio de mínima acción}. A cualquier otro desplazamiento
posible se le denomina \textit{desplazamiento virtual}, y al trabajo realizado
por la fuerza teórica que realiza el desplazamiento virtual, se le conoce como
\textit{trabajo virtual}.

\begin{equation}
	W_{AB} = \int^{B}_{A}{ \vec{f} \cdot d\vec{s} }
	\label{eq:workprinciple1}
\end{equation}

Usando la ecuación \ref{eq:newton2ndLaw} en la ecuación \ref{eq:workprinciple1},
se tiene que:

\begin{equation}
	W_{AB} = \int^{B}_{A}{ \dot{\vec{p}} \cdot d\vec{s} }
	= \int^{B}_{A}{ \frac{d\vec{p}}{dt} \cdot d\vec{s} }
	= \int^{B}_{A}{ \frac{d\vec{s}}{dt} \cdot d\vec{p} }
	= \int^{B}_{A}{ \vec{v} \cdot d\vec{p} }
	= m \int^{B}_{A}{ \vec{v} \cdot d\vec{v} }
	= \frac{m}{2} ( \vec{v}^T \cdot \vec{v} ) |_{A}^{B}
	\label{eq:workprinciple2}
\end{equation}

De la ecuación \ref{eq:workprinciple2} el término energía cinética $K$
para una partícula de masa $m$ se define como sigue:

\begin{equation}
	K \triangleq \frac{m}{2} ( \vec{v}^{\ T} \vec{v} )
	\label{eq:kineticEnergy}
\end{equation}

La energía cinética depende de la posición de la partícula y su velocidad,
siendo ésta energía  $ K = \frac{m}{2} v^2 $, para una partícula con velocidad
lineal en una dimensión ($\mathbb{R}$).

Observese que
$$
\frac{d}{dt} \frac{ \partial K }{ \partial \vec{v} }
= \dot{ \vec{p} }
= m\vec{a}
$$
Si se llama fuerza inercial ($f_I$) a la derivada temporal de la
derivada parcial de $K$ con respecto a la velocidad de la partícula,
se tiene que
\begin{equation}
	f_I
	= - \frac{d}{dt} \frac{ \partial K }{ \partial \vec{v} }
	= - \dot{ p }
	= - ma
\label{eq:inertialForce}
\end{equation}
entonces, se puede reescribir la ecuación \ref{eq:newton2ndLaw} como sigue

\begin{equation}
	f + f_I = f - \frac{d}{dt} \frac{ \partial K }{ \partial \vec{v} } = 0
	\label{eq:dalembertNewton}
\end{equation}

Por otro lado, si el trabajo $W_{AB}$ no depende del trayecto
que sigue la partícula, entonces, si se considera una trayectoria
cerrada $C$ para la partícula, de forma que ésta se traslade desde
un punto $A$ hasta un punto $B$ y de vuelta al punto $A$ por otro
trayecto estrictamente diferente, el trabajo total $W$ es 0,
lo que implica la existencia de un campo de fuerzas conservativo
en el espacio que ocupan esos puntos y la partícula.
Dicho campo de fuerzas conservativo conlleva a la existencia de una función
escalar $U$ de tal  manera que el campo de fuerzas se deduce como la ecuación
\ref{eq:potentialEnergy}, tal y como se muestra en el libro \cite{3dMotionOfRigidBodies}.

\begin{equation}
	\vec{f}(\vec{d}) = - \nabla U(\vec{d})
	\equiv
	\vec{f}(\vec{d}) = - \frac{ \partial U }{ \partial \vec{ d } }
	= - \frac{ \partial U }{ \partial \vec{ s } }
\label{eq:potentialEnergy}
\end{equation}

Donde $\vec{d} = x\hat{i} + y\hat{j} + z\hat{k}$, representa la posición de la
partícula en el sistema referencial ubicado en $0\hat{i} + 0\hat{j} + 0\hat{k}$.

Usando la ecuación \ref{eq:potentialEnergy} en la ecuación \ref{eq:workprinciple1},
se obtiene la expresión del trabajo realizado por la partícula en su trayectoria $AB$
en la ecuación \ref{eq:workPotentialEnergy}.

\begin{equation}
	W_{AB} = - \int^{B}_{A}{ \frac{ \partial U }{ \partial \vec{ s } } \cdot d\vec{s} }
	= U_A - U_B
\label{eq:workPotentialEnergy}
\end{equation}

De esta manera, se denomina \textit{energía potencial} a la función escalar $U$.
La energía total de la partícula es entonces $E = U+K$ de forma tal que

$$
	W_{AB} = (K_B + U_A) - (K_A + U_B)
$$

\subsection{Principio de D'Alembert.}

El principio de D'Alembert dicta que:
\textit{el trabajo virtual total realizado por las fuerzas impresas
(incluyendo las de restricción), aumentadas en las fuerzas de inercia,
se desvanece por el desplazamiento virtual}, tal como menciona el libro \cite{3dMotionOfRigidBodies},
se puede expresar como la ecuación \ref{eq:dalembertPrinciple}.

\begin{equation}
	( \vec{f} + \vec{f_I} ) \cdot \delta \vec{d} = 0
\label{eq:dalembertPrinciple}
\end{equation}

Donde $\delta \vec{d}$ es el desplazamiento virtual de la partícula.

Una partícula se acelera debido a fuerzas efectivas ($\vec{f_e}$) impresas sobre ella,
y se desacelera por aquellas fuerzas que limitan su desplazamiento
para ciertas configuraciones, denominadas fuerzas de restricción $\vec{f_r}$,
ambas fuerzas tienen efecto sobre la partícula en forma de una fuerza total $\vec{f}$.
Esto nos permite reescribir la ecuación \ref{eq:dalembertNewton} como

$$
\vec{f} + \vec{f_I} = (\vec{f_e} + \vec{f_r} ) + \vec{f_I}
$$

De esta menera, en combinación con el principio del trabajo virtual,
el principio de D'Alembert se puede calcular usando exclusivamente
las fuerzas efectivas despreciando las fuerzas de restricción como sigue:

$$
	( \vec{f_e} + \vec{f_I} ) \cdot \delta \vec{d} = 0
$$

Por último, existen cuatro tipo de fuerzas que conforman a la fuerza total
resultante $\vec{f}$.

\begin{itemize}
	\item{La fuerza inercial $\vec{f_I}$ ecuación \ref{eq:inertialForce}.}
	\item{Las fuerzas conservativas $\vec{f_U}$ ecuación \ref{eq:potentialEnergy}.}
	\item{Las fuerzas restrictivas o de restricción $\vec{f_r}$.}
	\item{Las fuerzas disipativas o de fricción $\vec{f_d}$.} Y por último,
	\item{las fuerzas exógenas $\vec{f_E}$, que son todas aquellas que agregan energía al sistema.}
\end{itemize}

\subsection{Mecánica Lagrangiana.}

Cuando se considera un sistema de múltiples partículas, ya sea de
un único cuerpo o múltiples cuerpos, caractérizar el comportamiento
utilizando las ecuaciones anteriores para cada partícula complica
el trabajo por la cantidad de ecuaciones del sistema.
Nuestro sistema de partículas puede ser expresado de forma más
sencilla en términos de las energías del movimiento permitido 
para las partículas y la configuración del sistema, utilizando
\textit{coordenadas generalizadas} de la siguiente manera:

$$
\vec{q} = (q_1,q_2,\dots,q_n)^T
$$

Las coordenadas generalizadas representadas por $\vec{q}$, son un arreglo
de las $n$ variables mínimas necesarias (linearmente independientes) que 
permiten definir la configuración del sistema.
Cabe mencionar que $\vec{q}$ no es un vector único.

De esta manera, la posición de cada particula dentro del sistema de $N$ partículas,
se puede obtener a partir de éstas coordenadas generalizadas y el tiempo.
Por ejemplo, la posición $\vec{d}$ de la partícula $j$ con respecto al marco de
referencias inercial $\Sigma_0$ es:

$$
	\vec{d}_j = f( q_1 , q_2 , \dots , q_n , t )
	= d_j ( \vec{q} , t ) \in \mathbb{R}^3
$$

Donde $d_j$ es la función que convierte las coordenadas generalizadas $\vec{q}$
y el tiempo en una posición espacial para la partícula $j$.

Por su parte, $\dot{ \vec{ q } }$, se denomina velocidad generalizada
tal que

$$
	\dot{\vec{q}} = ( \dot{q_1} , \dot{q_2} , \dots , \dot{ q_n } )^T
$$

La matriz Jacobiana $J$ de la partícula $j$ es la derivada parcial del vector
$\vec{d_j}$ con respecto a las coordenadas generalizadas $\vec{q}$ de tal
forma que

\begin{equation}
	J_j(\vec{q}) = \frac{\partial\vec{d_j}}{\partial{\vec{q}}}
	\in \mathbb{R}^{3\times n}
	\label{eq:Jacobian}
\end{equation}

Usando la matriz Jacobiana de la partícula $j$, se puede
expresar la velocidad de ésta misma partícula como

$$
	\vec{v_j} = J_j(\vec{q}) \dot{\vec{q}}
$$

Ahora bien, la energía cinética total del sistema puede
expresarse a través de las coordenadas generalizadas
y las velocidades generalizadas, como se muestra en la
ecuación \ref{eq:kinetic1}.

\begin{equation}
	K(\vec{q},\dot{\vec{q}}) = \frac{1}{2} \sum_{j=1}^{N} m_j \|\dot{\vec{d}}\|^2
	= \frac{1}{2} \sum_{j=1}^{N} m_j ( \dot{\vec{d_j}} \cdot \dot{\vec{d_j}} )
\label{eq:kinetic1}
\end{equation}

O usando la ecuación \ref{eq:Jacobian} en su lugar
se tiene que

\begin{equation}
	K( \vec{q}, \dot{\vec{q}} ) =
	\frac{1}{2} \dot{\vec{q}}^T H( \vec{q} ) \dot{\vec{q}}
\label{eq:kineticEnergyJacobian}
\end{equation}

Donde $H(\vec{q})$ se denomina \textit{matriz de inercia},
y se define como:

\begin{equation}
	H(\vec{q}) 
	\triangleq 
	\sum_{j=1}^{N} m_j J_j^T( \vec{q} ) J_j( \vec{q} )
	\in \mathbb{R}^{n\times n}
\label{eq:inertialMatrix}
\end{equation}

Existe otra variable conocida como \textit{fuerzas generalizadas}
$Q \in \mathbb{R}^n$, que es la suma de las fuerzas exógenas del sistema
como sigue

\begin{equation}
	Q = \sum_{j=1}^{N} J_j^T( \vec{q} ) \vec{f_e}
\label{eq:generalizedForces}
\end{equation}

Aplicando el principio de mínima acción en terminos de 
las coordenadas generalizadas, se puede llegar a la expresión
conocida como \textit{ecuación D'Alembert-Lagrange}
que se expresa en la ecuacióni \ref{eq:dalembertLagrange}, existen más
detalles y un excelente contraste entre la ecuación
\textit{D'Alembert-Lagrange} y la ecuación \textit{Euler-Lagrange}
en el libro \cite{3dMotionOfRigidBodies}.

\begin{equation}
	\frac{d}{dt} \frac{ \partial K }{ \partial \dot{ \vec{q} } }
	- \frac{ \partial K }{ \partial \vec{q} }
	= Q
\label{eq:dalembertLagrange}
\end{equation}

\subsection{Leyes de movimiento de Euler.}

Las leyes de movimiento de Euler son dos ecuaciones que extienden las
leyes de movimiento de Newton para una partícula, al movimiento
de un cuerpo rígido. Se parte de la premisa de que un cuerpo rígido
está compuesto por un conjunto de partículas infinitesimalmente pequeñas
cuya masa, $dm$, componen la masa total del cuerpo, $m$, y cuya
distancia entre sí es siempre constante.

Derivado de la primer ley de Newton, todo desplazamiento es relativo
a un marco de referencia inercial, a partir del cuál se puede obtener
el ímpetu\footnote{Cantidad de movimiento, \textit{momentum} o simplemente momento.}
de cada partícula como el producto de la masa $dm$ y la velocidad lineal $\dot{\vec{r}}$
de la partícula, $d\vec{p} = \dot{\vec{r}}dm$, como se expone en la segunda ley de Newton.
La tercer ley de Newton, aunada a la premisa del cuerpo rígido, permite eliminar
casi todas las fuerzas infinitecimales, $d\vec{f}$, entre partículas cuando éstas se suman,
resultando exclusivamente en fuerzas externas.
La fuerza resultante es entonces la contribución de cada componente
diferencial de fuerza del cuerpo rígido (expresado como $B$), es decir:

$$
\vec{f} = \int_B d\dot{\vec{p}} = \int_B df = m \dot{\vec{v}}_{cm}
$$

Dónde $\vec{v}_{cm}$ es la velocidad lineal de un punto teórico denominado
centro de masa, lugar dónde se puede suponer que se concentra toda la masa del cuerpo.

La primer ley de movimiento de Euler dicta que,
\textit{el ímpetu lineal de un cuerpo rígido es igual al producto de la masa
total del cuerpo y la velocidad lineal del centro de masa}, es decir:

\begin{equation}
	\vec{p} = m\vec{v}_{cm}
\label{eq:eulerSecondLaw}
\end{equation}

La segunda ley expresa,
\textit{la razón de cambio instantanea del ímpetu angular de una partícula sobre un punto fijo
es igual a la suma de los momentos centrales de fuerza externos actuando sobre el cuerpo}, es decir:

\begin{equation}
	d\dot{\vec{L}}_0 = d \vec{n}_0
\label{eq:eulerSecondLaw}
\end{equation}

Por otro lado, el momento central se define como el producto vectorial de dos vectores,
siendo el primero un vector distancia y el segundo, una magnitud física.
De ésta manera, el momento central de una fuerza sobre un punto, también denominado
par de fuerza o torque, es el producto vectorial entre el vector distancia que une
al punto de rotación y el punto de aplicación del vector de fuerza, y el vector fuerza,
es decir:

$$
	\vec{n} = \vec{r} \times \vec{f}
$$

Así mísmo, el ímpetu angular de una partícula, es el momento central del ímpetu lineal
de una partícula con respecto a un punto de rotación:

$$
	\vec{L} = \vec{r} \times \vec{p}
$$

De ésta manera, la ecuación \ref{eq:eulerSecondLaw} puede expresarse como:

\begin{equation}
	\int_B \dot{L} = \int_B \vec{r} \times d\vec{f} = \vec{n}
\label{eq:eulerSecondLaw2}
\end{equation}

\subsection{Cinemática de cuerpos rígidos.}

Para poder medir la posición, y por consiguiente, el movimiento de una partícula en el espacio,
es necesaria una referencia denominada referencial inercial, y que tiene la particularidad de no poseer aceleración.
En este punto se originan los 3 vectores principales, ejes del espacio euclideano,
en una orientación conveniente que cumple con la regla de la mano derecha.

Cualquier punto en el espacio puede ser descrito como una traslación del origen hasta
la posición del punto en cuestion, dicha posición y su cambio instantaneo permiten medir el movimiento
de la partícula.

Por otro lado, es posible adoptar un referencial no inercial, a partir del cual
se mida la posición de un grupo de partículas agrupadas en coordenadas
locales, dicho referencial se denomina referencial local, y a su vez, puede ser
el referencial base de un conjunto de referenciales no inerciales,
según lo amerite el sistema. Cualquier movimiento descrito en coordenadas locales no cumplen necesariamente
con las leyes de Newton, porque el referencial local no tiene necesariamente una aceleración cero.

Para poder analizar el movimiento dado en coordenadas locales utilizando las leyes de Newton,
es necesario realizar una transformación del sistema en referencial local, a un sistema de referencia
inercial.

Cualquier punto en el espacio descrito en coordenadas locales, $\vec{r}_p^{(1)}$, puede escribirse
en coordenadas globales, como la traslación del referencial global, expresado como $\Sigma_0$, hasta la posición
del origen del referencial local, $\vec{d}_{\Sigma_1}$, aumentado en el producto de una matriz de rotación
que transforma la orientación del referencial local en la orientación del referencial global,
$R_0^1$, y la posición del punto en coordenadas locales, $\vec{r}_p^{(1)}$, es decir:

\begin{equation}
	\vec{r}_p = \vec{r}_{\Sigma_1} + R_0^1 \vec{r}_p^{(1)}
	\label{eq:traslationAndRotation}
\end{equation}

La matriz de rotación, es una matriz en $\mathbb{R}^{3\times3}$, cuyas componentes
pueden calcularse a partir de 3 o más valores que representan la rotación del referencial
local con respecto al referencial global, éxisten diferentes formas
de representar ángulos entre referenciales, algunos ejemplos pueden encontrarse
en el libro \cite{3dMotionOfRigidBodies}, sin embargo, por simplicidad, 
en éste documento se utilizan los ángulos roll-pitch-yaw.

Siguiendo con el análisis cinemático, la partícula, $p$, cuya posición ahora representada en coordenadas globales
como, $\vec{r_p}$, posee una velocidad equivalente a la primera derivada temporal de $\vec{r}_p$, tal que:

$$
	\dot{ \vec{r_p} } = \dot{ \vec{r_{\Sigma_0}} } +
						\dot{ R_0^1 } \vec{r_p}^{(1)} +
						R_0^1 \dot{ \vec{r_p} } ^{\ (1)}
$$

Cuando la partícula $p$ y el referencial local $\Sigma_1$ pertenecen a un cuerpo rígido,
entonces se sabe de antemano que la velocidad $\dot{ \vec{r_p} } ^{\ (1)}$ vale
cero, pues las partículas de un cuerpo rígido guardan una distancia constante dentro del cuerpo,
además, como se menciona en el libro \cite{3dMotionOfRigidBodies}, la derivada temporal de la matriz de rotación,
se puede expresar como $\dot{R_0^1} = R_0^1 \ [\omega^{(1)}\times] = [\omega^{(0)}\times] \ R_1^0 $,
de ésta forma la velocidad de la partícula se reescribe como sigue:

\begin{equation}
	\dot{ \vec{r_p} } = \dot{ \vec{r} }_{\Sigma_1} +
						\dot{ R_0^1 } \vec{r_p}^{(1)}
					= \dot{ \vec{r} }_{\Sigma_1} +
					R_0^1 \ ( \vec{\omega}^{(1)} \times \vec{r_p}^{(1)} )
					= R_0^1 \ ( \vec{v}_{\Sigma_1}^{(1)} + \vec{\omega}^{(1)} \times \vec{r_p}^{(1)} )
\label{eq:linear3DVelocity}
\end{equation}

Dónde, $\vec{\omega}^{(1)}$, es un vector en $\mathbb{R}^{3}$ que simboliza la velocidad
angular orientada en terminos del referencial local $\Sigma_1$, y $\vec{v}_{\Sigma_1}^{(1)}$,
es un vector en $\mathbb{R}^3$ que representa la velocidad lineal del referencial local
$\Sigma_1$, orientada en términos del mísmo referencial local $\Sigma_1$, que se puede calcular
como:

$$
	\vec{v}^{(1)} = R_1^0 \ \dot{ \vec{ r } }_{\Sigma_1}
$$

Dado que la matriz de rotación cumple que $R_1^0 = {R_0^1}^{-1} = {R_0^1}^{T}$.

Por último, a partir de la expresión \ref{eq:linear3DVelocity},
la aceleración de la partícula $p$, se puede calcular como:

$$
	\ddot{ \vec{r_p} } =
					R_0^1 \ ( \dot{\vec{v}}_{\Sigma_1}^{(1)} + \dot{\vec{\omega}}^{(1)} \times \vec{r_p}^{(1)} ) +
					R_0^1 \ [\vec{\omega}^{(1)}\times] ( \vec{v}_{\Sigma_1}^{(1)} + \vec{\omega}^{(1)} \times \vec{r_p}^{(1)} )
$$
\begin{equation}
\ddot{ \vec{r_p} } = R_0^1 \biggr(
	\dot{\vec{v}}^{(1)} + \dot{\vec{\omega}}^{(1)}\times \vec{r_p}^{(1)}
	+ \vec{\omega}^{(1)} \times \vec{v}_{\Sigma_1}^{(1)} + \vec{\omega}^{(1)} \times ( \vec{\omega}^{(1)} \times \vec{r_p}^{(1)} )
\biggr )
\label{eq:linear3DAcceleration}
\end{equation}

La demostración y los detalles analíticos de las ecuaciones de velocidad y aceleración,
así como el análisis completo sobre las matrices de rotación
pueden encontrarse en el libro \cite{3dMotionOfRigidBodies}.

\subsection{Dinámica de cuerpos rígidos.}

Cuando un cuerpo se mueve en el espacio, éste movimiento se descompone en traslación y rotación,
la energía cinética resultante es la integración de la energía cinética de cada una de sus partículas
como se expresa a continuación:

$$
K = \frac{1}{2} \int_B || \dot{\vec{r_p}} ||^2 dm
$$

Si se recupera la ecuación \ref{eq:linear3DVelocity}, y se toma en cuenta que la energía cinética
no depende de la orientación (y por lo tanto, no depende del referencial \cite{3dMotionOfRigidBodies}),
la ecuación anterior se puede reescribir como:

$$
	K = \frac{1}{2} \int_B
	R_0^1 \ ( \vec{v}_{\Sigma_1}^{(1)} + \vec{\omega}^{(1)} \times \vec{r_p}^{(1)} )
	dm
	= \frac{1}{2} \int_B
	\ ( \vec{v}_{\Sigma_1}^{(1)} + \vec{\omega}^{(1)} \times \vec{r_p}^{(1)} )
	dm
$$

El resultado final es la ecuación \ref{eq:kineticEnergyNonCM}, cuyo referencial no inercial,
o referencial local se ubica en algún punto del cuerpo, fuera del centro de masa.

\begin{equation}
	K = \frac{1}{2} \vec{v}^\mathsmaller{\ T} \vec{v} m
	- \vec{v}^\mathsmaller{\ T} m[\vec{r_c}\times] \vec{\omega}
	+ \frac{1}{2}\vec{\omega}^\mathsmaller{\ T} I \vec{\omega}
\label{eq:kineticEnergyNonCM}
\end{equation}

El centro de masa, por su parte, se puede calcular como el efecto de la gravedad
sobre cada una de las partículas con masa que conforman el cuerpo rígido,
cuya integral conlleva a la expresión \ref{eq:centerOfMass}, dónde puede
observarse que el centro de masa no es nada más que el promedio ponderado sobre
la distribución de masa del cuerpo $B$ \cite{3dMotionOfRigidBodies}.

\begin{equation}
	\vec{r}_c^{\ (1)} \triangleq \frac{1}{m} \int_B \vec{r}_p^{\ (1)} dm
\label{eq:centerOfMass}
\end{equation}

Cuando la referencia local se ubica en el centro de masa, entonces $\vec{r}_c = 0$ y
la ecuación \ref{eq:kineticEnergyNonCM}, puede expresarse como:

\begin{equation}
	K = \frac{1}{2} \vec{v}^\mathsmaller{\ T} \vec{v} m
		+ \frac{1}{2}\vec{\omega}^\mathsmaller{\ T} I \vec{\omega}
\label{eq:kineticEnergyCM}
\end{equation}

Tanto la ecuación \ref{eq:kineticEnergyNonCM}, como la ecuación \ref{eq:kineticEnergyCM},
están en términos de la velocidad lineal, $\vec{v}$, del referencial local,
la velocidad angular, $\vec{\omega}$, del referencial local,
la masa total del cuerpo, $m$, y la matriz de inercia del cuerpo, $I$.

La matriz de inercia del cuerpo, es el resultado de aumentar el tensor de inercia
del cuerpo en un factor, que a su vez, está en términos de la distancia del eje de rotación al centro de masa,
como sigue:

\begin{equation}
	I = I_c - m[\vec{r}_c\times]^2
	\label{eq:inertiaMatrix}
\end{equation}

Dónde el tensor de inercia, $I_c$, se define como:

\begin{equation}
	I_c \triangleq - \int_B [\vec{r}_p^{\ (1)}\times]^2 dm > 0 \in \mathbb{R}^{3\times3} 
	\label{eq:inertiaTensor}
\end{equation}

Y el componente $[\vec{r}_c\times]^2$, que es el cuadrado de la \textit{matriz operador producto cruz}
del vector $r_c$, que se escribe como:

\begin{equation}
[\vec{r}_c\times]^2 =
\begin{pmatrix}
	- ( r^2_{cy} + r^2_{cz} ) & r_{cx}r_{cy} & r_{cx}r_{cz}\\
	r_{cx}r_{cy} & - ( r^2_{cx} + r^2_{cz} ) & r_{cy}r_{cz}\\
	r_{cx}r_{cz} & r_{cy}r_{cz} &  - ( r^2_{cx} + r^2_{cy} ) \\
\end{pmatrix}
\end{equation}

La matriz operador producto cruz (cross product matrix, en inglés) del vector $\vec{a}$,
es la representación del vector $\vec{a}$ como una matriz antisimétrica, que, multiplicada
con un vector $\vec{b}$, es equivalente al producto vectorial del vector $\vec{a}$ 
por el vector $\vec{b}$, como sigue:

$$
	\vec{a}\times\vec{b} = [\vec{a}\times]\vec{b}
$$

\begin{equation}
[\vec{a}\times] \triangleq
\begin{pmatrix}
	0 & -a_z & a_y \\
	a_z & 0 & -a_x \\
	-a_y & a_x & 0 \\
\end{pmatrix}
\end{equation}

La base de álgebra lineal y los operadores de ésta sección se pueden encontrar en los primeros
capítulos de la referencia \cite{3dMotionOfRigidBodies}.

\subsection{Formulación Lagrangiana.}

%Ecuación dinámica para un cuerpo rígido en un campo de fuerzas conservativo
%y fuerzas de fricción.

Si se reemplaza la expresión \ref{eq:kineticEnergyJacobian} en la ecuación \ref{eq:dalembertLagrange},
como se muestra en el libro \cite{3dMotionOfRigidBodies}, se llega a la expresión de fuerzas inerciales
\ref{eq:inertialForcesLagrange}, con la que tiene lugar la definición del torque de Coriolis \ref{eq:tcor}

\begin{equation}
	H(\vec{q})\ddot{\vec{q}}
	+ \dot{H}(\vec{q},\dot{\vec{q}})\dot{\vec{q}}
	- \frac{1}{2}
	\frac{\partial\{
		\dot{\vec{q}}^{\ T}H(\vec{q}) \dot{\vec{q}}
	\}}
	{\partial\vec{q}}
	= Q
	\label{eq:inertialForcesLagrange}
\end{equation}

\begin{equation}
	\tau_{cor}
	\triangleq
	\dot{H}(\vec{q},\dot{\vec{q}})\dot{\vec{q}}
	- \frac{1}{2}
	\frac{\partial\{
		\dot{\vec{q}}^{\ T}H(\vec{q}) \dot{\vec{q}}
	\}}
	{\partial\vec{q}}
	= C(\vec{q},\dot{\vec{q}})\dot{\vec{q}}
	\label{eq:tcor}
\end{equation}

La expresión $\tau_{cor}$ se denomina \textit{vector de Coriolis}, y el término $C(\vec{q},\dot{\vec{q}})$,
se denomina \textit{matriz de Coriolis}, y dan como resultado la expresión de las fuerzas
inerciales del sistema \ref{eq:inertialForces}.

\begin{equation}
	H(\vec{q})\ddot{\vec{q}}
	+ 
	C(\vec{q},\dot{\vec{q}})\dot{\vec{q}}
	= Q
	\label{eq:inertialForces}
\end{equation}

Se puede descomponer a la variable $Q$, de fuerzas generalizadas,
para conocer algunos componentes externos
al sistema y sus efectos sobre el mísmo. En el libro \cite{3dMotionOfRigidBodies},
en capítulo 2 se desarrolla a $Q$ como la suma de los torques: $\tau_U$, torque debido a un
campo conservativo, ecuación \ref{eq:torqueu}, $\tau_D$, torque debido a fricción, ecuación \ref{eq:torqued},
y $\tau_E$, para asignar a cualquier otro torque externo, ecuación \ref{eq:torquee}.

\begin{equation}
	- \tau_{U}
	=
	-\frac{\partial U(\vec{q})}{\partial \vec{q} }
	\triangleq
	-\vec{g}(\vec{q})
	\label{eq:torqueu}
\end{equation}

\begin{equation}
	- \tau_{D}
	=
	-\frac{\partial \mathcal{R}(\vec{q},\dot{\vec{q}}) }
	{\partial \dot{\vec{q}}}
	=
	-D(\vec{q},\dot{\vec{q}})\dot{\vec{q}}
	\label{eq:torqued}
\end{equation}

\begin{equation}
	\tau_{E}
	=
	\tau + \sum_{k=1}^{r} J_k^{\ T}(\vec{q})\vec{f_{ck}}
	\label{eq:torquee}
\end{equation}

Finalmente, reemplazando las ecuaciones \ref{eq:torqueu}, \ref{eq:torqued} y \ref{eq:torquee}
en la ecuación \ref{eq:inertialForces}, se tiene la \textit{ecuación D'Alembert-Lagrange general}
\ref{eq:generalDalembertLagrange}, para campos conservativos.

\begin{equation}
	H(\vec{q})\ddot{\vec{q}} + C(\vec{q},\dot{\vec{q}})\dot{\vec{q}} + D(\vec{q},\dot{\vec{q}})\dot{\vec{q}} + g(\vec{q}) = \tau_E
	\label{eq:generalDalembertLagrange}
\end{equation}

La ecuación \ref{eq:generalDalembertLagrange} puede expresarse también como la ecuación 
\ref{eq:classicalGeneralDalembertLagrange}.

\begin{equation}
	\frac{d}{dt}\frac{\partial K(\vec{q},\dot{\vec{q}})}
	{\partial{\dot{\vec{q}}}}
	- \frac{\partial \{K(\vec{q},\dot{\vec{q}}) - U(\vec{q})\}}
	{\partial{\vec{q}}}
	+ \frac{ \mathcal{R}(\vec{q},\dot{\vec{q}}) }
	{\partial{\dot{\vec{q}}}}
	=
	\tau_E
	\label{eq:classicalGeneralDalembertLagrange}
\end{equation}

Los detalles sobre las fuerzas generalizadas y la formulación Lagrangiana se pueden profundizar con más detalle en la bibliografía
\cite{3dMotionOfRigidBodies}.
