\renewcommand{\thepage}{\arabic{page}}

\chapter{Marco Teórico}

\section{Mecánica clásica.}

La mecánica clásica, es la rama de la física  que estudia las leyes del
comportamiento de cuerpos físicos macroscópicos en reposo y a velocidades
pequeñas comparadas a la velocidad de la luz.
Se podría decir que la mecánica clásica se divide en tres campos de estudio,
la \textit{estática}, la \textit{cinemática} y la \textit{dinámica}.

Por un lado, la estática estudia los cuerpos en reposo
y por otro, la cinemática y la dinámica estudian los cuerpos en movimiento,
aunque bajo diferentes enfoques.
La cinemática estudia el movimiento de los cuerpos sin consideración de las
fuerzas que lo provocan. La dinámica estudia las fuerzas que producen el
movimiento de un cuerpo.

Los fundamentos de la dinámica son las tres leyes de Newton:
\begin{itemize}
	\item{La ley de inercia.}
	\item{La ley de la interacción y la fuerza}, y por último
	\item{la ley de acción-reacción.}
\end{itemize}

La dinámica estudia a los cuerpos como una partícula centro de masa,
o como un conjunto de partículas que interactuan entre sí.
De ésta manera, un cuerpo sólido es un cuerpo cuyo conjunto de particulas
que lo conforman tienen separación y forma constante. Aunque en la naturaleza,
todos los cuerpos se deforman por cambios de temperatura, presión y/o fuerzas
externas, en casos prácticos es posible simplificar el análisis suponiendo
lo contrario.

La primera ley de Newton dicta:

\textit{Toda partícula conserva su estado de reposo o velocidad
constante (a partir de un marco de referencia inercial) a menos
que una fuerza sea aplicada.}

Para que un punto en el espacio pueda ser considerado un marco
de referencia inercial, éste debe tener aceleración nula.
Lo que permite que la variación del momento lineal de una partícula sea
igual a las fuerzas reales sobre la mísma. Lo que nos lleva a la segunda
ley de Newton:

\textit{El cambio del estado de movimiento de una partícula está dada por
el cambio de su momento, siendo directamente proporcional 
a las fuerzas motrices impresas.}

Sea $p$ el momento de una partícula de masa $m$ y velocidad
$\vec{v} \in \mathbb{R}^3$
tal que $\vec{p} = m\vec{v}$, entonces, la segunda ley de Newton indica que la
fuerza puede ser descrita como muestra la ecuación \ref{eq:newton2ndLaw}.

\begin{equation}
	\dot{\vec{p}} = m\dot{\vec{v}} = m\vec{a} = \vec{f}
	\label{eq:newton2ndLaw}
\end{equation}

Por último, la tercer ley de Newton dice:

\textit{A toda acción le corresponde una reacción igual pero en sentido
contrario: lo que quiere decir que las acciones mutuas de dos cuerpos siempre
son iguales y dirigidas en sentido opuesto.}


\subsection{El principio de mínima acción.}

Un cuerpo tiene energía cuando es capaz de realizar un trabajo.
En los sistemas mecánicos se identifican dos tipos de energía:
la \textit{energía cinética} y la \textit{energía potencíal}.

El trabajo $W_{AB}$ realizado por una partícula para desplazarse desde un
punto $A$ hasta un punto $B$ (figura \ref{fig:displacement}) se calcula como muestra la ecuación
\ref{eq:workprinciple1}.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{./00-Resources/02-Figures/work}
	\caption{Desplazamiento de una partícula}
	\label{fig:displacement}
\end{figure}

Dado que el trabajo de una fuerza de acción sobre una partícula depende del
trayecto de desplazamiento entre un punto A y un punto B,
éste será diferente para cada trayecto. Sin embargo, además de todos
los posibles desplazamientos que una partícula puede seguir,
sólo exíste uno que minimizará la acción (el trabajo), y ésta será la trayectoria
que la partícula seguirá de forma fortuita. A esta afirmación se le conoce
como \textit{principio de mínima acción}. A cualquier otro desplazamiento
posible se le denomina \textit{desplazamiento virtual}, y al trabajo realizado
por la fuerza teórica que realiza el desplazamiento virtual, se le conoce como
\textit{trabajo virtual}.

\begin{equation}
	W_{AB} = \int^{B}_{A}{ \vec{f} \cdot d\vec{s} }
	\label{eq:workprinciple1}
\end{equation}

Usando la ecuación \ref{eq:newton2ndLaw} en la ecuación \ref{eq:workprinciple1},
se tiene que:

\begin{equation}
	W_{AB} = \int^{B}_{A}{ \dot{\vec{p}} \cdot d\vec{s} }
	= \int^{B}_{A}{ \frac{d\vec{p}}{dt} \cdot d\vec{s} }
	= \int^{B}_{A}{ \frac{d\vec{s}}{dt} \cdot d\vec{p} }
	= \int^{B}_{A}{ \vec{v} \cdot d\vec{p} }
	= m \int^{B}_{A}{ \vec{v} \cdot d\vec{v} }
	= \frac{m}{2} ( \vec{v}^T \cdot \vec{v} ) |_{A}^{B}
	\label{eq:workprinciple2}
\end{equation}

De la ecuación \ref{eq:workprinciple2} el término energía cinética $K$
para una partícula de masa $m$ se consigue como muestra la ecuación
\ref{eq:kineticEnergy}.

\begin{equation}
	K = \frac{m}{2} ( \vec{v}^T \cdot \vec{v} )
	\label{eq:kineticEnergy}
\end{equation}

La energía cinética depende de la posición de la partícula y su velocidad,
siendo ésta energía  $ K = \frac{m}{2} v^2 $, para una partícula con velocidad
lineal en una dimensión ($\mathbb{R}$).

Observese que
$$
\frac{d}{dt} \frac{ \partial K }{ \partial \vec{v} }
= \dot{ \vec{p} }
= m\vec{a}
$$
Si se llama fuerza inercial ($f_I$) a la derivada temporal de la
derivada parcial de $K$ con respecto a la velocidad de la partícula,
se tiene que
\begin{equation}
	f_I
	= - \frac{d}{dt} \frac{ \partial K }{ \partial \vec{v} }
	= - \dot{ p }
	= - ma
\label{eq:inertialForce}
\end{equation}
entonces, se puede reescribir la ecuación \ref{eq:newton2ndLaw} como sigue

\begin{equation}
	f + f_I = f - \frac{d}{dt} \frac{ \partial K }{ \partial \vec{v} } = 0
	\label{eq:dalembertNewton}
\end{equation}

Por otro lado, si el trabajo $W_{AB}$ no depende del trayecto
que sigue la partícula, entonces, si se considera una trayectoria
cerrada $C$ para la partícula, de forma que ésta se traslade desde
un punto $A$ hasta un punto $B$ y de vuelta al punto $A$ por otro
trayecto estrictamente diferente, el trabajo total $W$ es 0,
lo que implica la existencia de un campo de fuerzas conservativo
en el espacio que ocupan esos puntos y la partícula.
Dicho campo de fuerzas conservativo conlleva a la existencia de una función
escalar $U$ de tal  manera que el campo de fuerzas se deduce como la ecuación
\ref{eq:potentialEnergy} 
\cite{3dMotionOfRigidBodies}.

\begin{equation}
	\vec{f}(\vec{d}) = - \nabla U(\vec{d})
	\equiv
	\vec{f}(\vec{d}) = - \frac{ \partial U }{ \partial \vec{ d } }
	= - \frac{ \partial U }{ \partial \vec{ s } }
\label{eq:potentialEnergy}
\end{equation}

Donde $d = x\hat{i} + y\hat{j} + z\hat{k}$, prepresenta la posición de la
partícula en el sistema referencial ubicado en $0\hat{i} + 0\hat{j} + 0\hat{k}$.

Usando la ecuación \ref{eq:potentialEnergy} en la ecuación \ref{eq:workprinciple1},
se obtiene la expresión del trabajo realizado por la partícula en su trayectoria $AB$
en la ecuación \ref{eq:workPotentialEnergy}.

\begin{equation}
	W_{AB} = - \int^{B}_{A}{ \frac{ \partial U }{ \partial \vec{ s } } \cdot d\vec{s} }
	= U_A - U_B
\label{eq:workPotentialEnergy}
\end{equation}

De esta manera, se denomina \textit{energía potencial} a la función escalar $U$.
La energía total de la partícula es entonces $E = U+K$ de forma tal que

$$
	W_{AB} = (K_B + U_A) - (K_A + U_B)
$$

\subsection{Principio de D'Alembert.}

El principio de D'Alembert dicta que:
\textit{el trabajo virtual total realizado por las fuerzas impresas
(incluyendo las de restricción), aumentadas en las fuerzas de inercia,
se desvanece por el desplazamiento virtual} \cite{3dMotionOfRigidBodies}
y se puede expresar como la ecuación \ref{eq:dalembertPrinciple}.

\begin{equation}
	( \vec{f} + \vec{f_I} ) \cdot \delta \vec{d} = 0
\label{eq:dalembertPrinciple}
\end{equation}

Donde $\delta \vec{d}$ es el desplazamiento virtual de la partícula.

Una partícula se acelera debido a fuerzas efectivas ($\vec{f_e}$) impresas sobre ella,
y se desacelera por aquellas fuerzas que limitan su desplazamiento
para ciertas configuraciones, denominadas fuerzas de restricción $\vec{f_r}$,
ambas fuerzas tienen efecto sobre la partícula en forma de una fuerza total $\vec{f}$.
Esto nos permite reescribir la ecuación \ref{eq:dalembertNewton} como

$$
\vec{f} + \vec{f_I} = (\vec{f_e} + \vec{f_r} ) + \vec{f_I}
$$}

De esta menera, en combinación con el principio del trabajo virtual,
el principio de D'Alembert se puede calcular usando exclusivamente
las fuerzas efectivas despreciando las fuerzas de restricción como sigue:

$$
	( \vec{f_e} + \vec{f_I} ) \cdot \delta \vec{d} = 0
$$

Por último, existen cuatro tipo de fuerzas que conforman a la fuerza total
resultante $\vec{f}$.

\begin{itemize}
	\item{La fuerza inercial $\vec{f_I}$ ecuación \ref{eq:inertialForce}.}
	\item{Las fuerzas conservativas $\vec{f_U}$ ecuación \ref{eq:potentialEnergy}.}
	\item{Las fuerzas restrictivas o de restricción $\vec{f_r}$.}
	\item{Las fuerzas disipativas o de fricción $\vec{f_d}$.} Y por último,
	\item{las fuerzas exógenas $\vec{f_E}$, que son todas aquellas que agregan energía al sistema.}
\end{itemize}

\subsection{Mecánica Lagrangiana.}

Cuando se considera un sistema de múltiples partículas, ya sea de
un único cuerpo o múltiples cuerpos, caractérizar el comportamiento
utilizando las ecuaciones anteriores para cada partícula complica
el trabajo.
Nuestro sistema de partículas puede ser expresado de forma más
sencilla en términos de las energías del movimiento permitido 
para las partículas y la configuración del sistema, utilizando
\textit{coordenadas generalizadas} de la siguiente manera:

$$
\vec{q} = (q_1,q_2,\dots,q_n)^T
$$

Las coordenadas generalizadas representadas por $\vec{q}$, son un arreglo
de las $n$ variables mínimas necesarias (linearmente independientes) que 
permiten definir la configuración del sistema.
Cabe mencionar que $\vec{q}$ no es un vector único.

De esta manera, la posición de cada particula dentro del sistema de $N$ partículas,
se puede obtener a partir de éstas coordenadas generalizadas y el tiempo.
Por ejemplo, la posición $\vec{d}$ de la partícula $j$ con respecto al marco de
referencias inercial $\Sigma_0$ es:

$$
	\vec{d}_j = f( q_1 , q_2 , \dots , q_n , t )
	= d_j ( \vec{q} , t ) \in \mathbb{R}^3
$$

Donde $d_j$ es la función que convierte las coordenadas generalizadas $\vec{q}$
y el tiempo en una posición espacial para la partícula $j$.

Por su parte, $\dot{ \vec{ q } }$, se denomina velocidad generalizada
tal que

$$
	\dot{\vec{q}} = ( \dot{q_1} , \dot{q_2} , \dots , \dot{ q_n } )^T
$$

La matriz Jacobiana $J$ de la partícula $j$ es la derivada parcial del vector
$\vec{d_j}$ con respecto a las coordenadas generalizadas $\vec{q}$ de tal
forma que

\begin{equation}
	J_j(\vec{q}) = \frac{\partial\vec{d_j}}{\partial{\vec{q}}}
	\in \mathbb{R}^{3xn}
	\label{eq:Jacobian}
\end{equation}

Usando la matriz Jacobiana de la partícula $j$, se puede
expresar la velocidad de ésta misma partícula como

$$
	\vec{v_j} = J_j(\vec{q}) \dot{\vec{q}}
$$

Ahora bien, la energía cinética total del sistema puede
expresarse a través de las coordenadas generalizadas
y las velocidades generalizadas, como se muestra en la
ecuación \ref{eq:kinetic1}.

\begin{equation}
	K(\vec{q},\dot{\vec{q}}) = \frac{1}{2} \sum_{j=1}^{N} m_j \|\dot{\vec{d}}\|^2
	= \frac{1}{2} \sum_{j=1}^{N} m_j ( \dot{\vec{d_j}} \cdot \dot{\vec{d_j}} )
\label{eq:kinetic1}
\end{equation}

O usando la ecuación \ref{eq:Jacobian} en su lugar
se tiene que

\begin{equation}
	K( \vec{q}, \dot{\vec{q}} ) =
	\frac{1}{2} \dot{\vec{q}}^T H( \vec{q} ) \dot{\vec{q}}
\label{eq:kineticEnergyJacobian}
\end{equation}

Donde $H(\vec{q})$ se denomina \textit{matriz de inercia},
y se define como:

\begin{equation}
	H(\vec{q}) 
	\triangleq 
	\sum_{j=1}^{N} m_j J_j^T( \vec{q} ) J_j( \vec{q} )
	\in \mathbb{R}^{nxn}
\label{eq:inertialMatrix}
\end{equation}

Existe otra vector conocido como \texit{fuerzas generalizadas}
$Q \in \mathbb{R}^n$, es la suma de las fuerzas exógenas del sistema
como sigue

\begin{equation}
	Q = \sum_{j=1}^{N} J_j^T( \vec{q} ) \vec{f_e}
\label{eq:generalizedForces}
\end{equation}

Aplicando el principio de mínima acción en terminos de 
las coordenadas generalizadas, se puede llegar a la expresión
conocida como \textit{ecuación D'Alembert-Lagrange}
\cite{3dMotionOfRigidBodies} que se expresa en la ecuación
\ref{eq:dalembertLagrange}.

\begin{equation}
	\frac{d}{dt} \frac{ \partial K }{ \partial \dot{ \vec{q} } }
	- \frac{ \partial K }{ \partial \vec{q} }
	= Q
\label{eq:dalembertLagrange}
\end{equation}

\subsection{Dinámica de cuerpos rígidos.}

\textbf{Escribir sobre la dinámica de cuerpos rígidos}
