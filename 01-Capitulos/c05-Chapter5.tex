\renewcommand{\thepage}{\arabic{page}}

\chapter{Implementación física.}

En éste capítulo se exponen los sensores principales del robot, los actuadores del robot,
el microcontrolador que ejecuta el firmware de control,
la arquitectura de software sobre la que se desarrollará el sistema de control y 
el sistema de comunicación que permite muestrear y manipular el estado
del controlador.

\section{Marco técnico.}

En ésta sección se entrega un marco de referencia a nivel técnico para comprender algunos conceptos
utilizados en la implementación, cuya teoría no se encuentra en el capítulo \textit{marco teórico}.
El objetivo no es profundizar cada concepto, sino introducirlo para que el lector pueda entenderlo
de forma general cuando se mencionen en el desarrollo de éste capítulo.

\subsection{Microcontrolador.}

Un microcontrolador, es un circuito integrado principalmente digital. Es en escencia una computadora completa,
pues cuenta internamente con el CPU, la memoria principal, una memoria ROM
y los periféricos mínimos para comunicarse con el exterior. El principal
propósito de un microcontrolador no es realizar cargas de trabajo computacional, sino gobernar actuadores
y/o leer y preprocesar datos de un sensor, ya sea que éstos datos viajen hasta un ordenador externo, o sirvan para
controlar de forma inteligente un proceso. De ahí el nombre de microcontrolador, alusión a su tamaño
y propósito.

Un microcontrolador es un ordenador de propósito específico, la elección de éste depende siempre del proyecto
a desarrollar. De ésta manera existen cientos de modelos fabricados por varias marcas, entre ellas
\textit{Texas Instruments}. Cuando un microcontrolador tiene el propósito de procesar señales, entonces
también lleva el nombre de DSP (del inglés \textit{digital signal processor}).

Según la marca y modelo, el uC (abreviación de microcontrolador), tendrá periféricos para procesar datos,
o para controlar sistemas externos, éstos periféricos tienen una interfaz con el CPU principal única
entre modelos y fabricantes, por esta razón, antes de poder utilizar el microcontrolador, es necesario
construir un software de control para cada periférico interno a partir del cual se puedan construir
los controladores para los sistemas externos, éste software no tiene ningúna garantía de compatibilidad
entre microcontroladores y muchas veces se desarrolla en lenguaje ensamblador o en lenguaje C/C++.

Algunos uC tienen una arquitectura excelente para compartir el tiempo de ejecución entre varios
procesos, lo que permite el desarrollo de sistemas de administración de recursos denominados sistemas
operativos.

Cualquier software desarrollado para un microcontrolador se denomina firmware ó soporte lógico
inalterable, pues una vez que éste software ha sido descargado en la memoria ROM del dispositivo,
reemplazarlo implica una alteración física del sistema, generalmente se requieren conexiones
eléctricas a lineas de comunicación dedicadas (conocidas como interfaz de programación).
Si la arquitectura lo soporta, el CPU también puede reescribir sobre la ROM de programa,
permitiendo el desarrollo de firmware de autoprogramación denominado boot loader.

\subsection{Sistema operativo en tiempo real.}

Un sistema operativo, es un software con el objetivo de administrar un conjunto de recursos
computacionales, que al mísmo tiempo sirve como herramienta para construír aplicaciones sobre
dichos recursos. Si el software, además de administrar recursos del dispositivo, provee interfaces\footnote{Una interfaz
de software, es un conjunto de funciones cuyos argumentos y salidas están bien definidas y son inalterables.}
para administrar el tiempo de ejecución y/o medirlo, éste sistema operativo también se denomina
sistema operativo en tiempo real.

Los sistemas operativos en tiempo real pueden depender completamente del hardware o pueden
ser un conjunto de interfaces de software y estructuras de datos generales para administrar de forma
lógica los recursos, como es el caso del RTOS (del inglés \textit{real time operative system})
\textit{FreeRTOS}, que gracias a su implementación completamente en lenguaje C, permite compartir
la arquitectura de software entre arquitecturas de hardware, siempre que dicho hardware
cumpla con algunos requisitos mínimos.

Un firmware en tiempo real, por su parte, es cualquier software embebido\footnote{También denominado software empotrado,
es cualquier firmware para microcontrolador.} cuya ejecución depende de alguna manera, del tiempo en múltiplos
de segundos (de ahí el nombre de tiempo real), que puede o no, estar desarrollado sobre un RTOS.

\subsection{Controlador de periférico.}

Cualquier hardware que pueda ser gobernado desde un CPU, depende de una interfaz de comunicación
con dicho CPU que permita intercambiar datos entre ambos, y además, una especificación del modelo
de los datos que pueden intercambiar (muchas veces la frecuencia con la que pueden ser accedidos,
también es importante, sobre todo para sistemas en tiempo real), éste hardware se denomina periférico.

Un periférico puede ser interno, si dicha interfaz de comunicación es a través del bus principal del CPU.
O puede ser externo, si se conecta a través de un periférico interno de la computadora.

Un periférico, independientemente de si es interno o externo, depende completamente de su
modelo de datos para operar, pues es en ésta especificación en la que se describen las acciones
y los resultados de las acciones que dicho periférico es capaz de realizar. Así que, todo
periférico necesita un software que indique al CPU de que forma acceder a los datos
del periférico y/o enviar comandos de acción. Éste software se denomina controlador o driver (en inglés).

Se podría decir que toda computadora es un sistema distribuído, pues mientras que el CPU realiza las
operaciones lógicas y cálculos matemáticos, los datos resultantes se traducen en
información o acción interpretada por un periférico, que a su vez, puede ser un CPU de función específica.

\subsection{Bus IIC.}

El bus IIC es una interfaz de comunicación para periféricos externos, consiste en dos lineas
cuyas señales son completamente digitales. Las lineas se denominan \textit{datos} y \textit{reloj}.
La señal de reloj envia un tren de pulsos cuya separación temporal (periodo) es idéntica,
e indica la velocidad de transmisión de símbolos. Mientras tanto, la señal de datos,
se dedica a enviar una serie de pulsos cuya duración es determinada por los pulsos del reloj
y cuyo valor en voltaje depende de si se envía un símbolo lógico alto o bajo.

En un bus IIC se pueden conectar más de un periférico, y cada uno de ellos puede enviar
datos por el bus bajo la petición explícita del CPU. A éste tipo de comunicación se le
denomina maestro-esclavo.

Para que el maestro solicite una transacción, primero tiene que enviar una dirección lógica
(un número), mísma que debe coincidir con la dirección del dispositivo esclavo, seguido
de los datos que el dispositivo soporta, indicado en su especificación.

Debido a que sólo existe una linea de datos, ésta interfaz de comunicación sólo
permite salidas o entradas, pero no ambas al mísmo tiempo. A éste tipo de comunicación
se le conoce como \textit{half duplex}.

\subsection{Bus UART.}

El bus UART (del inglés \textit{universal asynchronous receiver-transmitter},
también denominado puerto serie),
es un bus de comunicación digital de tipo asíncrono y full duplex. El primer término
indica que no existe una linea dedicada a señalar el reloj, y el segundo que éste bus
es capaz de transmitir y recibir datos al mísmo tiempo. Por otro lado, en contraste
con el bus IIC, éste bus se diseñó para conectar dos dispositivos de punto a punto
(no es compartido).

Para que dos dispositivos se puedan comunicar a través del bus UART,
es necesario que ambos soporten exáctamente la mísma velocidad de transacción
de símbolos (denominado también \textit{baudios}) y exáctamente la mísma
codificación de símbolos.

Éste bus soporta diferentes voltajes y varias lineas, aunque puede encontrarse
en casi cualquier microcontrolador, con un voltaje de 3.3 volts ó 5 volts y únicamente
dos lineas de comunicación, \textit{transmisión} y \textit{recepción}.
Ambas lineas realizan transacción de datos en forma de un tren de pulsos que salen de
la linea de transmisión del dispositivo transmisor y entran por la linea de recepción
del dispositivo receptor, permitiendo a ambos dispositivos transmitir y recibir
al mísmo tiempo.

\subsection{Puente bluetooth serial.}

Un dispositivo puente de comunicación, tiene el propósito de almacenar
los datos recibidos por un canal y retransmitirlos por otro canal, donde
ambos canales pueden ser el mísmo tipo de interfaz de comunicaicón o
pueden ser diferentes.

Un puente bluetooth serial, es un dispositivo que en uno de sus canales
soporta comunicación bluetooth y en el otro extremo, cuenta con lineas de comunicación
UART. Su funcionamiento se basa en construir en el canal bluetooth, una interfaz
para configurar los baudios del canal UART y otra para almacenar los datos
entrantes y salientes del canal UART, mientras que, en el dispositivo
bluetooth externo, debe haber un controlador que traduzca dichas interfaces en
un puerto serie virtual. Cualquier dato recibido por el canal UART es retransmitido
por bluetooth a través de la interfaz de datos, mientras que, cualquier
dato recibido por la interfaz de datos bluetooth, es retransmitido por el puerto
serie.

\subsection{Filtro complementario para IMU.}

Un filtro complementario para IMU, es un filtro digital cuya salida resulta de la suma ponderada
de los ángulos de orientación computados por un lado a partir de la información del acelerómetro,
y por el otro, la información del giroscópio. Ésta alternativa de filtro digital resulta
ser más simple y suficientemente efectiva para el proyecto del robot péndulo invertido,
como se muestra en la bibliografía \cite{complementaryFilter1,complementaryFilter2}.

\begin{equation}
	\epsilon_{n+1} = k(\epsilon_{n} + \epsilon_{n_g}) + (1-k)(\epsilon_{n_a})
	\ |\ 
	k \in \mathbb{R}, 0 \leq k \leq 1
\label{eq:complementaryFilter}
\end{equation}

La expresión \ref{eq:complementaryFilter}, muestra al filtro complementario como un
sistema discreto lineal e invariante en el tiempo, dónde $\epsilon_{n_a}$, es el
ángulo estimado apartir de las aceleraciones, la variable $\epsilon_{n_g}$, es el ángulo
estimado apartir de las velocidades angulares, $\epsilon_{n}$, es el valor actual del
ángulo y $k$ es la ganancia del filtro.

\subsection{Señal PWM.}

Una señal PWM (del inglés, \textit{pulse-width modulation}), es un tren de pulsos,
cuya separación temporal está definida por un periodo de operación $T_o$, pero la longitud
temporal de cada pulso dentro de la ventana temporal de operación
depende de un porcentaje de trabajo. Cuando el porcentaje de trabajo
es 100\%, el pulso tiene una longitud temporal equivalente al periodo de operación,
por el contrario, será un porcentaje del periodo de operación,
como muestra la figura \ref{fig:pwmSim}.


\begin{figure}[H]
	\centering
	\includegraphics[scale=0.3]{./00-Resources/02-Figures/c518-pwm}
	\caption{Simulación de una señal PWM con periodo $T_o=0.010$ segundos y porcentaje de trabajo del 25\%.}
	\label{fig:pwmSim}
\end{figure}

\section{Sensores y actuadores.}

\subsection{Unidad de medición de inercia.}

El sensor de inercia también conocido como IMU (por sus siglas en inglés de \textit{inertial measurement unit}),
es un dispositivo electrónico de tipo MEM (del inglés \textit{microelectromechanical}) que permite calcular
la orientación del robot RAP.
El IMU instalado en el robot es de la marca \textit{InvenSense} y modelo \textit{MPU-9250}, capaz de medir
la acelaración, la velocidad angular y el campo magnético en unidades de: \textit{fuerza g}\footnote{La \textit{fuerza
g} técnicamente no es una fuerza, sino una medida de aceleración dónde cada unidad corresponde a la aceleración
de la gravedad terrestre.},
\textit{radianes por segundo} y \textit{micro tesla}. Respectivamente.
Éste IMU tine una interfaz completamente digital IIC (del inglés \textit{Inter-Integrated Circuit}) para configuración
y medición, posee además una linea digital para enviar interrupciones al procesador para indicar
que los datos se han actualizado.

La orientación del IMU en la placa de control del robot, no coincide con la orientación del referencial local
del robot expuesto en el diagrama de cuerpos \ref{fig:c31-freebodies}, el IMU,
ubicado dentro del recuadro amaríllo de la figura \ref{fig:c32-electric}, tiene una orientación
en términos de los ejeces del referencial $\Sigma'$ tal que así:

$$
	x_{imu} = -z';\ 
	y_{imu} = -x';\ 
	z_{imu} = -y'
$$

Éste desface de orientación se resuelve por software.
Para calcular el ángulo de inclinación a partir de las aceleraciones, se implementa
el siguiente sistema discreto LTI.

$$
	- \theta_{n_a+1} = atan( y_{na} , z_{na} ) + o_{a}
$$

Dónde $y$ y $z$ son las aceleraciones en el eje homónimo, y $o_{a}$ es el desface
del ángulo.

Para calcular el ángulo de inclinación a partir de las velocidades angulares,
se implementa el siguiente sistema discreto LTI.

$$
	- \theta_{n_g+1} = \dot{\theta}_{n_g} + \dot{\theta}_{n_g-1} + o_{g}
$$

Dónde $\dot{\theta}$ es la velocidad angular estimada por el giroscópio
y $o_{g}$ es el desface de la estimación de ángulo.

Posteriormente, ambos ángulos, $\theta_{n_g}$ y $\theta_{n_a}$, son procesados
a través de un filtro complementario con ganancia $k=98\%$. El signo del ángulo resultante
se encuentra invertido, efecto que se resuelve durante el proceso de control.

\subsection{Encoders incrementales.}

Un encoder, es un disco que produce pulsos eléctricos de acuerdo al ángulo en
que se encuentre su eje de rotación. Éste, tiene una cierta cantidad de
pulsos que indican una rotación completa, y la frecuencia de los pulsos está directamente
relacionada con la velocidad angular del disco.
Cuando el dispositivo es capaz de producir dos pulsos desfasados durante su rotación,
entonces el encoder permite distinguir el signo de la rotación, a partir del cual
se puede procesar la velocidad angular y el sentido de giro.
La resolución de un encoder, es la cantidad de pulsos por rotación que es capaz de producir.

Calcular la velocidad de giro de un motor, requiere que el motor tenga acoplado al eje
de rotación un encoder. Existen dos métodos para calcular la velocidad del motor.
Si el giro del motor es lento y el encoder tiene una resolución baja, un temporizador
entre pulsos multiplicado por el ángulo de separación entre los pulsos,
indicaría el tiempo que le toma al eje rotar ese ángulo.
Por otro lado, si la velocidad de giro impide al procesador estimar el tiempo entre pulsos,
otra alternativa es utilizar un periodo de muestreo, multiplicado por la cantidad de pulsos
recibidos en esa ventana de tiempo, aumentado en el ángulo entre pulsos, da como resultado
el ángulo recorrido por el eje del motor en ese periodo de muestreo.

El prototipo RAP implementa el segundo método a través de un periférico interno
del uC, denominado \textit{QEI} (del inglés, \textit{quadrature encoder interface}),
mísmo que se encarga de estimar el ángulo absoluto del eje y el signo.
El uC cuenta con dos de éstos periféricos, lo que permite asignar uno a cada motor.

\subsection{Motores DC.}

Ambos motores del prototipo RAP tienen las mísmas especificaciones.
Para gobernarlos se hace uso de un puente H por motor,
(figura \ref{fig:c33-controlpcb} derecha, marcado como puentes H),
a través de una señal PWM por cada sentido de giro. En total, se requieren
4 señales PWM (2 por cada motor).

La realimentación de corriente que proveen los puentes H permiten construir
un sistema de control de lazo cerrado. Sin embargo, el prototipo RAP
implementa un control de lazo abierto.

El control de lazo abierto de cada motor, consiste en una ganancia $Km=0.00291566$ que convierte
una señal de par de fuerza (torque) $u$ en una señal en \% de cíclo de trabajo PWM con signo
a través de la función $d = u/Km$.
El controlador del periférico externo puente H se encarga de convertir el porcentaje de trabajo $d$
a señal PWM, considerando el signo.

La ganancia $Km$ es resultado de considerar al motor como un sistema lineal. De acuerdo
a la hoja de datos del motor (modelo \textit{4752} de la marca \textit{Pololu}, datos técnicos \cite{pololuMotor}),
éste es capaz de entregar de forma teórica 14$Kg\cdot cm$ con un voltaje de 12 volts y una corriente de 5.5 amperios
como máximo superior, y como valor medio a 6 volts con una corriente de 4 amperios entrega un torque de
7.9$Kg\cdot cm$.

Analizando éstas propiedades en términos del cíclo de trabajo, se tienen dos relaciones lineales.
Desde la perspectiva del control de voltaje, se puede decir que el motor alcanza el pico de torque en el 100\%
de cíclo de trabajo PWM, y su valor medio en el 50\% de cíclo de trabajo PWM.
Para el primer caso

$$
Km_{v_1} = 0.14(kgm)/100(\%) = 0.0014 (kgm/\%)
$$

en el segundo caso se tiene

$$
Km_{v_2} = 0.079(kgm)/50(\%) = 0.00158 (kgm/\%)
$$

el promedio de ambos es $Km_{v}=0.00149$.

Desde la perspectiva del control de corriente, el análisis arroja que

$$
Km_{c_1} = 0.14(kgm)/100\% = 0.0014(kgm/\%)
$$

considerando el pico máximo, y 

$$
Km_{c_2} = 0.079(kgm)/54.545454'\% = 0.001448333(kgm/\%)
$$

considerando el valor medio.
Haciendo un promedio de éstos 4 valores posibles de $Km$, se obtiene el valor propuesto como

$$
Km_{avg} = \frac{1}{4}( Km_{v_1} + Km_{v_2} + Km_{c_1} + Km_{c_2} ) \approx 0.00145783
$$

Sin embargo, recordando que el control es aplicado sobre una rueda virtual como señal $u$,
el factor real de conversión es

$$
Km = (0.00145783)\cdot(2) = 0.00291566
$$

pues dicha señal $u$ es $u = u_1 + u_2$, por que el torque total de los actuadores, debe ser la suma del
torque total aplicado por cada motor.

\section{Microcontrolador DSP.}

El microcontrolador \textit{Tiva C TM123} de \textit{Texas instruments}, es un uC de arquitectura
ARM de 32 bits modelo \textit{M4F}, posee una unidad de coma flotante e instrucciones especiales
para procesos DSP. Como se mencionó con anterioridad en éste mismo capitulo, éste procesador
cuenta con dos unidades de medición para encoders, lo que permite independizar al CPU principal
del proceso de muestreo de las ruedas. A su vez, de entre todos los periféricos interesantes que posee,
sobresalen para el proyecto, un conjunto de buses IIC y buses UART.
La arquitectura del uC permite implementar un sistema operativo en tiempo real.
\textit{Texas Instruments} ha implementado sobre ésta arquitectura su propio RTOS de nombre
\textit{Ti-RTOS}, que además de proveer interfaces para
la administración lógica de tiempos y recursos, también posee controladores para todos los periféricos internos,
éstos controladores son parte de una biblioteca llamada \textit{TivaWare}, la cual viene instalada
en una ROM dedicada en el propio uC, lo que acelera el proceso de desarrollar prototipos.

En este proyecto se ha utilizado tanto la biblioteca como el RTOS, compilando el firmware
desde el entorno de desarrollo \textit{Code Composer Studio}, el cual también es propiedad
de \textit{Texas Instruments}.

\section{Arquitectura de software.}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{./00-Resources/02-Figures/c54-blockDiagram}
	\caption{Bloques lógicos de la arquitectura del software, la posición, de abajo hacia arriba,
	indica el nivel de abstracción, dónde el nivel más alto corresponde a flujo de datos, y el nivel
	más bajo, a comunicación y control del hardware.}
	\label{fig:softwareBlockDiagram}
\end{figure}

Como se puede apreciar en la figura \ref{fig:softwareBlockDiagram}, el software se puede
descomponer por grandes bloques de abstracción. Los dos bloques más bajos son todas las bibliotecas
de software embebido provistas por el fabricante, mientras que las capas superiores, son piezas
de software desarrolladas especialmente para el proyecto.

Las piezas más importantes, son los controladores de periféricos externos del robot,
éstos son los drivers que mueven a ambos motores y permiten codificar y decodificar
los datos del sensor de inercias. Éstos controladores se implementan a través
de los drivers internos provistos por el fabricante.

La siguiente capa consiste en el sistema de control de lazo cerrado ó simplemente
sistema de control, y el sistema de medición de variables de ambiente ó entorno, también
llamado environment. Éstas piezas de software comparten el mísmo nivel, porque interactúan
directamente con los drivers del robot.

La capa más alta del sistema es dedicada al sistema de comunicación, que, apesar
de tener contacto directo con los drivers a través del RTOS, los procesos son
netamente intercambio de datos entre los hilos del RTOS y la computadora externa.

A continuación se describen brevemente las 3 piezas de software que dan vida
al proyecto.

\subsection{Sistema de medición del entorno.}

Éste sistema se encarga de leer cada sensor con una frecuencia de 2 milisegundos,
que posteriormente procesa para construír una estructura privada denominada
\textit{variables de entrono}. Para que cualquier proceso, externo al sistema de entorno,
pueda acceder a ésta estructura, se realiza una petición a través de la interfaz
\textit{Environment\_read( variableID , ptrBuffer );}, mísma que desactiva momentaneamente
las interrupciones del CPU para copiar dichos datos desde la estructura hasta el buffer.
La función de lectura es de tipo no reentrante\footnote{
Una función reentrante, es una función que puede ser interrumpida y vuelta a ejecutar
sin repercusiones en el resultado.}, y de no ser por el bloqueo de interrupciones
podría incurrir en una condición de carrera\footnote{
Una condición de carrera, sucede cuando dos procesos cuyo orden de ejecución es desconocido
comparten un mísmo recurso que debe consumirse en un orden determinado.}
que dañaría la estructura de datos, afectando al sistema de control.

\subsection{Sistema de control de lazo cerrado.}

El sistema de control, realiza una petición al sistema de entorno para leer las variables
de estado estimadas, realiza la correción de signo en el ángulo de inclinación y hace
un llamado al \textit{controlLoop} y al sistema de muestreo.

El \textit{controlLoop}, es una función sin argumento y que no retorna nada,
su propósito es actualizar la variable local del sistema de control $u$, la cual
almacena la señal que deberá ser enviada a cada motor.
El cálculo de la señal, $u$, es el resultado de multiplicar el vector de estado por la matriz
de ganancias (resultado de la propuesta de control), y dividirlo entre el factor de conversión
$Km$.

El sistema de muestreo, consiste en rellenar una trama y encolarla a través del sistema
de comunicación, éste proceso se activa o desactiva a petición desde el sistema de comunicación.

Tanto el \textit{controlLoop} como el sistema de muestreo se ejecutan en el periodo de control,
ésto es cada 10 milisegundos. La prioridad de éste proceso es la más alta de todo el firmware.

\section{Sistema de comunicación.}

Es el proceso de menor prioridad del firmware. En él, se configura un puerto UART
a 115200 baudios con una codificación de 8 bits y 1 bit de paro.
El sistema de comunicación no tiene periodo de ejecución, sino que se ejecuta bajo
demanda y únicamente cuando el sistema tiene tiempo libre.

A través del driver del periférico interno UART, implementado por el fabricante,
se construye un hilo para recibir datos, y un hilo para transmitirlos.
El hilo receptor, verifica que los bytes recibidos tienen el formato adecuado,
en cuyo caso almacena una copia de éstos datos y hace un llamado al despachador.

El despachador, es una función que identifica el endpoint\footnote{
En software, un endpoint es una abstracción para identificar canales de comunicación entre procesos,
los canales pueden ser buffers, puertos, interfaces, etc.
} a quien va dirigido el paquete dentro de una tabla de funciones, luego hace
el llamado al procesador del endpoint y espera su respuesta.

El procesador del endpoint, es una función que se almacena en una lista denominada,
lista de endpoints o lista de puertos, recibe como argumento el puntero a
un buffer de datos, y la longitud de dichos datos.
El desarrollador puede definir el formato de los datos que puede recibir,
(dentro de las capacidades de la especificación del sistema de comunicación),
y las respuestas que se envían a través de una trama.

Por su parte, el hilo emisor se activa cuando se ha recibido una trama para enviar
y sólo si el CPU tiene tiempo muerto. Éste hilo empaqueta la trama en un buffer
dedicado y envía el puntero al driver UART del RTOS para su transmisión,
el driver UART del RTOS implementa transacciones DMA (del inglés \textit{direct memory access}),
las cuales agilizan las transacciones entre la RAM y un periférico interno.

A continuación se describe la especificación del sistema de comunicación del RAP.

\subsection{Requerimientos.}

El sistema de comunicación debe consumir la menor cantidad de recursos del RTOS,
debe permitir transmitir y recibir bytes sin formato definido, con especial
atención a variables de tipo flotante de doble precisión. Al mismo tiempo,
el sistema de comunicación debe ser capaz de transmitir muestreas entre el RAP
y la computadora mientras recibe comandos de acción.

\subsection{Tramas.}

Para que lo anterior sea posible, el sistema de comunicación se basa en la transmisión
de paquetes de datos denominados tramas (figura \ref{fig:c55-frame}).

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.15]{./00-Resources/02-Figures/c55-frame}
	\caption{Representación gráfica de una trama en el sistema de comunicación del RAP.}
	\label{fig:c55-frame}
\end{figure}

Una trama se compone por 2 bytes dedicados a la cabecera, de 1 a 255 bytes destinados
a los datos crudos y de 1 a 5 bytes opcionales, para la suma de verificación.

La cabecera de la trama, dedica 1 byte al símbolo de inicio de trama, y 1 byte
para indicar la longitud de la carga de datos (payload en inglés, o datos crudos).

La suma de verificación, por su parte, dedica 5 bytes opcionales para una suma de verificación 
tipo CRC (no implementada por el momento) y 1 byte no opcional, para el símbolo de final de trama.

\subsection{Canales.}

El sistema de comunicación no admite tramas cuyo payload tenga longitud 0, debido a que el primer
byte del payload es destinado a identificar el canal o endpoint de comunicación. Es decir,
el primer byte del payload indica la función que decodificará los datos de la trama, el desarrollador
puede implementar sus propias funciones y definir los argumentos codificados en la trama.

El firmware del RAP tiene instalados 6 endpoints, desarrollados especialmente para el proyecto.

\begin{itemize}
	\item{\textbf{Ping}: con identificador 0 y sin recibir datos, éste endpoint transmite una trama de respuesta con
	su propio identificador.}
	\item{\textbf{startSampler}: con identificador 1, recibe 4 bytes en formato entero sin signo para solicitar una cantidad fija
	de muestras, o una cantidad indefinida de muestras si la cantidad es 0; las muestras son enviadas en forma de tramas
	con identificador 1. La codificación consiste en 5 valores en formato flotante de doble precisión cuyo orden es
	[variable $\alpha$, variable $\theta$, variable $\dot{\alpha}$, variable $\dot{\theta}$, variable $u$], la variable $u$,
	es el valor de torque aplicado a la rueda virtual del robot.}
	\item{\textbf{stopSampler}: con identificador 2 y sin recibir datos, éste endpoint detiene el proceso de muestreo
	independientemente de la cantidad de tramas restante.}
	\item{\textbf{startControl}: con identificador 3 y sin recibir datos, activa la ejecución del \textit{controlLoop}.}
	\item{\textbf{stopControl}: con identificador 4 y sin recibir datos, detiene la ejecución del \textit{controlLoop}.}
	\item{\textbf{setK}: con identificador 5, recibe 4 valores en formato flotante de doble precisión cuyo orden
	es [$K_{\alpha}$, $K_{\theta}$, $K_{\dot{\alpha}}$, $K_{\dot{\theta}}$ ], éstos valores se cargan como
	la matriz de ganancias del \textit{controlLoop}.}
\end{itemize}

