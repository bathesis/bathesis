\renewcommand{\thepage}{\arabic{page}}

\chapter{Diseño del robot.}

En éste capítulo se presentan los detalles mecánicos y eléctricos
del diseño del robot RAP. Además, se ubicarán los referenciales
locales y se producirá un modelo no lineal del sistema RAP
utilizando la ecuación D'Alembert - Lagrange. Posteriormente,
se linealiza y simplifica el modelo para que pueda ser
utilizado en el siguiente capítulo.

\section{Diseño mecánico del robot.}

El robot RAP se compone de un carro, un cuerpo y las ruedas.
El carro (figura \ref{fig:c31-robotArch}), es la estructura que sujeta las ruedas,
la batería, la electrónica de potencia y la electrónica
de control (figura \ref{fig:c31-robot} izquierda). Está conformado por elementos de acrílico cortados a láser.
El cuerpo (figura \ref{fig:c31-robot} derecha), es una estructura de MDF cortada a láser, que cubre
la electrónica del robot y permite añadir componentes externos,
como una computadora o el dispositivo de programación del microcontrolador.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.05]{./00-Resources/02-Figures/frontalylateral}
	\caption{Componentes electrónicos del RAP instalados en el carro del robot.}
	\label{fig:c31-robotArch}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.05]{./00-Resources/02-Figures/c31-robot1}
	\includegraphics[scale=0.05]{./00-Resources/02-Figures/c31-robot}
	\caption{Carro del robot RAP (izquierda). Robot RAP con cuerpo de MDF (derecha).}
	\label{fig:c31-robot}
\end{figure}

Ambas ruedas del robot son de tipo scooter, con un diámetro de 144 milímetros,
un grosor de 29 milímetros y un peso de 235 gramos.
Estas se encuentran acopladas al eje del reductor de un motor de corriente directa de
12 volts. Ambos motores comparten especificaciones: el reductor tiene una razón de 30:1,
poseen un encoder digital de efecto hall de 64 unidades
por revolución, acoplado al eje del motor antes del reductor. Ambos motores
alcanzan una velocidad de 330 revoluciones por minuto con un consumo de
200 miliamperios sin carga. Con carga, los motores entregan un par de fuerza
de 14 kilogramos por centímetro con un consumo de 5.5 amperios (esta carga es el límite máximo
de los motores, el cual podría dañarlos), los motores son de la marca Pololu \cite{pololuMotor}.

El robot con el cuerpo acoplado, tiene una altura de 67 centímetros, sin el cuerpo,
tiene una altura de 20 centímetros, ambas medidas son a partir del piso.
El centro de masa del sistema con cuerpo tiene una altura de 17.27 centímetros
a partir del eje de las ruedas, con un peso de 3.071 kilogramos.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.125]{./00-Resources/02-Figures/c31-freebodies}
	\caption{Diagrama de cuerpos del robot, base del modelo matemático,
	muestra la ubicación de los referenciales no inerciales con respecto al referencial inercial.}
	\label{fig:c31-freebodies}
\end{figure}

Como se aprecia en al diagrama de cuerpos (figura \ref{fig:c31-freebodies}),
el modelo del robot RAP no contempla el acimut, por lo que la traslacion
se restringe a un único eje.
El referencial local $\Sigma''$ del sistema se ubica a la altura del eje de las ruedas,
el eje de movimiento es el eje $x$, además, el referencial local $\Sigma''$ del robot no 
se expone a ningún tipo de rotación cuando se traslada, 
por lo que el referencial local $\Sigma''$ y el referencial inercial $\Sigma$ comparten
la mísma orientación. Sin embargo, el referencial no inercial $\Sigma'$ acoplado
al cuerpo del robot si presenta una rotación sobre el eje $z$ del referencial local $\Sigma''$ cuando
el cuerpo del robot se inclina, el ángulo de ésta rotación es $\theta$ positivo en sentido
antihorario. El ángulo de rotación de la rueda se considera $\alfa$ positivo en sentido
antihorario sobre el eje $z$ del referencial $\Sigma''$.

\section{Diseño eléctrico del robot.}

El sistema RAP necesita 3 tensiones diferentes para funcionar,
la electrónica dígital requiere 3.3 volts y 5 volts. La electrónica de potencia
requiere una tensión de 12 volts, en todos los casos de corriente directa, además,
los motores pueden consumir hasta 5.5 amperios cada uno.

Para alimentar el sistema se utiliza una batería de LiPo de la marca 
\textit{Wild Scorpion}, 
la batería es de 4 celdas, con una capacidad de 4200 miliamperios hora,
el voltaje nominal de la batería es de 14.8 volts, sin embargo, el voltaje
con carga completa es de 16 volts, y 14 volts cuando se ha descargado.
La capacidad de carga es de entre 5 y 10 C, la capacidad de descarga es de hasta
40 C.

La capacidad de carga o descarga de una batería se mide en C,
dónde un C equivale a la capacidad total de la batería en miliamperios,
en el caso de la batería del RAP, 1 C equivale a 4200 miliamperios, siendo
la capacidad de descarga máxima (la cual podría dañar la batería), de hasta
168 Amperios.

Para evitar que la señal de control hacia los motores se degrade cuando la batería
se descargue, se coloca un regulador de tipo conmutado entre la batería
y la electrónica de potencia (figura \ref{fig:c32-electric}, rectángulo azul).
Para alimetar la electrónica digital se tiene un regulador de tipo conmutado que
baja la tensión de 12 volts a 5 volts (figura \ref{fig:c32-electric}, rectángulo verde),
y en la placa de control se cuenta con un regulador extra de tipo lineal,
para bajar la tensión de 5 volts a 3.3 volts (figura \ref{fig:c32-electric}, rectángulo amarillo).
Cuando los motores consumen corriente de la batería, la tensión podría bajar
y momentaneamente dejar sin alimentación la placa de control, por lo tanto,
se tiene un par de capacitores de 2200 micro faradios a la salida del regulador
de 12 a 5 volts, éstos capacitores almacenan la energía suficiente
para mantener al sistema controlador alimentado por almenos 1 segundo.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.1]{./00-Resources/02-Figures/c32-electric}
	\caption{Electrónica de alimentación del robot RAP. En el rectángulo azul se encuentra
el primer regulador conmutado con salida de 12 volts. En el rectángulo verde se encuentra
el segundo regulador conmutado con entrada de 12 volts y salida de 5 volts. En el rectángulo
amarillo se encuentra el tercer regulador de tipo lineal con entrada de 5 volts y salida de 3.3 volts.}
	\label{fig:c32-electric}
\end{figure}

Para gobernar ambos motores se requieren dos puentes H,
el robot RAP tiene dos puentes H de la marca
\textit{Freescale Semiconductor} modelo \textit{MC33926},
éstos circuitos integrados soportan una salida de hasta 5 amperios
cada uno, cuentan con un monitor de corriente para que el microcontrolador
a través del ADC (siglas en inglés de convertidor de analógico a digital)
pueda sensar la corriente que es entregada a los motores,
además, tienen un pin dedicado a informar al microcontrolador si se ha detectado
algún corto circuito. Éstos puentes H pueden ser gobernados con señales
de 3 volts y 5 volts, lo que los hace compatible con lógica TTL ó CMOS.

Para controlar éstos puentes H se requieren 7 lineas digitales de salida por circuito integrado,
1 linea de entrada digital para el bit de notifiación de corto circuito y una linea analógica
para la realimentación de corriente. El control de potencia se hace con dos canales PWM,
uno para cada dirección de giro, la frecuencia máxima de éstas señales que el circuito integrado
soporta son de 20 kilohertz.

\section{Diseño electrónico del robot.}

La electrónica digital de control del robot RAP,
consiste en un microcontrolador de la marca \textit{Texas Instruments},
modelo \textit{Tiva C TM4C123GH6PM}
(figura \ref{fig:c33-controlpcb}, tercer fotografía, marcado como CPU),
un sensor de inercia con acelerometro, giroscopio y magnetometro de la marca \textit{InvenSense}
modelo \textit{MPU-9250} 
(figura \ref{fig:c33-controlpcb}, tercer fotografía, marcado como sensor IMU),
y un módulo puente de comunicación UART a bluetooth,
de la marca \textit{Guangzhou HC Information Technology}, modelo \textit{HC-06}
(figura \ref{fig:c33-controlpcb}, segunda fotografía, marcado como módulo HC-06),

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.04]{./00-Resources/02-Figures/c33-control1}
	\includegraphics[scale=0.04]{./00-Resources/02-Figures/c33-control2}
	\includegraphics[scale=0.04]{./00-Resources/02-Figures/c33-control}
	\caption{Puertos de la placa de control y circuitos elementales del robot RAP.
	La primer imágen (de izquierda a derecha), muestra el puerto de programación/depuración y
	el puerto UART externo. En la segunda imágen, se aprecia el módulo HC-06 conectado en el puerto UART.
	La tercer imágen, señala los principales circuitos integrados de la placa de control.}
	\label{fig:c33-controlpcb}
\end{figure}

\section{Modelo D'Alembert-Lagrange del robot.}
 
\subsection{Restricciones del sistema.}

Los referenciales del sistema se ubican como muestra el diagrama de cuerpos en la figura
\ref{fig:c31-freebodies}. La restricción de movimiento del sistema evita que exista rotación
sobre su propio eje (no hay cambio en el acimut), por lo que el movimiento del sistema se
considera completamente lineal, a excepción de la rotación en el eje del péndulo, y, evidentemente,
las ruedas.
En la figura \ref{fig:c34-model}, se presentan las variables $r_b$ y $r_w$, que representan
la posición del referencial $\Sigma'$ y el referencial $\Sigma''$, respectivamente, ambos ubicados
en el eje de rotación de las ruedas.
Además, se hace la suposición de que ambas ruedas rotan exactamente a la mísma velocidad,
lo que permite eliminar por completo el ángulo de giro del robot. Por otro lado,
permite visualizar que el referencial inercial, representado matemáticamente como $\Sigma$,
se encuentra elevado con respecto al suelo a una altura igual al radio de las ruedas.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.25]{./00-Resources/02-Figures/c34-model}
	\caption{Representación del modelo del péndulo invertido fuera del orígen (referencial inercial).}
	\label{fig:c34-model}
\end{figure}

\subsection{Análisis cinemático de los cuerpos.}

La matriz de rotación $R^b$, permite transformar los vectores en $\Sigma'$ a vectores
en orientación $\Sigma$ y se define como:

\begin{equation}
	R^b \triangleq
\begin{pmatrix}
	C_\theta & -S_\theta & 0 \\
	S_\theta & C_\theta & 0 \\
	0 & 0 & 1
\end{pmatrix}
	\label{eq:bodyRotation}
\end{equation}

Dónde $C_\theta$ y $S_\theta$ significan coseno y seno de $\theta$ respectivamente.

Se define a $l_m$ como la longitud entre el orígen del referencial $\Sigma'$ y el centro de masa del robot.
Y se define a $\vec{d}_b^{\ (b)}$, el vector distancia entre el orígen y la posición del centro de masa
con respecto al referencial del cuerpo $\Sigma'$, tal que $\vec{d}_b^{\ (b)} = (0,l_m,0)^{\ T}$,
entonces, la posición del centro de masa del cuerpo con respecto al referencial inercial
es igual a la posición del referencial no inercial $\Sigma'$, expresado como $r_b$, aumentado
en el producto de la matriz de rotación $R^{b}$ y el vector $\vec{d}_b^{\ (b)}$ como sigue:

\begin{equation}
	\vec{d}_b = \vec{r}_b + R^{b} \vec{d}_b^{\ (b)}
	\label{eq:bodyPositionIncomplete}
\end{equation}

Ahora se define a $R$ como el radio exterior igual en ambas ruedas, el centro de masa
de las ruedas se encuentra justo en el eje de rotación, que además, es el orígen del referencial
$\Sigma''$, éste referencial no presenta cambio de orientación con respecto al referencial inercial,
así que su traslación puede expresarse con la ecuación \ref{eq:wheelPosition}, la cual expresa
que la traslación es igual al producto entre el ángulo de rotación de la rueda $i$ y su radio $R$.

\begin{equation}
	\vec{d}_{w_i} = \vec{r}_{w_i} = 
	\begin{pmatrix}
	-\alpha_{i} R \\
	0 \\
	0 \\
	\end{pmatrix}
	\label{eq:wheelPosition}
\end{equation}

Usando la ecuación \ref{eq:wheelPosition}, se puede completar la definición de la ecuación
\ref{eq:bodyPositionIncomplete} como sigue:

\begin{equation}
	\vec{d}_b = \vec{r}_b + R^{b} \vec{d}_b^{\ (b)} =
	\begin{bmatrix}
	-\alpha R \\
	0 \\
	0 \\
	\end{bmatrix}
	+
\begin{bmatrix}
	C_\theta & -S_\theta & 0 \\
	S_\theta & C_\theta & 0 \\
	0 & 0 & 1
\end{bmatrix}
\begin{bmatrix}
	0 \\
	l_m \\
	0 \\
\end{bmatrix}
	=
\begin{bmatrix}
	-\alpha R - l_m S_\theta \\
	l_m C_\theta \\
	0
\end{bmatrix}
	\label{eq:bodyPosition}
\end{equation}

Derivando las expresiones \ref{eq:wheelPosition} y \ref{eq:bodyPosition}
se obtienen las ecuaciones \ref{eq:bodiesSpeeds}, cuyo desarrollo conlleva
a las velocidades lineales de: la rueda $i$ como la ecuación \ref{eq:wheelSpeed},
y el centro de masa del cuerpo como la ecuación \ref{eq:bodySpeed}.

\begin{equation}
\begin{align*}
	\dot{\vec{d}}_{w_i} = \dot{\vec{r}}_{w_i} \\
	\dot{\vec{d}}_b = \dot{\vec{r}}_b + \dot{R}^{b} \vec{d}_b^{\ (b)}
	= \dot{\vec{r}}_b + \dot{R}^{b}[\omega^{(b)} \times] \vec{d}_b^{\ (b)}
\end{align*}
	\label{eq:bodiesSpeeds}
\end{equation}

\begin{equation}
	\dot{\vec{d}}_{w_i} =
	\begin{bmatrix}
	-\dot{\alpha_i} R \\
	0 \\
	0
	\end{bmatrix}
	\label{eq:wheelSpeed}
\end{equation}

\begin{equation}
	\dot{\vec{d}}_{b} =
	\begin{bmatrix}
	-\dot{\alpha} R - l_m \dot{\theta} C_{\theta} \\
	- l_m \dot{\theta} S_{\theta} \\
	0
	\end{bmatrix}
	\label{eq:bodySpeed}
\end{equation}

\subsection{Energía cinética de los cuerpos.}

A partir de las expresiones \ref{eq:wheelSpeed} y \ref{eq:bodySpeed},
la energía cinética de los cuerpos se puede expresar como:
la energía cinética de las ruedas $K_{w_i}$ (ecuación \ref{eq:wheelKineticEnergy}),
y la energía cinética del cuerpo del robot $K_{b}$ (ecuación \ref{eq:bodyKineticEnergy}).
En el caso de la ecuación \ref{eq:wheelKineticEnergy}, el eje de rotación es el centro
de masa, por lo que es posible utilizar la ecuación \ref{eq:kineticEnergyCM},
y el término $\mathcal{I}_{w_i}$ corresponde al tensor de inercia de la rueda $i$.
Mientras que en la ecuación \ref{eq:bodyKineticEnergy}, se hace uso de la expresión 
\ref{eq:kineticEnergyNonCM}, y el término $\mathcal{I}_{b'}$ corresponde al tensor
de inercias $\mathcal{I}_{b}$ aumentado en $- m_b[ \vec{d}_b^{\ (b)} \times ]^2$.

\begin{equation}
	K_{w_i} = \frac{1}{2} \dot{\vec{r}}_{w_i}^\mathsmaller{\ T} \dot{\vec{r}}_{w_i} m_{w_i}
		+ \frac{1}{2}\vec{\omega}^{\ T}_{w_i} \mathcal{I}_{w_i} \vec{\omega}_{w_i}
	\label{eq:wheelKineticEnergy}
\end{equation}

Dónde $\vec{\omega}_{w_i}$, es la velocidad angular de la rueda $i$ que cumple:
$\vec{\omega}_{w_i} = (0,0,\dot{\alpha}_{w_i})^{T}$.

\begin{equation}
	K_b = \frac{1}{2} \dot{\vec{r}}_{b}^\mathsmaller{\ T} \dot{\vec{r}}_{b} m_b
		- \dot{\vec{r}}_{b}^\mathsmaller{\ T} m[R^{b}\vec{r}_b^{\ (b)}\times] \vec{\omega_b}
		+ \frac{1}{2}\vec{\omega_b}^{\ T} \mathcal{I}_{b'} \vec{\omega_b}
	\label{eq:bodyKineticEnergy}
\end{equation}

Dónde $\vec{\omega}_{b}$, es la velocidad angular del centro de masa del cuerpo
del robot, que cumple:
$\vec{\omega}_{b} = (0,0,\dot{\theta})^{T}$.

Como $\vec{\omega}_{b}$ y $\vec{\omega}_{w_i}$, son vectores cuya única componente
diferente de cero es la componente en el eje $z$, el producto $\vec{\omega}^{\ T}\mathcal{I}\vec{\omega}$
de ambas ecuaciones (\ref{eq:wheelKineticEnergy} y \ref{eq:bodyKineticEnergy}), puede simplificarse
de la siguiente manera:

\begin{equation}
	K_{w_i} = \frac{1}{2} \dot{\vec{r}}_{w_i}^\mathsmaller{\ T} \dot{\vec{r}}_{w_i} m_{w_i}
		+ \frac{1}{2}\dot{\alpha}^2_{w_i} I_{w_i}
\end{equation}

Dónde $I_{w_i}$ es el componente de la tercer fila y tercer columna del tensor de inercias
$\mathcal{I}_{w_i}$ de la rueda $i$.

\begin{equation}
	K_b = \frac{1}{2} \dot{\vec{r}}_{b}^\mathsmaller{\ T} \dot{\vec{r}}_{b} m_b
		- \dot{\vec{r}}_{b}^\mathsmaller{\ T} m[R^{b}\vec{d}_b^{\ (b)}\times] \vec{\omega_b}
		+ \frac{1}{2} \dot{\theta}^2 I_b
\end{equation}

Dónde $I_b$ es el componente de la tercer fila y tercer columna de la matriz de inercias
$\mathcal{I}_{b'}$ que cumple: $I_b = m_b l_m^2 + I_{b_{zzc}}$, con
$I_{b_{zzc}} = -\int_{B} yz dm$. De acuerdo a la ecuación \ref{eq:inertiaMatrix}.

Terminando de desarrollar las ecuaciones, se debe llegar a la ecuación
\ref{eq:wheelKineticEnergyFull} y la ecuación \ref{eq:bodyKineticEnergyFull},
que expresan la energía cinética de la rueda $i$ y el cuerpo del robot, respectivamente.

\begin{equation}
	K_{w_i} = \frac{1}{2} m_{w_i} \dot{\alpha}_i^2 R^2 + \frac{1}{2} \dot{\alpha}_i^2 I_{w_i}
	\label{eq:wheelKineticEnergyFull}
\end{equation}

\begin{equation}
	K_b = \frac{1}{2} m_{b} \dot{\alpha}^2 R^2
	+ \frac{1}{2} \dot{\theta}^2 I_b
	+ m_b l_m R C_{\theta} \dot{\alpha} \dot{\theta}
	\label{eq:bodyKineticEnergyFull}
\end{equation}

La energía cinética total puede escribirse como la ecuación \ref{eq:totalK}, que contempla
la energía cinética de cada rueda de forma individual.

\begin{equation}
\begin{align*}
	K = \frac{1}{2} m_{b} \dot{\alpha}^2 R^2 + \frac{1}{2} \dot{\theta}^2 I_b
		+ m_b l_m R C_{\theta} \dot{\alpha} \dot{\theta} \\
	+ \frac{1}{2} m_{w_1} \dot{\alpha}_1^2 R^2 + \frac{1}{2} \dot{\alpha}_1^2 I_{w_1} \\
	+ \frac{1}{2} m_{w_2} \dot{\alpha}_2^2 R^2 + \frac{1}{2} \dot{\alpha}_2^2 I_{w_2} \\
\end{align*}
	\label{eq:totalK}
\end{equation}

Si se define a $I_w = I_{w_1} + I_{w_2}$ y a $m_w = m_{w_1} + m_{w_2}$, junto
con la restricción de rotación de las ruedas, de forma que $\alpha_{w_1} = \alpha_{w_2}$, entonces,
la ecuación de energía cinetica total debe ser la ecuación \ref{eq:totalKineticEnergy}.

\begin{equation}
\begin{align*}
	K = \frac{1}{2} m_{b} \dot{\alpha}^2 R^2 + \frac{1}{2} \dot{\theta}^2 I_b
			+ m_b l_m R C_{\theta} \dot{\alpha} \dot{\theta} \\
		+ \frac{1}{2} m_{w} \dot{\alpha}^2 R^2 + \frac{1}{2} \dot{\alpha}^2 I_{w} \\
	= \frac{1}{2} \biggr ( (m_b + m_w) R^2 + I_w \biggr ) \dot{\alpha}^2 \\
	 + \frac{1}{2} I_b \dot{\theta}^2 + m_b l_m R C_{\theta} \dot{\alpha} \dot{\theta}
\end{align*}
	\label{eq:totalKineticEnergy}
\end{equation}

\subsection{Formulación Lagrangiana.}

Definiendo a $\vec{q} = (\alpha,\theta)^T$ como el vector de coordenadas generalizadas del sistema.
La ecuación \ref{eq:totalKineticEnergy} también puede expresarse en términos de la
matriz de inercias $H(\vec{q})$ como muestra la ecuación \ref{eq:totalKwHMatrix}.
La tabla \ref{tab:variables}, enlista las principales variables utilizadas en el modelo matemático.

\begin{equation}
	K = \frac{1}{2} \dot{\vec{q}}^{\ T} H(q) \dot{\vec{q}} \\
	  = \frac{1}{2} \dot{\vec{q}}^{\ T}
		\begin{pmatrix}
			(m_b + m_w)R^2 + I_w	& 	m_b l_m R C_{\theta} \\
			m_b l_m R C_{\theta}	&	I_b \\
		\end{pmatrix}
		\dot{\vec{q}}
	\label{eq:totalKwHMatrix}
\end{equation}

\begin{table}[H]
	\centering
	\begin{tabular}{||c | c | c ||} 
		\hline
		Símbolo & Nombre & Unidades\\ [0.5ex] 
		\hline\hline
		$\theta$ & Ángulo de inclinación del cuerpo. & $rads$ \\ [1ex]
		\hline
		$\alpha$ & Ángulo de giro de ambas ruedas. & $rads$ \\ [1ex]
		\hline
		$m_b$ & Masa del cuerpo del robot. & $Kg$ \\ [1ex]
		\hline
		$m_w$ & Masa de ambas ruedas. & $Kg$ \\ [1ex]
		\hline
		$I_b$ & Inercia del cuerpo. & $Kg \cdot m^2$ \\ [1ex]
		\hline
		$I_w$ & Inercia total de ambas ruedas. & $Kg \cdot m^2$ \\ [1ex]
		\hline
		$R$ & Radio exterior de ambas ruedas. & $m$ \\ [1ex]
		\hline
		$l_m$ & Altura del centro de masa del cuerpo. & $m$ \\ [1ex]
		\hline
		$\mu_d$ & Coeficiente de fricción cinética del piso. & - \\ [1ex]
		\hline
		$\tau_u$ & Torque aplicado por los actuadores. & $N \cdot m$ \\ [1ex]
		\hline
		$u_i$ & Fuerza aplicada por el motor $i$. & $N$ \\ [1ex]
		\hline
		$u$ & Fuerza aplicada por ambos motores. & $N$ \\ [1ex]
		\hline
		$q$ & Coordenadas generalizadas del sistema. & - \\ [1ex]
		\hline
		$Q$ & Fuerzas generalizadas del sistema. & - \\ [1ex]
		\hline
		$H(q)$ & Matriz de inercias. & - \\ [1ex]
		\hline
		$C(q,\dot{q})$ & Matriz de Coriolis. & - \\ [1ex]
		\hline
		$D(q,\dot{q})$ & Matriz generalizada de disipación. & - \\ [1ex]
		\hline
		$\mathbf{g}(q)$ & Vector de gravedad. & - \\ [1ex]
		\hline
		$K(\vec{q},\dot{\vec{q}})$ & Energía cinética del sistema. & - \\ [1ex]
		\hline
		$U(\vec{q})$ & Energía potencial del sistema. & - \\ [1ex]
		\hline
	\end{tabular}
	\caption{Tabla de variables del modelo matemático}
	\label{tab:variables}
\end{table}

A partir de la matriz de inercia $H(\vec{q})$ y la energía cinética total $K$,
se calcula el vector de Coriolis $C(\vec{q},\dot{\vec{q}})\dot{\vec{q}}$ como sigue:

%Reemplazando la energía cinética total del sistema (ecuación \ref{eq:totalKwHMatrix}),
%en la ecuación \ref{eq:dalembertLagrange}, se obtiene lo siguiente:
%\ref{eq:inertialForces}, se obtiene lo siguiente:

\begin{equation}
\begin{align*}
	C(\vec{q},\dot{\vec{q}})\dot{\vec{q}} =
		\dot{H}(\vec{q},\dot{\vec{q}}) \dot{\vec{q}} - \frac{\partial K}{\partial \vec{q} } = \\
	\begin{bmatrix}
		0 & -m_b l_m R S_{\theta}\dot{\theta} \\
		-m_b l_m R S_{\theta}\dot{\theta} & 0 \\
	\end{bmatrix}
	\begin{bmatrix}
		\dot{\alpha}\\
		\dot{\theta}
	\end{bmatrix}
	-
	\begin{bmatrix}
		0 \\
		-m_b l_m R S_{\theta}\dot{\alpha}\dot{\theta}
	\end{bmatrix}	\\
	=
	\begin{bmatrix}
		-m_b l_m R S_{\theta}\dot{\theta}^2\\
		0
	\end{bmatrix}
\end{align*}
	\label{eq:corVector}
\end{equation}

Dónde es posible deducir la matriz de Coriolis $C(\vec{q},\dot{\vec{q}})$ en la
ecuación \ref{eq:corMatrix}.

\begin{equation}
	C(\vec{q},\dot{\vec{q}}) = 
	\begin{bmatrix}
		0 & -m_b l_m R S_{\theta} \dot{\theta} \\
		0 & 0
	\end{bmatrix}
	=
	\begin{bmatrix}
		C_1 & C_2 \\
		C_3 & C_4
	\end{bmatrix}
	\label{eq:corMatrix}
\end{equation}

Junto con la matriz de inercias, ecuación \ref{eq:inerMatrix},
ahora se puede escribir la ecuación D'Alembert - Lagrange como en la ecuación
\ref{eq:DLModel1}.

\begin{equation}
	H(\vec{q}) = 
	\begin{bmatrix}
		(m_b + m_w)R^2 + I_w	& 	m_b l_m R C_{\theta} \\
		m_b l_m R C_{\theta}	&	I_b \\
	\end{bmatrix}
	=
	\begin{bmatrix}
		H_1 & H_2 \\
		H_2 & H_3
	\end{bmatrix}
	\label{eq:inerMatrix}
\end{equation}

\begin{equation}
\begin{align*}
	\begin{bmatrix}
		\biggr ( (m_b + m_w)R^2 + I_w \biggr ) \ddot{\alpha} +
			\biggr ( m_b l_m R C_{\theta} \biggr ) \ddot{\theta} \\
		m_b l_m R C_{\theta} \ddot{\alpha} + I_b \ddot{\theta} \\
	\end{bmatrix}
	+
	\begin{bmatrix}
		-m_b l_m R S_{\theta} \dot{\theta}^2 \\
		0
	\end{bmatrix} \\
	= H(\vec{q})\ddot{\vec{q}} + C(\vec{q},\dot{\vec{q}})\dot{\vec{q}} = Q
\end{align*}
	\label{eq:DLModel1}
\end{equation}

Para desarrollar a Q, se pueden calcular las matrices Jacobianas de
cada cuerpo. De forma que la expresión \ref{eq:jacobWheel}, representa a la matriz Jacobiana
de la rueda $i$, y la expresión \ref{eq:jacobBody}, representa a la matriz Jacobiana del cuerpo
del robot RAP (el péndulo), y utilizarlas para calcular los torques exógenos del sistema.

\begin{equation}
	J_{w_i}(\vec{q}) = \frac{\partial \vec{d}_{w_i}}{\partial \vec{q}} =
	\begin{bmatrix}
		-R & 0 \\
		0 & 0 \\
		0 & 0 \\
	\end{bmatrix}
	\label{eq:jacobWheel}
\end{equation}

\begin{equation}
	J_b(\vec{q}) = \frac{\partial \vec{d}_{b}}{\partial \vec{q}} =
	\begin{bmatrix}
		-R & - l_m C_{\theta} \\
		0 & - l_m S_{\theta} \\
		0 & 0 \\
	\end{bmatrix}
	\label{eq:jacobBody}
\end{equation}

Para calcular el par de fuerza debido a la gravedad, se hace uso de la energía potencial,
$U(\vec{q})$, que depende de las coordenadas generalizadas. Se define a continuación:

$$
	\vec{U}(\vec{q}) = 
	\begin{bmatrix}
	0\\
	m_b g l_m C_{\theta} \\
	\end{bmatrix}
$$

Dónde $g$ es la aceleración de la gravedad.

$\mathbf{g}(\vec{q})$, es el vector de gravedad, el cual expresa el par de fuerza
aplicado sobre los cuerpos en términos de las coordenadas generalizadas, se puede
calcular como sigue:

$$
	\mathbf{g}(\vec{q}) \triangleq
		\frac{\partial U(\vec{q}) }{\partial \vec{q}}
$$

La ecuación \ref{eq:gravVector} muestra el resultado al que se debe llegar.

\begin{equation}
	\mathbf{g}(\vec{q}) = 
\begin{bmatrix}
	0 \\
	- m_b g l_m S_{\theta}\\
\end{bmatrix}
\label{eq:gravVector}
\end{equation}

El movimiento del péndulo se supone no tiene discipación cinética si despreciamos
el coeficiente de fricción del aire, por lo que, si sólo se considera a la fricción
cinética de las ruedas $\vec{f}_{d_{wi}}$ debido al coeficiente de fricción cinética
del piso $\mu_d$, entonces se puede definir a $\vec{f}_{d_{wi}}$,
como el vector de fuerza de discipación cinética sobre la rueda $i$.

$$
	\vec{f}_{d_{wi}} = 
	\begin{bmatrix}
	- \mu_d \dot{\alpha}\\
	0\\
	0
	\end{bmatrix}
$$

El resultado de éste análisis conlleva al vector de discipación generalizado \ref{eq:dampVector},
a partir del cual puede deducirse la matriz de discipación generalizada \ref{eq:dampMatrix}.

\begin{equation}
	D(\vec{q},\dot{\vec{q}}) \dot{\vec{q}} = 
		J_{w_1}^{\ T} \vec{f}_{d_{w1}} + 
		J_{w_2}^{\ T} \vec{f}_{d_{w2}} =
	\begin{bmatrix}
		2R\mu_d\dot{\alpha} \\
		0 
	\end{bmatrix}
\label{eq:dampVector}
\end{equation}

\begin{equation}
	D(\vec{q},\dot{\vec{q}}) =
	\begin{bmatrix}
		2R\mu_d & 0\\
		0 & 0
	\end{bmatrix}
\label{eq:dampMatrix}
\end{equation}

\subsection{Modelo no lineal del sistema mecánico.}

El modelo matemático en forma general se expresa en la ecuación \ref{eq:modelGeneralNonLineal},
como la combinación de las ecuaciones \ref{eq:corVector}, \ref{eq:inerMatrix}, \ref{eq:gravVector},
y \ref{eq:dampVector}.

\begin{equation}
\begin{align*}
	H(\vec{q})\ddot{q} +
	 C(\vec{q},\dot{\vec{q}})\dot{\vec{q}} +
	 D(\vec{q},\dot{\vec{q}})\dot{\vec{q}} +
	\mathbf{g}(\vec{q}) = \tau_u \\
	\begin{bmatrix}
		\biggr ( (m_b + m_w)R^2 + I_w \biggr ) \ddot{\alpha} +
			\biggr ( m_b l_m R C_{\theta} \biggr ) \ddot{\theta} \\
		m_b l_m R C_{\theta} \ddot{\alpha} + I_b \ddot{\theta} \\
	\end{bmatrix}
	+
	\begin{bmatrix}
		-m_b l_m R S_{\theta} \dot{\theta}^2 \\
		0
	\end{bmatrix}\\
	+
	\begin{bmatrix}
		2R\mu_d \dot{\alpha}\\
		0\\
	\end{bmatrix}
	+
	\begin{bmatrix}
		0\\
		- m_b g l_m S_{\theta}\\
	\end{bmatrix}
	= 
	\begin{bmatrix}
		u\\
		0\\
	\end{bmatrix}
\end{align*}
\label{eq:modelGeneralNonLineal}
\end{equation}

Dónde $u$ es la fuerza aplicada por los actuadores del sistema, y $\tau_u$ es el par
de fuerzas aplicado por los actuadores del sistema cuya definición es la siguiente:

\begin{equation}
\tau_u \triangleq
\begin{bmatrix}
	2u_i\\
	0
\end{bmatrix}
=
\begin{bmatrix}
	u\\
	0
\end{bmatrix}
\end{equation}

Desarrollar la ecuación \ref{eq:modelGeneralNonLineal} conlleva a la ecuación \ref{eq:modelGeneralNonLineal1}.

\begin{equation}
	\begin{bmatrix}
		\biggr ( (m_b + m_w)R^2 + I_w \biggr ) \ddot{\alpha}
			+ m_b l_m R C_{\theta} \ddot{\theta}
			- m_b l_m R S_{\theta} \dot{\theta}^2
			+ 2 R\mu_d \dot{\alpha} \\
		m_b l_m R C_{\theta} \ddot{\alpha}
			+ I_b \ddot{\theta}
			- m_b g l_m S_{\theta} \\
	\end{bmatrix}
	= 
	\begin{bmatrix}
		u\\
		0\\
	\end{bmatrix}
\label{eq:modelGeneralNonLineal1}
\end{equation}

\section{Linealización y simplificación del modelo.}

Para linealizar el sistema, se tiene que calcular la serie de Taylor de la función $sen(\theta)$ y $cos(\theta)$,
bajo la suposición de que la variación del ángulo $\theta$ será mínima al rededor del ángulo de equilíbrio,
el cual es $\theta=0$. Decír que la variación del ángulo será mínima se traduce a
simplificar la derivada temporal del ángulo de inclinación como

$$
	\dot{\theta} \approx 0
$$

De ésta manera, $sen(\theta) = \theta$ y $cos(\theta) = 1$ para ángulos muy pequeños,
lo que permite reescribir el sistema como en la ecuación \ref{eq:modelGeneralLineal}
que lo representa de forma lineal.

$$
	C_{\theta} = C_0 + \frac{ -S_0 }{1}\theta + \frac{ -C_0 }{2} \theta^2 + \dots
	\approx 1
$$
$$
	S_{\theta} = S_0 + \frac{ C_0 }{1}\theta + \frac{ -S_0 }{2} \theta^2 + \dots
	\approx \theta
$$

\begin{equation}
	\begin{bmatrix}
		\biggr ( (m_b + m_w)R^2 + I_w \biggr ) \ddot{\alpha}
			+ m_b l_m R \ddot{\theta}
			%- m_b l_m R \theta \dot{\theta}^2
			+ 2 R\mu_d \dot{\alpha} \\
		m_b l_m R \ddot{\alpha}
			+ I_b \ddot{\theta}
			- m_b g l_m \theta \\
	\end{bmatrix}
	= 
	\begin{bmatrix}
		u\\
		0\\
	\end{bmatrix}
\label{eq:modelGeneralLineal}
\end{equation}

