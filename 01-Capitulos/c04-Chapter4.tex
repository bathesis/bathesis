\renewcommand{\thepage}{\arabic{page}}
\chapter{Diseño del controlador.}

En éste capítulo se desarrollará el modelo matemático para representarlo
como un sistema lineal de ecuaciones diferenciales conocido como
representación en el espacio de estado.
Primero se encuentra la \textit{matriz de estado} y la \textit{matriz de entrada}
de forma analítica, con las que, apoyandose en una herramienta conocida como \textit{Octave}
se puede calcular de forma numérica la \textit{matriz de controlabilidad}, y su rango,
que permite conocer si el modelo propuesto del sistema es controlable. 
Por su parte, con la \textit{matriz de estado} y la \textit{matriz de salida}
del sistema también se puede calcular la \textit{matriz de observabilidad}, cuyo
rango indica si el modelo es observable.

Todos los scripts utilizados en el proyecto se encuentran en el anexo \ref{anexo:octaveScripts}.

\section{Modelo en el espacio de estado.}

A partir del modelo lineal del sistema (ecuación \ref{eq:modelGeneralLineal}),
se definen las siguientes variables para simplificar el análisis.

$$
	H_1 \triangleq \biggr ( (m_w + m_b)R^2 + I_w \biggr )
$$
$$
	H_2' \triangleq m_b l_m R
$$
$$
	H_3 \triangleq I_b
$$
$$
	D_1 \triangleq 2R\mu_d
$$
$$
	g_2 \triangleq - m_b g l_m
$$

\begin{equation}
	\begin{bmatrix}
		H_1 \ddot{\alpha} + H_2' \ddot{\theta} + D_1 \dot{\alpha} \\
		H_2'\ddot{\alpha} + H_3 \ddot{\theta} + g_2 \theta 
	\end{bmatrix}
	=
	\begin{bmatrix}
		u\\
		0
	\end{bmatrix}
	\label{eq:linealModelReduced}
\end{equation}

Desarrollando la igualdad $\ddot{\alpha} = \ddot{\alpha}$ se puede encontrar a la variable $\ddot{\theta}$ como sigue:

\begin{align*}
	\frac{ u - D_1\dot{\alpha} - H_2'\ddot{\theta} }{ H_1 } = 
	\frac{ -g_2 \theta - H_3 \ddot{\theta} }{ H_2' }	\\
	H_2'u - H_2'D_1\dot{\alpha} - H_2'H_2'\ddot{\theta} = 
	-g_2 H_1 \theta - H_1H_3\ddot{\theta}	\\
	h_1 \triangleq ( H_2'H_2' - H_1H_3 ) \\
	\ddot{\theta} = \frac{ g_2H_1 }{ h1 } \theta
		- \frac{ H_2'D_1 }{ h_1 } \dot{\alpha}
		+ \frac{ H_2' }{ h_1 } u
\end{align*}

Desarrollando la igualdad $\ddot{\theta} = \ddot{\theta}$ se puede encontrar a la variable $\ddot{\alpha}$ como sigue:

\begin{equation}
\begin{align*}
	\frac{ u - D_1\dot{\alpha} - H_1\ddot{\alpha} }{ H_2' } = 
	\frac{ -g_2\theta - H_2'\ddot{\alpha} }{ H_3 }	\\
	H_3 u - H_3D_1\dot{\alpha} - H_3H_1\ddot{\alpha} =
	-g_2H_2'\theta - H_2' H_2'\ddot{\alpha}	\\
	\ddot{\alpha} = - \frac{ g_2H_2' }{ h_1 } \theta +
		\frac{ H_3D_1 }{ h_1 } \dot{\alpha} -
		\frac{ H_3 }{ h_1 } u
\end{align*}
\end{equation}

Sea $\vec{x} = (\alpha,\theta,\dot{\alpha},\dot{\theta})^{\ T}$, el vector de estado del sistema dinámico,
y sea $\vec{y} = (\alpha,\theta,\dot{\alpha},\dot{\theta})^{\ T}$, el vector de salida del sistema,
entonces, la representación del modelo dinámico en el espacio de estado es la ecuación \ref{eq:stateSpaceModel}.
Como el sistema está restringido en la acción del ángulo $\alpha$, la variable de control
es tal que $u = u_1 + u_2\ |\ u_1 = u_2$, es decir, la mitad para cada rueda.

\begin{equation}
\begin{align*}
	\vec{x} = 
	\begin{bmatrix}
		\dot{\alpha} \\
		\dot{\theta} \\
		\ddot{\alpha} \\
		\ddot{\theta}
	\end{bmatrix}
	=
	\begin{bmatrix}
		0 & 0 & 1 & 0 \\
		0 & 0 & 0 & 1 \\
		0 & \frac{ -g_2H_2' }{h_1} & \frac{H_3 D_1}{h_1} & 0 \\
		0 & \frac{ g_2H_1 }{h_1} & \frac{-H_2'D_1}{h_1} & 0
	\end{bmatrix}
	\begin{bmatrix}
		\alpha \\
		\theta \\
		\dot{\alpha} \\
		\dot{\theta}
	\end{bmatrix}
	+
	\begin{bmatrix}
		0 \\
		0 \\
		-\frac{H_3}{h_1} \\
		\frac{H_2'}{h_1} \\
	\end{bmatrix}
	u \\
	\vec{y}
	=
	\begin{bmatrix}
		1 & 0 & 0 & 0 \\
		0 & 1 & 0 & 0 \\
		0 & 0 & 1 & 0 \\
		0 & 0 & 0 & 1 \\
	\end{bmatrix}
	\begin{bmatrix}
		\alpha \\
		\theta \\
		\dot{\alpha} \\
		\dot{\theta}
	\end{bmatrix}
\end{align*}
\label{eq:stateSpaceModel}
\end{equation}

\section{Análisis del modelo dinámico en el espacio de estado.}

Para analizar numéricamente el sistema \ref{eq:stateSpaceModel}, se sustituyen
las variables \ref{tab:variableUnits} del robot en la matriz de estado y la matriz de entrada.
Éstas variables fueron obtenidas a través de mediciones físicas del robot y sus piezas. Excepto
el coeficiente de fricción, el cual es un valor arbitrario.

\begin{table}[H]
	\centering
	\begin{tabular}{||c | c | c ||} 
		\hline
		Símbolo & Nombre & Valor estimado o medido. \\ [0.5ex] 
		\hline\hline
		\hline
		$m_b$ & Masa del cuerpo del robot. & $3.071\ Kg$ \\ [1ex]
		\hline
		$m_w$ & Masa de ambas ruedas. & $0.470\ Kg$ \\ [1ex]
		\hline
		$I_b$ & Inercia del cuerpo. & $0.091593\ Kg \cdot m^2$ \\ [1ex]
		\hline
		$I_w$ & Inercia total de ambas ruedas. & $0.056870\ Kg \cdot m^2$ \\ [1ex]
		\hline
		$R$ & Radio exterior de ambas ruedas. & $0.072\ m$ \\ [1ex]
		\hline
		$l_m$ & Altura del centro de masa del cuerpo. & $0.1727\ m$ \\ [1ex]
		\hline
		$\mu_d$ & Coeficiente de fricción cinética del piso. & $0.8$ \\ [1ex]
		\hline
		$g$ & Aceleración de la gravedad. & $9.8\ m/s$ \\ [1ex]
		\hline
	\end{tabular}
	\caption{Tabla de variables del modelo matemático con valores medidos, aproximados y sugeridos.}
	\label{tab:variableUnits}
\end{table}

La matriz de estado con éstos valores ya sustituídos resulta en la siguiente matriz:

\begin{equation}
	A = 
	\begin{bmatrix}
	0.00000  &   0.00000  &  1.00000  &  0.00000\\
	0.00000  &   0.00000  &  0.00000  &  1.00000\\
	0.00000  & -16.10681  & -1.71259  &  0.00000\\
	0.00000  &  31.73043  &  0.35700  &  0.00000
	\end{bmatrix}
	\label{eq:stateMatrixModel}
\end{equation}

La matriz de entrada con éstos valores ya sustituídos resulta en:

\begin{equation}
	B =
	\begin{bmatrix}
		0.00000\\
		0.00000\\
		14.86624\\
		-3.09893\\
	\end{bmatrix}
	\label{eq:inputMatrixModel}
\end{equation}

\subsection{Análisis de estabilidad.}

La estabilidad del sistema depende de los valores propios de la matriz \ref{eq:stateMatrixModel}
de estado, si la parte real de alguno de los valores propios de dicha matriz es mayor o igual
a cero, entonces se puede asegurar que el sistema es inestable.

Con el comando $eig(A)$ de Octave se pueden computar los valores propios de la matriz A.
El resultado obtenido es el siguiente:

\begin{equation}
	eig( A ) = 
	\begin{bmatrix}
		0.00000\\
		5.56238\\
		-5.75777\\
		-1.51720
	\end{bmatrix}
	\label{eq:stabilityModel}
\end{equation}

Como existe un valor propio ubicado en el semiplano derecho del plano complejo $s$,
se puede asegurar que el sistema es inestable.

\subsection{Análisis de controlabilidad.}

Gracias a Octave, se puede computar la matriz de controlabilidad que se define como
$CO \triangleq [B\ |\ AB\ |\ AAB\ |\ AAAB]$. El rango de ésta matriz se puede
computar con el comando \textit{rank(CO)}, el resultado es:

\begin{equation}
	rank( CO ) = rank\biggr (
	\begin{bmatrix}
     0.00000 &   14.86624 &  -25.45978 &   93.51599\\
     0.00000 &   -3.09893 &    5.30719 & -107.41931\\
    14.86624 &  -25.45978 &   93.51599 & -245.63655\\
    -3.09893 &    5.30719 & -107.41931 &  201.78435
	\end{bmatrix}
	\biggr ) = 4
	\label{eq:rankControl}
\end{equation}

El resultado \ref{eq:rankControl} muestra que el modelo del sistema es en efecto
controlable.

\subsection{Análisis de observabilidad.}

En éste proyecto no se construirá ningún filto Kalman, sin embargo,
la observabilidad nos permite saber si existe ésta posibilidad para los trabajos
futuros.
El rango de la matriz de observabilidad, la cual se define como
$OB \triangleq [C\ |\ CA\ |\ CAA\ |\ CAAA] $,
tiene que ser igual a 4 para asegurar que el modelo es observable.

\begin{equation}
	rank( OB ) = rank
	\begin{bmatrix}
		1.00000 &     0.00000  &    0.00000  &    0.00000\\
		0.00000 &     1.00000  &    0.00000  &    0.00000\\
		0.00000 &     0.00000  &    1.00000  &    0.00000\\
		0.00000 &     0.00000  &    0.00000  &    1.00000\\
		0.00000 &     0.00000  &    1.00000  &    0.00000\\
		0.00000 &     0.00000  &    0.00000  &    1.00000\\
		0.00000 &   -16.10681  &   -1.71259  &    0.00000\\
		0.00000 &    31.73043  &    0.35700  &    0.00000\\
		0.00000 &   -16.10681  &   -1.71259  &    0.00000\\
		0.00000 &    31.73043  &    0.35700  &    0.00000\\
		0.00000 &    27.58437  &    2.93297  &  -16.10681\\
		0.00000 &    -5.75007  &   -0.61139  &   31.73043\\
		0.00000 &    27.58437  &    2.93297  &  -16.10681\\
		0.00000 &    -5.75007  &   -0.61139  &   31.73043\\
		0.00000 &  -558.31666  &  -10.77304  &   27.58437\\
		0.00000 &  1016.66764  &   12.37470  &   -5.75007
	\end{bmatrix}
	= 4
	\label{eq:rankObsv}
\end{equation}

El resultado \ref{eq:rankObsv} muestra la salida de Octave, el sistema es en efecto
observable.

\section{Discretización del modelo dinámico.}

Gracias a la herramienta Octave, discretizar el modelo sólo requiere identificar el periodo de muestreo en segundos.
El robot RAP es capaz de medir las variables de entorno y computar un control PID
cada 5 milisegundos, sin embargo, tomando en cuenta el sistema de comunicación y
la cantidad de variables que ahora se necesitan compartir en tiempo real,
se ha propuesto un periodo de muestreo de 10 milisegundos, lo que resulta en una
frecuencia de muestreo y control de 100 Hertz.
Por lo tanto, el periodo se define como $T_s = 0.010$.

Discretizar el sistema dinámico es posible con el comando $c2d$ de Octave,
entregando como argumento el sistema dinámico de tiempo
continuo, el periodo de muestreo y el tipo de discretización. 
Para el proyecto se han probado dos tipos de discretización,
la discretización \textit{bilineal} (también conocida como método de \textit{Tustin})
y la discretización por \textit{retenedor de orden cero}
(también conocido como zoh del inglés, \textit{zero order hold}).
La discretización de orden cero produjo ganancias demasiado altas para el sistema,
mientras que la discretización bilineal produjo ganancias mucho más cercanas
a las funcionales.

Ésta sección expone los resultados obtenidos por la discretización bilineal.

Matriz de estado del sistema discreto es la siguiente:

\begin{equation}
	G = 
	\begin{bmatrix}
		1 & -0.0007991 &   0.009915 & -3.996e-06\\
		0 &      1.002 &  1.771e-05 &    0.01001\\
		0 &    -0.1598 &      0.983 & -0.0007991\\
		0 &     0.3173 &   0.003542 &      1.002
	\end{bmatrix}
	\label{eq:DModelGMatrix}
\end{equation}

Matriz de entrada del sistema discreto es la siguiente:

\begin{equation}
	H = 
	\begin{bmatrix}
		0.007371\\
		-0.001538\\
		1.474\\
		-0.3075
	\end{bmatrix}
	\label{eq:DModelHMatrix}
\end{equation}

Matriz de salida del sistema discreto es la siguiente:

\begin{equation}
	C = 
	\begin{bmatrix}
		0.1&  -3.996e-05& 0.0004958 & -1.998e-07\\
		0  &    0.1001  & 8.856e-07 &  0.0005004\\
		0  & -0.007991  &   0.09915 & -3.996e-05\\
		0  &   0.01586  & 0.0001771 &     0.1001
	\end{bmatrix}
	\label{eq:DModelHMatrix}
\end{equation}


Matriz de transmisión directa del sistema discreto es la siguiente:

\begin{equation}
	D = 
	\begin{bmatrix}
		0.0003685\\
		-7.688e-05\\
		0.07371\\
		-0.01538
	\end{bmatrix}
	\label{eq:DModelHMatrix}
\end{equation}


\subsection{Análisis de estabilidad.}

Los valores propios de la matriz \ref{eq:DModelGMatrix} son los siguientes:

\begin{equation}
	eig(H) = 
	\begin{bmatrix}
		1.00000\\
		1.05722\\
		0.94403\\
		0.98494
	\end{bmatrix}
	\label{eq:DModelHEigV}
\end{equation}

Los valores propios \ref{eq:DModelHEigV} de la matriz de estado indican que el 
sistema es inestable, debido a que dos de éstos se encuentran fuera del círculo unitario
del plano complejo $z$, región de estabilidad para sistemas discretos.

\subsection{Análisis de controlabilidad.}

La matriz de controlabilidad del sistema discreto $dCO$ tiene el rango siguiente:

\begin{equation}
	rank(dCO) = rank \biggr (
	\begin{bmatrix}
		0.0073706  &  0.0219892 &  0.0363668 &  0.0505124\\
		-0.0015375 & -0.0045913 & -0.0076079 & -0.0105978\\
		1.4741248  &  1.4495828 &  1.4259423 &  1.4031828\\
		-0.3075053 & -0.3032589 & -0.3000616 & -0.2979000
	\end{bmatrix}
	\biggr ) 
	= 4
	\label{eq:DModelHControllability}
\end{equation}

Con lo que se puede concluír que el sistema es controlable.

\subsection{Análisis de observabilidad.}

La matriz de observabilidad del sistema discreto $dOB$ tiene el rango siguiente:

\begin{equation}
	rank(dOB) = rank
	\begin{bmatrix}
		0.10000 & -0.00004 &  0.00050 & -0.00000\\
		0.00000 &  0.10008 &  0.00000 &  0.00050\\
		0.00000 & -0.00799 &  0.09915 & -0.00004\\
		0.00000 &  0.01586 &  0.00018 &  0.10008\\
		0.10000 & -0.00020 &  0.00148 & -0.00000\\
		0.00000 &  0.10040 &  0.00000 &  0.00150\\
		0.00000 & -0.02386 &  0.09747 & -0.00020\\
		0.00000 &  0.04761 &  0.00053 &  0.10040\\
		0.10000 & -0.00052 &  0.00245 & -0.00000\\
		0.00000 &  0.10103 &  0.00001 &  0.00251\\
		0.00000 & -0.03954 &  0.09581 & -0.00052\\
		0.00000 &  0.07946 &  0.00088 &  0.10103\\
		0.10000 & -0.00099 &  0.00340 & -0.00001\\
		0.00000 &  0.10199 &  0.00002 &  0.00353\\
		0.00000 &  -0.05508&  0.09418 & -0.00099\\
		0.00000 &  0.11150 &  0.00122 &  0.10199
	\end{bmatrix}
	= 4
	\label{eq:DModelHObservability}
\end{equation}

Lo que indíca que el sistema discretizado también es observable.

\section{Propuesta de control.}

De acuerdo con el objetivo propuesto, la intención del controlador es estabilizar
las variables $\alpha$, $\dot{\alpha}$, $\theta$ y $\dot{\theta}$, correspondientes
a la posición y velocidad del carro, y la inclinación del péndulo. Por lo tanto,
el sistema de control se simplifica dado que la entrada del sistema es el orígen
del espacio de estados.

La arquitectura del sistema de control que se ha propuesto, se puede expresar
como el siguiente diagrama a bloques.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{./00-Resources/02-Figures/c44-controllerBlockDiagram}
	\caption{Diagrama a bloques de la propuesta de control discreto.}
	\label{fig:controlBlockDiagram}
\end{figure}

En el diagrama \ref{fig:controlBlockDiagram}, se considera que el sistema ya está
discretizado, por lo tanto las entradas y las salidas de cada bloque son digitales,
con un periodo de muestreo de $T_s=0.010$ segundos.
El bloque denominado \textit{muestreador}, es el bloque que almacena las variables
de estado y la salida del sistema en tiempo real para compartirlas con el
sistema de comunicación.

\subsection{Matriz de ganancias K.}

Para calcular las ganancias del controlador LQR discreto, se usa el comando $dlqr(sys,Q,R)$
de Octave que requiere como argumento el sistema dinámico ya discretizado y las matrices
de importancia $Q$ y $R$.

La matriz $Q$ se obtiene de multiplicar la transpuesta de la matriz de salida, $C$, del sistema
por ella mísma, es decir $Q = C^{\ T}C$, aunque también se pueden proponer de forma arbitraria,
tomando en cuenta que la diagonal de la matriz $Q$ controla el nivel de ``importancia"
que tiene cada variable de estado, para el sistema RAP, la matriz arbitraria $Q$ tiene 
la forma \ref{eq:Qvariables}. Por otro lado, la diagonal de la matriz $R$
controla el nivel de ``importancia" que tiene cada variable de control, y puede tomar el
valor de 1 para todas ellas, indicando que tienen el mismo peso sobre el sistema.

\begin{equation}
	Q = 
	\begin{bmatrix}
		w_{\alpha} & 0 & 0 & 0 \\
		0 & w_{\theta} & 0 & 0 \\
		0 & 0 & w_{\dot{\alpha}} & 0 \\
		0 & 0 & 0 & w_{\dot{\theta}} \\
	\end{bmatrix}
	\label{eq:Qvariables}
\end{equation}

Como el sistema propuesto restringe la acción de las ruedas a una única entrada
$u = u_1 + u_2$, la matriz $R$ es de dimensión $\mathbb{R}^{1\times 1}$ para el
sistema RAP, y tiene la siguiente forma.

\begin{equation}
	R = [ w_u ]
	\label{eq:Rvariables}
\end{equation}

El prototipo RAP no requiere que la matriz de control $K$ se compile con el firmware, ésta
se transmite a través del sistema de comunicación antes o durante el funcionamiento
del sistema de control, lo que permite experimentar con varias ganancias sin modificar
el firmware. Los detalles de la implementación se muestran en el siguiente capítulo.

La matriz de ganancias instaladas en el RAP y los resultados obtenidos
se exponen en la conclusión del documento.
Los scripts para calcular la matriz K se encuentran en el anexo \ref{anexo:octaveScripts}.

\subsection{Matriz de ganancias del control LQR de tiempo continuo.}

La matriz \ref{eq:QMatrixCT}, es la matriz de pesos de variables de estado propuesta
para computar la matriz \ref{eq:KMatrixCT}, mientras que la matriz \ref{eq:RMatrixCT},
es el peso de la señal de control sobre el sistema.

Éstos son los resultados de calcular $K$ a partir del modelo de tiempo continuo.

\begin{equation}
	Q = C^{\ T}C =
	\begin{bmatrix}
		1 & 0 & 0 & 0 \\
		0 & 1 & 0 & 0 \\
		0 & 0 & 1 & 0 \\
		0 & 0 & 0 & 1 \\
	\end{bmatrix}
	\label{eq:QMatrixCT}
\end{equation}

\begin{equation}
	R = 
	\begin{bmatrix}
		1
	\end{bmatrix}
	\label{eq:RMatrixCT}
\end{equation}

\begin{equation}
	K = [-1.0000,\ -84.2883,\ -1.5648,\ -15.6501]
	\label{eq:KMatrixCT}
\end{equation}

La gráfica \ref{fig:lqrCTGraph}, muestra los resultados de la simulación de
éste control.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.25]{./00-Resources/02-Figures/c44-lqrCTSim}
	\caption{Simulación del controlador de tiempo continuo con estado inicial $x=[0,1,0,0]$.}
	\label{fig:lqrCTGraph}
\end{figure}

\subsection{Matriz de ganancias del control LQR de tiempo discreto (método bilineal).}

La matriz \ref{eq:QMatrixDTBilin}, es la matriz de pesos de variables de estado propuesta
para computar la matriz \ref{eq:KMatrixDTBilin}, mientras que la matriz \ref{eq:RMatrixDTBilin},
es el peso de la señal de control sobre el sistema.

Éstos son los resultados de calcular $K$ a partir del modelo discretizado
por el método de Tustin con un tiempo de muestreo de 10 milisegundos.

\begin{equation}
	Q = C^{\ T}C =
	\begin{bmatrix}
	0.010000000000  &  -0.000003995681 &  0.000049575452 & -0.000000019978\\
	-0.000003995681 &   0.010331385002 & -0.000789472163 &  0.001638010324\\
	0.000049575452  &  -0.000789472163 &  0.009831178933 &  0.000013764943\\
	-0.000000019978 &   0.001638010324 &  0.000013764943 &  0.010016121819
	\end{bmatrix}
	\label{eq:QMatrixDTBilin}
\end{equation}

\begin{equation}
	R = 
	\begin{bmatrix}
		1
	\end{bmatrix}
	\label{eq:RMatrixDTBilin}
\end{equation}

\begin{equation}
	K = [0.088203,-7.617183,-0.139139,-1.412756]
	\label{eq:KMatrixDTBilin}
\end{equation}

La gráfica \ref{fig:lqrDTBilinGraph}, muestra los resultados de la simulación de
éste control.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.25]{./00-Resources/02-Figures/c44-lqrDTBilinSim}
	\caption{Simulación del controlador de tiempo discreto (método bilineal) con estado inicial $x=[0,1,0,0]$.}
	\label{fig:lqrDTBilinGraph}
\end{figure}


\subsection{Matriz de ganancias del control LQR de tiempo discreto (método zoh).}

La matriz \ref{eq:QMatrixDTzoh}, es la matriz de pesos de variables de estado propuesta
para computar la matriz \ref{eq:KMatrixDTzoh}, mientras que la matriz \ref{eq:RMatrixDTzoh},
es el peso de la señal de control sobre el sistema.

Éstos son los resultados de calcular $K$ a partir del modelo discretizado
por el método de retenedor de orden cero con un tiempo de muestreo de 10 milisegundos.

\begin{equation}
	Q = C^{\ T}C =
	\begin{bmatrix}
		1 & 0 & 0 & 0 \\
		0 & 1 & 0 & 0 \\
		0 & 0 & 1 & 0 \\
		0 & 0 & 0 & 1 \\
	\end{bmatrix}
	\label{eq:QMatrixDTzoh}
\end{equation}

\begin{equation}
	R = 
	\begin{bmatrix}
		1
	\end{bmatrix}
	\label{eq:RMatrixDTzoh}
\end{equation}

\begin{equation}
	K = [-0.88152,-76.50836,-1.39770,-14.19047]
	\label{eq:KMatrixDTzoh}
\end{equation}

La gráfica \ref{fig:lqrDTZOHGraph}, muestra los resultados de la simulación de
éste control.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.25]{./00-Resources/02-Figures/c44-lqrDTzohSim}
	\caption{Simulación del controlador de tiempo discreto (método zoh) con estado inicial $x=[0,1,0,0]$.}
	\label{fig:lqrDTZOHGraph}
\end{figure}

